﻿			<!-- Start Footer Section-->
			<footer id="footer">
				<div class="main">
					<div class="footer-info clearfix">
						<div class="org-details">
							<h2><?php echo fs_get_option('fs_hakb'); ?></h2>
							<article class="about-us">
								<blockquote>
									<?php echo fs_get_option('fs_hak'); ?>
								</blockquote>								
							</article>
						</div>
						<div class="org-details jobs-block">
							<h2>İLAN KATEGORİLERİ</h2>
							<ul class="jobs-category">
								<?php wp_nav_menu( array( 'theme_location' => 'altmenu', 'container' => false, 'menu_class' => '', 'container_id' => 'header', 'fallback_cb' => 'wp_page_menu', 'items_wrap' => '%3$s', ) );  ?>
							</ul>
						</div>
						<div class="org-details contact-us">
							<h2>İLETİŞİM \ BİZE ULAŞIN</h2>
							<ul class="address">
								<li>
									<span class="title">Adres</span><i>:</i><span><?php echo fs_get_option('fs_adres'); ?></span>
								</li>
								<li>
									<span class="title">telefon</span><i>:</i><span><?php echo fs_get_option('fs_tel'); ?></span>
								</li>
								<li>
									<span class="title">E-Mail</span><i>:</i><span><a href="mailto:<?php echo fs_get_option('fs_mail'); ?>" class="email"><?php echo fs_get_option('fs_mail'); ?></a></span>
								</li>
								<li>
									<a href="http://www.yesilmen.net" link="black" alink="red" vlink="blue">Aydın Yeşilmen</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="copyright">
					<div class="main">
						<span><?php echo fs_get_option('fs_copy'); ?></span>
					</div>
				</div>
			</footer>
			<!-- End Footer Section-->
		</div>
		<!-- End Wrapper-->		
		<?php wp_footer(); ?>
	</body>
</html>
