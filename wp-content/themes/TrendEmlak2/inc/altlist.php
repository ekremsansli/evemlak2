						<div class="property-box type-2">
							<?php 			
								wp_reset_query();
								$listtip = fs_get_option('fs_altlist');
								$listsay = fs_get_option('fs_altlist2');
								$listkon = fs_get_option('fs_altlist3');
								if($listtip == 'kiralik'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'tur' => 'kiralik',
										'showposts'=> $listsay
									);
									$baslik = 'Kiralık İlanları';
								elseif($listtip == 'satilik'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'tur' => 'satilik',
										'showposts'=> $listsay
									);
									$baslik = 'Satılık İlanları';
								elseif($listtip == 'yeni'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),											
										'showposts'=> $listsay
									);
									$baslik = 'Yeni İlanlar';
								elseif($listtip == 'konut'):
									$args = array
									(
										'post_type'=> 'konut',
										'showposts'=> $listsay
									);
									$baslik = 'Konut İlanları';
								elseif($listtip == 'isyeri'):
									$args = array
									(
										'post_type'=> 'isyeri',
										'showposts'=> $listsay
									);
									$baslik = 'İşyeri İlanları';
								elseif($listtip == 'arsa'):
									$args = array
									(
										'post_type'=> 'arsa',
										'showposts'=> $listsay
									);
									$baslik = 'Arsa İlanları';
								elseif($listtip == 'trustik-tesis'):
									$args = array
									(
										'post_type'=> 'trustik-tesis',
										'showposts'=> $listsay
									);
									$baslik = 'Trustik Tesis İlanları';
								elseif($listtip == 'fiyati-dusenler'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'showposts'=> $listsay,
										'meta_query' => array(										
											array(
												'key' => 'ozel',
												'value' => '3'
											)										
										)
									);
									$baslik = 'Fiyatı Düşenler';
								elseif($listtip == 'krediye-uygun'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'showposts'=> $listsay,
										'meta_query' => array(										
											array(
												'key' => 'ozel',
												'value' => '2'
											)										
										)
									);
									$baslik = 'Krediye Uygunlar';
								elseif($listtip == 'acil-satilik'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'showposts'=> $listsay,
										'meta_query' => array(										
											array(
												'key' => 'ozel',
												'value' => '4'
											)										
										)
									);
									$baslik = 'Acil Satılıklar';
								elseif($listtip == 'acil-kiralik'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'showposts'=> $listsay,
										'meta_query' => array(										
											array(
												'key' => 'ozel',
												'value' => '5'
											)										
										)
									);
									$baslik = 'Acil Kiralıklar';
								else:
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),											
										'showposts'=> $listsay
									);
									$baslik = 'Yeni İlanlar';										
								endif;
							?>
							<div class="title-head clearfix">
								<h2><?php echo $baslik; ?></h2><a href="<?php if($listkon) echo get_page_link($listkon); ?>" class="all">Tümü</a>
							</div>
							<ul class="gallery-block">
								<?php 												
									query_posts( $args );
									if (have_posts()) : while (have_posts()) : the_post();																						
								?>
								<li>
									<figure class="appartment">
										<a href="<?php the_permalink(); ?>">
										<?php
											if ( has_post_thumbnail()) : 
												the_post_thumbnail( 'list', array('class' => 'fl-right', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'' )); 
											elseif (get_post_meta($post->ID, 'resim', true) != '') : 
												echo '<img class="fl-right" src="'.get_post_meta($post->ID, 'resim', true).'" width="246" height="169" alt="'.get_the_title().'" />'; 
											else : 
												echo '<img class="fl-right" src="'.fs_get_option('fs_logo').'" width="246" height="169" alt="'.get_the_title().'" />';
											endif;
										?>
										</a>
										<span class="pricing">
											<?php
											if(get_post_type()=="arsa")
											{
												echo number_format((intval(get_post_meta($post->ID, 'fiyat', true)) * intval(get_post_meta($post->ID, 'm2', true))), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true));
											}
											else
											{
												echo number_format(intval(get_post_meta($post->ID, 'fiyat', true)), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); 
											}
											?>
										</span>
										<?php satistipi(); ?>
									</figure>
									<article class="appartment-details">
										<strong><?php the_title(); ?></strong>
										<span><?php semtgetir(); ?></span>
										<a href="<?php the_permalink(); ?>" class="more-info">Detaylı İncele</a>
									</article>
								</li>
								<?php endwhile; endif; ?>
							</ul>
						</div>