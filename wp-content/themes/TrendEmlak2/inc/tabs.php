						<div class="property-box" id="slidelist">
							<?php 			
								wp_reset_query();
								$listtip = fs_get_option('fs_slilist');
								$listsay = fs_get_option('fs_slilist2');
								$listkon = fs_get_option('fs_slilist3');
								$krediyeuygun = fs_get_option('fs_krediyeuygun');
								$fiyatidusen = fs_get_option('fs_fiyatidusenler');
								$acil = fs_get_option('fs_acil');
								if($listtip == 'kiralik'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'tur' => 'kiralik',
										'showposts'=> $listsay
									);
									$baslik = 'Kiralık İlanları';
								elseif($listtip == 'satilik'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'tur' => 'satilik',
										'showposts'=> $listsay
									);
									$baslik = 'Satılık İlanları';
								elseif($listtip == 'yeni'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),											
										'showposts'=> $listsay
									);
									$baslik = 'Yeni İlanlar';
								elseif($listtip == 'konut'):
									$args = array
									(
										'post_type'=> 'konut',
										'showposts'=> $listsay
									);
									$baslik = 'Konut İlanları';
								elseif($listtip == 'isyeri'):
									$args = array
									(
										'post_type'=> 'isyeri',
										'showposts'=> $listsay
									);
									$baslik = 'İşyeri İlanları';
								elseif($listtip == 'arsa'):
									$args = array
									(
										'post_type'=> 'arsa',
										'showposts'=> $listsay
									);
									$baslik = 'Arsa İlanları';
								elseif($listtip == 'trustik-tesis'):
									$args = array
									(
										'post_type'=> 'trustik-tesis',
										'showposts'=> $listsay
									);
									$baslik = 'Trustik Tesis İlanları';
								elseif($listtip == 'fiyati-dusenler'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'showposts'=> $listsay,
										'meta_query' => array(										
											array(
												'key' => 'ozel',
												'value' => '3'
											)										
										)
									);
									$baslik = 'Fiyatı Düşenler';
								elseif($listtip == 'krediye-uygun'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'showposts'=> $listsay,
										'meta_query' => array(										
											array(
												'key' => 'ozel',
												'value' => '2'
											)										
										)
									);
									$baslik = 'Krediye Uygunlar';
								elseif($listtip == 'acil-satilik'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'showposts'=> $listsay,
										'meta_query' => array(										
											array(
												'key' => 'ozel',
												'value' => '4'
											)										
										)
									);
									$baslik = 'Acil Satılıklar';
								elseif($listtip == 'acil-kiralik'):
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),
										'showposts'=> $listsay,
										'meta_query' => array(										
											array(
												'key' => 'ozel',
												'value' => '5'
											)										
										)
									);
									$baslik = 'Acil Kiralıklar';
								else:
									$args = array
									(
										'post_type'=> array('konut','isyeri','arsa','trustik-tesis'),											
										'showposts'=> $listsay
									);
									$baslik = 'Yeni İlanlar';										
								endif;
							?>
							<div class="view-info">
								<ul class="tabs-box">
									<li>
										<a href="<?php if($fiyatidusen) echo get_page_link($fiyatidusen); ?>"> <span class="info-icon"><i class="fa fa-hand-o-down"></i></span> <span class="text">Fiyatı Düşenler</span> </a>
									</li>
									<li>
										<a href="<?php if($acil) echo get_page_link($acil); ?>"> <span class="info-icon"><i class="fa fa-tachometer"></i></span> <span class="text">Acil Olan İlanlar</span> </a>
									</li>
									<li>
										<a href="<?php if($krediyeuygun) echo get_page_link($krediyeuygun); ?>"> <span class="info-icon"><i class="fa fa-money"></i></span> <span class="text">Krediye Uygun Olanlar</span> </a>
									</li>
								</ul>
								<div class="move-item">
									<a href="#" class="prev">prev arrow</a>
									<span class="element"><a class="tumu" href="<?php if($listkon) echo get_page_link($listkon); ?>">TUMU</a></span>
									<a href="#" class="next">next arrow</a>
								</div>
							</div>
							<div class="tabss">
							<div class="tabs0">
								<ul class="gallery-block slider">
									<?php 													
										query_posts( $args );
										if (have_posts()) : while (have_posts()) : the_post();																						
									?>
									<li>
										<figure class="appartment">
											<a href="<?php the_permalink(); ?>">
											<?php
												if ( has_post_thumbnail()) : 
													the_post_thumbnail( 'list', array('class' => 'fl-right', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'' )); 
												elseif (get_post_meta($post->ID, 'resim', true) != '') : 
													echo '<img class="fl-right" src="'.get_post_meta($post->ID, 'resim', true).'" width="246" height="169" alt="'.get_the_title().'" />'; 
												else : 
													echo '<img class="fl-right" src="'.fs_get_option('fs_logo').'" width="246" height="169" alt="'.get_the_title().'" />';
												endif;
											?>
											</a>
											<span class="pricing">
												<?php
												if(get_post_type()=="arsa")
												{
													echo number_format((intval(get_post_meta($post->ID, 'fiyat', true)) * intval(get_post_meta($post->ID, 'm2', true))), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true));
												}
												else
												{
													echo number_format(intval(get_post_meta($post->ID, 'fiyat', true)), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); 
												}
												?>
											</span>
											<?php satistipi(); ?>
										</figure>
										<article class="appartment-details catalog-box">
											<strong><?php the_title(); ?></strong>
											<span><?php semtgetir(); ?></span>
											<ul class="catalog">
												<li>
													<i class="icon1"></i>
													<span class="left-text">ilan No</span>
													<b class="dot">:</b>											
													<small class="left-text right-text"><?php ilanno($post->ID); ?></small>
												</li>
												<?php if(get_post_type()=="trustik-tesis"): ?>
												<li>
													<i class="icon2"></i>
													<span class="left-text">Metrekare</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo intval(get_post_meta($post->ID, 'am2', true))+intval(get_post_meta($post->ID, 'km2', true)); ?> m²</small>
												</li>
												<li>
													<i class="icon3"></i>
													<span class="left-text">oda</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'oda', true); ?></small>
												</li>
												<li>
													<i class="icon4"></i>
													<span class="left-text">yatak</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'yatak', true); ?></small>
												</li>
												<li>
													<i class="icon6"></i>
													<span class="left-text">Yıldız</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'yildiz', true); ?></small>
												</li>
												<?php else: ?>
												<li>
													<i class="icon2"></i>
													<span class="left-text">Metrekare</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'm2', true); ?> m²</small>
												</li>
												<?php endif; ?>
												<?php if(get_post_type()=="konut"): ?>
												<li>
													<i class="icon3"></i>
													<span class="left-text">salon</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'salon', true); ?></small>
												</li>											
												<li>
													<i class="icon4"></i>
													<span class="left-text">yatak odasi</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'oda', true); ?></small>
												</li>
												<li>
													<i class="icon5"></i>
													<span class="left-text">banyo</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'banyo', true); ?></small>
												</li>
												<?php endif; ?>
												<?php if(get_post_type()=="arsa"): ?>
												<li>
													<i class="icon2"></i>
													<span class="left-text">m² Fiyatı</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'fiyat', true); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); ?></small>
												</li>											
												<li>
													<i class="icon2"></i>
													<span class="left-text">İmar</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php the_terms( $post->ID, 'imar', '', ', ', '' ); ?></small>
												</li>
												<li>
													<i class="icon2"></i>
													<span class="left-text">kaks</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'kaks', true); ?></small>
												</li>
												<?php endif; ?>
												<?php if(get_post_type()=="isyeri"): ?>
												<li>
													<i class="icon3"></i>
													<span class="left-text">oda</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'oda', true); ?></small>
												</li>											
												<li>
													<i class="icon2"></i>
													<span class="left-text">Tip</span>
													<b class="dot">:</b>
													<small class="left-text right-text"><?php the_terms( $post->ID, 'isyeri-tipi', '', ', ', '' ); ?></small>
												</li>
												<li>
													<i class="icon5"></i>
													<span class="left-text">Durum</span>
													<b class="dot">:</b>
													<small class="left-text right-text">
													<?php switch (get_post_meta($post->ID, 'yapidurumu', true)){ 
														case 1: echo 'Sıfır';break;
														case 2: echo 'İkinci El';break;
														case 3: echo 'İnşaat';break;
														case 4: echo 'Proje';break;													
														default; echo 'Belirtilmemiş'; 
													}?>
													</small>
												</li>
												<?php endif; ?>
											</ul>
											<a href="<?php the_permalink(); ?>" class="more-info">Detaylı İncele</a>
										</article>

									</li>
									<?php endwhile; endif; ?>								
								</ul>
								</div>								
							</div>
						</div>