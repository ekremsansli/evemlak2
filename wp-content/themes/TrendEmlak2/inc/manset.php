
						<div class="banner">
							<ul class="slides">
								<li>
									<div id="manset">
										<?php 
											wp_reset_query();
											$args = array
											(
												'post_type'=> 'manset',
												'showposts'=> '5'
											);
											query_posts( $args );
											if (have_posts()) : while (have_posts()) : the_post();
										?>
										<div class="figure">
											<a href="<?php if(get_post_meta($post->ID, 'url', true)) echo get_post_meta($post->ID, 'url', true); else the_permalink(); ?>">
												<?php
													if ( has_post_thumbnail()) : 
														the_post_thumbnail( 'manset', array('class' => 'fl-right', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'' )); 
													elseif (get_post_meta($post->ID, 'resim', true) != '') : 
														echo '<img class="fl-right" src="'.get_post_meta($post->ID, 'resim', true).'" width="766" height="259" alt="'.get_the_title().'" />'; 
													else : 
														echo '<img class="fl-right" src="'.fs_get_option('fs_logo').'" width="766" height="259" alt="'.get_the_title().'" />';
													endif;
												?>
											</a>
										</div>
										<?php endwhile; endif; ?>
									</div>
									<div class="slider-text">
										<span class="info-icon"><i class="fa fa-info">&nbsp;</i></span>
										<div class="inner-text">
											<?php 
												wp_reset_query();
												$args = array
												(
													'post_type'=> 'manset',
													'showposts'=> '5'
												);
												query_posts( $args );
												$i=0;
												if (have_posts()) : while (have_posts()) : the_post();
											?>
											<div>
												<strong><?php the_title(); ?></strong>
												<span><?php fs_excerpt('fs_kisa_yazi10', 'fs_kisalt'); ?></span>
											</div>
											<?php $i++; endwhile; endif; ?>											
										</div>
										<ul class="control" id="thumb">
											<?php for($j=0; $j<$i; $j++): ?>
												<li>
													<a href="javascript:;" data-slide-index="<?php echo $j; ?>"></a>
												</li>
											<?php endfor; ?>											
										</ul>
									</div>
								</li>
							</ul>
						</div>