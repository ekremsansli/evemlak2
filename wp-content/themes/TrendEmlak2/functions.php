<?php
class TrendWP {
	var $ayar = null;

	function __construct($ayar = null) {
		if ($ayar) {
			$this->ayar = $ayar;
		}

	}

	function lisans_sorgula() {
		$lisans = mysql_fetch_array( @mysql_query( 'SELECT `id`,`lisans1`,`lisans2` FROM `trendwp_lisans` where `tema`=\'' . $this->ayar['kod'] . '\' order by id desc LIMIT 1' ) );
		$results = $this->lisans_kontrol( $lisans['lisans1'], $lisans['lisans2'] );

		if ($results['status'] == 'Active') {
			if ($results['localkey']) {
				$this->lisans_anahtar_guncelle( false, $results['localkey'] );
				return 0;
			}
		}
		else {
			if ($results['status'] == 'Invalid') {
				return 1;
			}


			if ($results['status'] == 'Expired') {
				return 2;
			}


			if ($results['status'] == 'Suspended') {
				return 3;
			}
		}

	}
}

function manset_ekle() {
	register_post_type( 'manset', array( 'labels' => array( 'name' => __( 'Manşet' ), 'singular_name' => __( 'Manşet' ), 'add_new' => __( 'Yeni Manşet' ), 'add_new_item' => __( 'Yeni Manşet Ekle' ), 'edit_item' => __( 'Manşeti Düzenle' ), 'new_item' => __( 'Yeni Manşet' ), 'view_item' => __( 'Manşete Git' ), 'search_items' => __( 'Manşetlerde Ara' ), 'not_found' => __( 'Hiç manşet bulunamadı.' ), 'not_found_in_trash' => __( 'Çöpte hiç manşet yok.' ), 'parent_item_colon' => '' ), 'public' => true, 'has_archive' => true, 'publicly_queryable' => true, 'show_ui' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'manset' ), 'capability_type' => 'post', 'hierarchical' => false, 'menu_position' => 4, 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ), 'has_archive' => 'manset' ) );
}

function konut_ekle() {
	register_post_type( 'konut', array( 'labels' => array( 'name' => __( 'Konut' ), 'singular_name' => __( 'Konut' ), 'add_new' => __( 'Yeni Konut' ), 'add_new_item' => __( 'Yeni Konut Ekle' ), 'edit_item' => __( 'Konutu Düzenle' ), 'new_item' => __( 'Yeni Konut' ), 'view_item' => __( 'Konuta Git' ), 'search_items' => __( 'Konutlarda Ara' ), 'not_found' => __( 'Hiç konut bulunamadı.' ), 'not_found_in_trash' => __( 'Çöpte hiç konut yok.' ), 'parent_item_colon' => '' ), 'public' => true, 'has_archive' => true, 'publicly_queryable' => true, 'show_ui' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'konut' ), 'capability_type' => 'post', 'hierarchical' => false, 'menu_position' => 4, 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ), 'has_archive' => 'konut' ) );
}

function isyeri_ekle() {
	register_post_type( 'isyeri', array( 'labels' => array( 'name' => __( 'İşyeri' ), 'singular_name' => __( 'İşyeri' ), 'add_new' => __( 'Yeni İşyeri' ), 'add_new_item' => __( 'Yeni İşyeri Ekle' ), 'edit_item' => __( 'İşyerini Düzenle' ), 'new_item' => __( 'Yeni İşyeri' ), 'view_item' => __( 'İşyerine Git' ), 'search_items' => __( 'İşyerinde Ara' ), 'not_found' => __( 'Hiç işyeri bulunamadı.' ), 'not_found_in_trash' => __( 'Çöpte hiç işyeri yok.' ), 'parent_item_colon' => '' ), 'public' => true, 'has_archive' => true, 'publicly_queryable' => true, 'show_ui' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'isyeri' ), 'capability_type' => 'post', 'hierarchical' => false, 'menu_position' => 4, 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ), 'has_archive' => 'isyeri' ) );
}

function arsa_ekle() {
	register_post_type( 'arsa', array( 'labels' => array( 'name' => __( 'Arsa' ), 'singular_name' => __( 'Arsa' ), 'add_new' => __( 'Yeni Arsa' ), 'add_new_item' => __( 'Yeni Arsa Ekle' ), 'edit_item' => __( 'Arsayı Düzenle' ), 'new_item' => __( 'Yeni Arsa' ), 'view_item' => __( 'Arsaya Git' ), 'search_items' => __( 'Arsalarda Ara' ), 'not_found' => __( 'Hiç arsa bulunamadı.' ), 'not_found_in_trash' => __( 'Çöpte hiç arsa yok.' ), 'parent_item_colon' => '' ), 'public' => true, 'has_archive' => true, 'publicly_queryable' => true, 'show_ui' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'arsa' ), 'capability_type' => 'post', 'hierarchical' => false, 'menu_position' => 4, 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ), 'has_archive' => 'arsa' ) );
}

function trustik_ekle() {
	register_post_type( 'trustik-tesis', array( 'labels' => array( 'name' => __( 'Trustik Tesis' ), 'singular_name' => __( 'Trustik Tesis' ), 'add_new' => __( 'Yeni Trustik Tesis' ), 'add_new_item' => __( 'Yeni Trustik Tesis Ekle' ), 'edit_item' => __( 'Trustik Tesisi Düzenle' ), 'new_item' => __( 'Yeni Trustik Tesis' ), 'view_item' => __( 'Trustik Tesise Git' ), 'search_items' => __( 'Trustik Tesislerde Ara' ), 'not_found' => __( 'Hiç trustik tesis bulunamadı.' ), 'not_found_in_trash' => __( 'Çöpte hiç trustik tesis yok.' ), 'parent_item_colon' => '' ), 'public' => true, 'has_archive' => true, 'publicly_queryable' => true, 'show_ui' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'trustik-tesis' ), 'capability_type' => 'post', 'hierarchical' => false, 'menu_position' => 4, 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ), 'has_archive' => 'trustik-tesis' ) );
}

function add_custom_taxonomies() {
	register_taxonomy( 'tur', array( 'konut', 'isyeri', 'trustik-tesis', 'arsa' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Emlak Türü', 'taxonomy general name' ), 'singular_name' => _x( 'Emlak Türü', 'taxonomy singular name' ), 'search_items' => __( 'Emlak Türü ara' ), 'all_items' => __( 'Tüm Emlak Türleri' ), 'edit_item' => __( 'Emlak Türü düzenle' ), 'update_item' => __( 'Emlak Türü güncelle' ), 'add_new_item' => __( 'Yeni Emlak Türü ekle' ), 'new_item_name' => __( 'Yeni Emlak Türü' ), 'menu_name' => __( 'Emlak Türü' ) ), 'rewrite' => array( 'slug' => 'tur', 'with_front' => false, 'hierarchical' => false ) ) );
	register_taxonomy( 'cephe', array( 'konut', 'isyeri' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Cephe', 'taxonomy general name' ), 'singular_name' => _x( 'Cephe', 'taxonomy singular name' ), 'search_items' => __( 'Cephe ara' ), 'all_items' => __( 'Tüm Cepheler' ), 'edit_item' => __( 'Cephe düzenle' ), 'update_item' => __( 'Cephe güncelle' ), 'add_new_item' => __( 'Yeni Cephe ekle' ), 'new_item_name' => __( 'Yeni Cephe' ), 'menu_name' => __( 'Cephe' ) ), 'rewrite' => array( 'slug' => 'cephe', 'with_front' => false, 'hierarchical' => false ) ) );
	register_taxonomy( 'konut-ozelligi', array( 'konut', 'isyeri' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Konut Özellikleri', 'taxonomy general name' ), 'singular_name' => _x( 'Konut Özelliği', 'taxonomy singular name' ), 'search_items' => __( 'Konut Özeliği ara' ), 'all_items' => __( 'Tüm Konut Özellikleri' ), 'edit_item' => __( 'Konut Özeliği düzenle' ), 'update_item' => __( 'Konut Özelliği güncelle' ), 'add_new_item' => __( 'Yeni Konut Özeliği ekle' ), 'new_item_name' => __( 'Yeni Konut Ozeliği' ), 'menu_name' => __( 'Konut Özelikleri' ) ), 'rewrite' => array( 'slug' => 'konut-ozelligi', 'with_front' => false, 'hierarchical' => false ) ) );
	register_taxonomy( 'ic-ozellik', array( 'konut' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'İç Özellikler', 'taxonomy general name' ), 'singular_name' => _x( 'İç Özelliği', 'taxonomy singular name' ), 'search_items' => __( 'İç Özeliği ara' ), 'all_items' => __( 'Tüm İç Özellikleri' ), 'edit_item' => __( 'İç Özeliği düzenle' ), 'update_item' => __( 'İç Özelliği güncelle' ), 'add_new_item' => __( 'Yeni İç Özeliği ekle' ), 'new_item_name' => __( 'Yeni İç Ozeliği' ), 'menu_name' => __( 'İç Özelikleri' ) ), 'rewrite' => array( 'slug' => 'ic-ozellik', 'with_front' => false, 'hierarchical' => false ) ) );
	register_taxonomy( 'isyeri-ozellikleri', array( 'isyeri' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'İşyeri Özellikleri', 'taxonomy general name' ), 'singular_name' => _x( 'İşyeri Özelliği', 'taxonomy singular name' ), 'search_items' => __( 'İşyeri Özeliği ara' ), 'all_items' => __( 'Tüm İşyeri Özellikleri' ), 'edit_item' => __( 'İşyeri Özeliği düzenle' ), 'update_item' => __( 'İşyeri Özelliği güncelle' ), 'add_new_item' => __( 'Yeni İşyeri Özeliği ekle' ), 'new_item_name' => __( 'Yeni İşyeri Ozeliği' ), 'menu_name' => __( 'İşyeri Özelikleri' ) ), 'rewrite' => array( 'slug' => 'isyeri-ozellikleri', 'with_front' => false, 'hierarchical' => false ) ) );
	register_taxonomy( 'manzara', array( 'konut', 'isyeri', 'trustik-tesis', 'arsa' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Manzara', 'taxonomy general name' ), 'singular_name' => _x( 'Manzara', 'taxonomy singular name' ), 'search_items' => __( 'Manzara ara' ), 'all_items' => __( 'Tüm Manzaralar' ), 'edit_item' => __( 'Manzara düzenle' ), 'update_item' => __( 'Manzara güncelle' ), 'add_new_item' => __( 'Yeni Manzara ekle' ), 'new_item_name' => __( 'Yeni Manzara' ), 'menu_name' => __( 'Manzara' ) ), 'rewrite' => array( 'slug' => 'manzara', 'with_front' => false, 'hierarchical' => false ) ) );
	register_taxonomy( 'altyapi', array( 'konut', 'isyeri', 'trustik-tesis' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Alt Yapı', 'taxonomy general name' ), 'singular_name' => _x( 'Alt Yapı', 'taxonomy singular name' ), 'search_items' => __( 'Alt Yapı ara' ), 'all_items' => __( 'Tüm Alt Yapılar' ), 'edit_item' => __( 'Alt Yapı düzenle' ), 'update_item' => __( 'Alt Yapı güncelle' ), 'add_new_item' => __( 'Yeni Alt Yapı ekle' ), 'new_item_name' => __( 'Yeni Alt Yapı' ), 'menu_name' => __( 'Alt Yapı' ) ), 'rewrite' => array( 'slug' => 'altyapi', 'with_front' => false, 'hierarchical' => false ) ) );
	register_taxonomy( 'cevre', array( 'konut', 'isyeri', 'trustik-tesis' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Çevre Özellikleri', 'taxonomy general name' ), 'singular_name' => _x( 'Çevre Özelliği', 'taxonomy singular name' ), 'search_items' => __( 'Çevre Özelliği ara' ), 'all_items' => __( 'Tüm Çevre Özellikleri' ), 'edit_item' => __( 'Çevre Özelliği düzenle' ), 'update_item' => __( 'Çevre Özelliği güncelle' ), 'add_new_item' => __( 'Yeni Çevre Özelliği ekle' ), 'new_item_name' => __( 'Yeni Çevre Özelliği' ), 'menu_name' => __( 'Çevre Özellikleri' ) ), 'rewrite' => array( 'slug' => 'cevre', 'with_front' => false, 'hierarchical' => false ) ) );
	register_taxonomy( 'sosyal', array( 'konut' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Sosyal', 'taxonomy general name' ), 'singular_name' => _x( 'Sosyal', 'taxonomy singular name' ), 'search_items' => __( 'Sosyal ara' ), 'all_items' => __( 'Tüm Sosyallar' ), 'edit_item' => __( 'Sosyal düzenle' ), 'update_item' => __( 'Sosyal güncelle' ), 'add_new_item' => __( 'Yeni Sosyal ekle' ), 'new_item_name' => __( 'Yeni Sosyal' ), 'menu_name' => __( 'Sosyal' ) ), 'rewrite' => array( 'slug' => 'sosyal', 'with_front' => false, 'hierarchical' => false ) ) );
	register_taxonomy( 'konut-tipi', 'konut', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Emlak Tipi', 'taxonomy general name' ), 'singular_name' => _x( 'Emlak Tipi', 'taxonomy singular name' ), 'search_items' => __( 'Emlak Tipi ara' ), 'all_items' => __( 'Tüm Emlak Tipleri' ), 'edit_item' => __( 'Emlak Tipi düzenle' ), 'update_item' => __( 'Emlak Tipi güncelle' ), 'add_new_item' => __( 'Yeni Emlak Tipi ekle' ), 'new_item_name' => __( 'Yeni Emlak Tipi' ), 'menu_name' => __( 'Emlak Tipi' ) ), 'rewrite' => array( 'slug' => 'konut-tipi', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'isyeri-tipi', 'isyeri', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Emlak Tipi', 'taxonomy general name' ), 'singular_name' => _x( 'Emlak Tipi', 'taxonomy singular name' ), 'search_items' => __( 'Emlak Tipi ara' ), 'all_items' => __( 'Tüm Emlak Tipleri' ), 'edit_item' => __( 'Emlak Tipi düzenle' ), 'update_item' => __( 'Emlak Tipi güncelle' ), 'add_new_item' => __( 'Yeni Emlak Tipi ekle' ), 'new_item_name' => __( 'Yeni Emlak Tipi' ), 'menu_name' => __( 'Emlak Tipi' ) ), 'rewrite' => array( 'slug' => 'isyeri-tipi', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'trustik-tesis-tipi', 'trustik-tesis', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Emlak Tipi', 'taxonomy general name' ), 'singular_name' => _x( 'Emlak Tipi', 'taxonomy singular name' ), 'search_items' => __( 'Emlak Tipi ara' ), 'all_items' => __( 'Tüm Emlak Tipleri' ), 'edit_item' => __( 'Emlak Tipi düzenle' ), 'update_item' => __( 'Emlak Tipi güncelle' ), 'add_new_item' => __( 'Yeni Emlak Tipi ekle' ), 'new_item_name' => __( 'Yeni Emlak Tipi' ), 'menu_name' => __( 'Emlak Tipi' ) ), 'rewrite' => array( 'slug' => 'trustik-tesis-tipi', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'imar', 'arsa', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'İmar Durumu', 'taxonomy general name' ), 'singular_name' => _x( 'İmar Durumu', 'taxonomy singular name' ), 'search_items' => __( 'İmar Durumu ara' ), 'all_items' => __( 'Tüm İmar Durumları' ), 'edit_item' => __( 'İmar Durumunu düzenle' ), 'update_item' => __( 'İmar Durumunu güncelle' ), 'add_new_item' => __( 'Yeni İmar Durumu ekle' ), 'new_item_name' => __( 'Yeni İmar Durumu' ), 'menu_name' => __( 'İmar Durumu' ) ), 'rewrite' => array( 'slug' => 'imar-durumu', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'arsa-altyapi', 'arsa', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Alt Yapı', 'taxonomy general name' ), 'singular_name' => _x( 'Alt Yapı', 'taxonomy singular name' ), 'search_items' => __( 'Alt Yapı ara' ), 'all_items' => __( 'Tüm Alt Yapılar' ), 'edit_item' => __( 'Alt Yapı düzenle' ), 'update_item' => __( 'Alt Yapıyı güncelle' ), 'add_new_item' => __( 'Yeni Alt Yapı ekle' ), 'new_item_name' => __( 'Yeni Alt Yapı' ), 'menu_name' => __( 'Alt Yapı' ) ), 'rewrite' => array( 'slug' => 'arsa-altyapi', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'konum', 'arsa', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Konum', 'taxonomy general name' ), 'singular_name' => _x( 'Konum', 'taxonomy singular name' ), 'search_items' => __( 'Konum ara' ), 'all_items' => __( 'Tüm Konumlar' ), 'edit_item' => __( 'Konumu düzenle' ), 'update_item' => __( 'Konumu güncelle' ), 'add_new_item' => __( 'Yeni Konum ekle' ), 'new_item_name' => __( 'Yeni Konum' ), 'menu_name' => __( 'Konum' ) ), 'rewrite' => array( 'slug' => 'konum', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'arsa-ozellikler', 'arsa', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Genel Özellikler', 'taxonomy general name' ), 'singular_name' => _x( 'Genel Özellik', 'taxonomy singular name' ), 'search_items' => __( 'Genel Özellik ara' ), 'all_items' => __( 'Tüm Genel Özellikler' ), 'edit_item' => __( 'Genel Özelliği düzenle' ), 'update_item' => __( 'Genel Özelliği güncelle' ), 'add_new_item' => __( 'Yeni Genel Özellik ekle' ), 'new_item_name' => __( 'Yeni Genel Özellik' ), 'menu_name' => __( 'Genel Özellikler' ) ), 'rewrite' => array( 'slug' => 'genel-ozellikler', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'oda-tipi', 'trustik-tesis', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Oda Tipi', 'taxonomy general name' ), 'singular_name' => _x( 'Oda Tipi', 'taxonomy singular name' ), 'search_items' => __( 'Oda Tipi ara' ), 'all_items' => __( 'Tüm Oda Tipleri' ), 'edit_item' => __( 'Oda Tipini düzenle' ), 'update_item' => __( 'Oda Tipini güncelle' ), 'add_new_item' => __( 'Yeni Oda Tipi ekle' ), 'new_item_name' => __( 'Yeni Oda Tipi' ), 'menu_name' => __( 'Oda Tipi' ) ), 'rewrite' => array( 'slug' => 'oda-tipi', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'oda-ozellikleri', 'trustik-tesis', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Oda Özellikleri', 'taxonomy general name' ), 'singular_name' => _x( 'Oda Özelliği', 'taxonomy singular name' ), 'search_items' => __( 'Oda Özelliği ara' ), 'all_items' => __( 'Tüm Oda Özellikleri' ), 'edit_item' => __( 'Oda Özelliğini düzenle' ), 'update_item' => __( 'Oda Özelliğini güncelle' ), 'add_new_item' => __( 'Yeni Oda Özelliği ekle' ), 'new_item_name' => __( 'Yeni Oda Özelliği' ), 'menu_name' => __( 'Oda Özelliği' ) ), 'rewrite' => array( 'slug' => 'oda-ozellikleri', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'banyo-ozellikleri', 'trustik-tesis', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Banyo Özellikleri', 'taxonomy general name' ), 'singular_name' => _x( 'Banyo Özelliği', 'taxonomy singular name' ), 'search_items' => __( 'Banyo Özelliği ara' ), 'all_items' => __( 'Tüm Banyo Özellikleri' ), 'edit_item' => __( 'Banyo Özelliğini düzenle' ), 'update_item' => __( 'Banyo Özelliğini güncelle' ), 'add_new_item' => __( 'Yeni Banyo Özelliği ekle' ), 'new_item_name' => __( 'Yeni Banyo Özelliği' ), 'menu_name' => __( 'Banyo Özelliği' ) ), 'rewrite' => array( 'slug' => 'banyo-ozellikleri', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'tesis-ozellikleri', 'trustik-tesis', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Tesis Özellikleri', 'taxonomy general name' ), 'singular_name' => _x( 'Tesis Özelliği', 'taxonomy singular name' ), 'search_items' => __( 'Tesis Özelliği ara' ), 'all_items' => __( 'Tüm Tesis Özellikleri' ), 'edit_item' => __( 'Tesis Özelliğini düzenle' ), 'update_item' => __( 'Tesis Özelliğini güncelle' ), 'add_new_item' => __( 'Yeni Tesis Özelliği ekle' ), 'new_item_name' => __( 'Yeni Tesis Özelliği' ), 'menu_name' => __( 'Tesis Özelliği' ) ), 'rewrite' => array( 'slug' => 'tesis-ozellikleri', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'yeme-icme', 'trustik-tesis', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Yeme & İçme', 'taxonomy general name' ), 'singular_name' => _x( 'Yeme & İçme', 'taxonomy singular name' ), 'search_items' => __( 'Yeme & İçme ara' ), 'all_items' => __( 'Tüm Yeme & İçme' ), 'edit_item' => __( 'Yeme & İçme düzenle' ), 'update_item' => __( 'Yeme & İçme güncelle' ), 'add_new_item' => __( 'Yeni Yeme & İçme ekle' ), 'new_item_name' => __( 'Yeni Yeme & İçme' ), 'menu_name' => __( 'Yeme & İçme' ) ), 'rewrite' => array( 'slug' => 'yeme-icme', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'cevre-aktiviteleri', 'trustik-tesis', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Çevre Aktiviteleri', 'taxonomy general name' ), 'singular_name' => _x( 'Çevre Aktivitesi', 'taxonomy singular name' ), 'search_items' => __( 'Çevre Aktivitesi ara' ), 'all_items' => __( 'Tüm Çevre Aktiviteleri' ), 'edit_item' => __( 'Çevre Aktivitesini düzenle' ), 'update_item' => __( 'Çevre Aktivitesini güncelle' ), 'add_new_item' => __( 'Yeni Çevre Aktivitesi ekle' ), 'new_item_name' => __( 'Yeni Çevre Aktivitesi' ), 'menu_name' => __( 'Çevre Aktiviteleri' ) ), 'rewrite' => array( 'slug' => 'cevre-aktiviteleri', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'tesis-aktiviteleri', 'trustik-tesis', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Tesis Aktiviteleri', 'taxonomy general name' ), 'singular_name' => _x( 'Tesis Aktivitesi', 'taxonomy singular name' ), 'search_items' => __( 'Tesis Aktivitesi ara' ), 'all_items' => __( 'Tüm Tesis Aktiviteleri' ), 'edit_item' => __( 'Tesis Aktivitesini düzenle' ), 'update_item' => __( 'Tesis Aktivitesini güncelle' ), 'add_new_item' => __( 'Yeni Tesis Aktivitesi ekle' ), 'new_item_name' => __( 'Yeni Tesis Aktivitesi' ), 'menu_name' => __( 'Tesis Aktiviteleri' ) ), 'rewrite' => array( 'slug' => 'tesis-aktiviteleri', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'toplanti-kongre', 'trustik-tesis', array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Toplantı & Kongre', 'taxonomy general name' ), 'singular_name' => _x( 'Toplantı & Kongre', 'taxonomy singular name' ), 'search_items' => __( 'Toplantı & Kongre ara' ), 'all_items' => __( 'Tüm Toplantı & Kongre' ), 'edit_item' => __( 'Toplantı & Kongre düzenle' ), 'update_item' => __( 'Toplantı & Kongre güncelle' ), 'add_new_item' => __( 'Yeni Toplantı & Kongre ekle' ), 'new_item_name' => __( 'Yeni Toplantı & Kongre' ), 'menu_name' => __( 'Toplantı & Kongre' ) ), 'rewrite' => array( 'slug' => 'toplanti-kongre', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'ulasim', array( 'konut', 'isyeri', 'trustik-tesis' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Ulaşım', 'taxonomy general name' ), 'singular_name' => _x( 'Ulaşım', 'taxonomy singular name' ), 'search_items' => __( 'Ulaşım ara' ), 'all_items' => __( 'Tüm Ulaşım' ), 'edit_item' => __( 'Ulaşım düzenle' ), 'update_item' => __( 'Ulaşım güncelle' ), 'add_new_item' => __( 'Yeni Ulaşım ekle' ), 'new_item_name' => __( 'Yeni Ulaşım' ), 'menu_name' => __( 'Ulaşım' ) ), 'rewrite' => array( 'slug' => 'ulasim', 'with_front' => false, 'hierarchical' => true ) ) );
	register_taxonomy( 'semt', array( 'konut', 'isyeri', 'trustik-tesis', 'arsa' ), array( 'hierarchical' => true, 'labels' => array( 'name' => _x( 'Semt', 'taxonomy general name' ), 'singular_name' => _x( 'Semt', 'taxonomy singular name' ), 'search_items' => __( 'Semt ara' ), 'all_items' => __( 'Tüm Semtler' ), 'edit_item' => __( 'Semt düzenle' ), 'update_item' => __( 'Semt güncelle' ), 'add_new_item' => __( 'Yeni Semt ekle' ), 'new_item_name' => __( 'Yeni Semt' ), 'menu_name' => __( 'Semt' ) ), 'rewrite' => array( 'slug' => 'semt', 'with_front' => false, 'hierarchical' => true ) ) );
}

function k_baslik() {
	$k_baslik = get_the_title(  );

	if (62 < strlen( $k_baslik )) {
		$k_baslik = mb_substr( $k_baslik, 0, 62 ) . '...';
	}

	echo $k_baslik;
}

function k_baslik2() {
	$k_baslik2 = get_the_title(  );

	if (50 < strlen( $k_baslik2 )) {
		$k_baslik2 = mb_substr( $k_baslik2, 0, 50 ) . '...';
	}

	echo $k_baslik2;
}

function fs_theme_footer() {
	echo '
' . fs_get_option( 'fs_tracking' ) . '
';
}

function fs_get_option($key) {
	global $fs_options;

	$fs_options = get_option( 'fs_options' );
	$fs_defaults = array( 'fs_logo' => get_bloginfo( 'template_url' ) . '/resimler/logo.png' );
	foreach ($fs_defaults as $k => $v) {

		if (!$fs_options[$k]) {
			$fs_options[$k] = $fs_defaults[$k];
			continue;
		}
	}

	return $fs_options[$key];
}

function fs_excerpt($devami = '', $devami2 = '') {
	global $post;

	if (function_exists( $devami )) {
		add_filter( 'excerpt_length', $devami );
	}


	if (function_exists( $devami2 )) {
		add_filter( 'excerpt_more', $devami2 );
	}

	$output = get_the_excerpt(  );
	$output = apply_filters( 'wptexturize', $output );
	$output = apply_filters( 'convert_chars', $output );
	$output = '' . $output . '';
	echo $output;
}

function fs_kisa_yazi100($length) {
	return 100;
}

function fs_kisa_yazi65($length) {
	return 65;
}

function fs_kisa_yazi55($length) {
	return 55;
}

function fs_kisa_yazi25($length) {
	return 25;
}

function fs_kisa_yazi15($length) {
	return 15;
}

function fs_kisa_yazi10($length) {
	return 10;
}

function fs_kisa_yazi5($length) {
	return 5;
}

function fs_kisalt($more) {
	return '...';
}

function wpbx_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	$commenter = get_comment_author_link(  );

	if (ereg( '<a[^>]* class=[^>]+>', $commenter )) {
		$commenter = ereg_replace( '(<a[^>]* class=[\'"]?)', 'url ', $commenter );
	}
	else {
		$commenter = ereg_replace( '(<a )/', 'class="url "', $commenter );
	}

	$avatarURL = $avatar_email = get_comment_author_email(  );
	str_replace( 'class=\'avatar', 'class=\'avatar', get_avatar( $avatar_email, 40, $default = $avatarURL . '/images/avatar.jpg' ) );
	$avatar = get_bloginfo( 'template_directory' );
	echo '	<li ';
	comment_class(  );
	echo ' id="comment-';
	comment_ID(  );
	echo '">
		<div id="div-comment-';
	comment_ID(  );
	echo '">
			<div class="comment-meta">
				<div class="comm-name">
					';
	echo $commenter;
	echo '				</div>
				<div class="comm-date">
					';
	printf( __( '%1$s' ), get_comment_date(  ), get_comment_time(  ), '#comment-' . get_comment_ID(  ) );
	echo '					<span class="reply-link">
						<span class="meta-sep">|</span> ';
	comment_reply_link( array_merge( $args, array( 'add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );
	echo '					</span>
					<span class="ratings">  ';

	if (function_exists( ckrating_display_karma )) {
		ckrating_display_karma(  );
	}

	echo '</span>
				</div>
			</div>
			<div class="comment-author vcard">
				';
	echo $avatar;
	echo '			</div>
			';

	if ($comment->comment_approved == '0') {
		_e( '					<span class=\'unapproved\'>Yorumunuz onay bekliyor.</span>
', 'wpbx' );
	}

	echo '
			<div class="comment-content">';
	comment_text(  );
	echo '</div>

		</div>
';
}

function wpbx_comments($file) {
	if (!function_exists( 'wp_list_comments' )) {
		$file = TEMPLATEPATH . '/comments.old.php';
	}

	return $file;
}

function comment_add_microid($classes) {
	$c_email = get_comment_author_email(  );
	$c_url = get_comment_author_url(  );

	if (( !empty( $$c_email ) && !empty( $$c_url ) )) {
		$microid = 'microid-mailto+http:sha1:' . sha1( sha1( 'mailto:' . $c_email ) . sha1( $c_url ) );
		$classes[] = $microid;
	}

	return $classes;
}

function kutu_listesi() {
	add_meta_box( 'post-meta-boxes', __( 'Emlak Seçenekleri', 'ayesoft' ), 'kutular', 'post', 'normal', 'high' );
	add_meta_box( 'post-meta-boxes', __( 'Emlak Seçenekleri', 'ayesoft' ), 'kutular2', 'konut', 'normal', 'high' );
	add_meta_box( 'post-meta-boxes', __( 'Emlak Seçenekleri', 'ayesoft' ), 'kutular3', 'isyeri', 'normal', 'high' );
	add_meta_box( 'post-meta-boxes', __( 'Emlak Seçenekleri', 'ayesoft' ), 'kutular4', 'arsa', 'normal', 'high' );
	add_meta_box( 'post-meta-boxes', __( 'Emlak Seçenekleri', 'ayesoft' ), 'kutular5', 'trustik-tesis', 'normal', 'high' );
	add_meta_box( 'post-meta-boxes', __( 'Manşet Seçenekleri', 'ayesoft' ), 'kutular6', 'manset', 'normal', 'high' );
}

function kutulari_getir() {
	$meta_boxes = array( 'galeri' => array( 'name' => 'galeri', 'title' => __( 'Resimler:', 'ayesoft' ), 'type' => 'galeri' ) );
	return apply_filters( 'kutulari_getir', $meta_boxes );
}

function kutulari_getir6() {
	$meta_boxes = array( 'url' => array( 'name' => 'url', 'title' => __( 'URL:', 'ayesoft' ), 'type' => 'input', 'desc' => 'Yönleneceği URL - Boş Bırakırsanız Manşet Konusuna Gider' ) );
	return apply_filters( 'kutulari_getir', $meta_boxes );
}

function kutulari_getir2() {
	$meta_boxes = array( 'galeri' => array( 'name' => 'galeri', 'title' => __( 'Resimler:', 'ayesoft' ), 'type' => 'galeri' ), 'ozel' => array( 'name' => 'ozel', 'title' => __( 'Özel Durum:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Özel Durum Yok', 'Krediye Uygun', 'Fiyatı Düştü', 'Acil Satılık', 'Acil Kiralık' ) ), 'fiyat' => array( 'name' => 'fiyat', 'title' => __( 'Fiyatı:', 'ayesoft' ), 'type' => 'fiyat' ), 'birim' => array( 'name' => 'birim', 'title' => __( 'Birim:', 'ayesoft' ), 'type' => 'birim', 'options' => array( 'Türk Lirası', 'Dolar', 'Euro', 'Sterlin' ) ), 'yapidurumu' => array( 'name' => 'yapidurumu', 'title' => __( 'Yapı Durumu:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Sıfır', 'İkinci El', 'İnşaat', 'Proje' ) ), 'yapitipi' => array( 'name' => 'yapitipi', 'title' => __( 'Yapı Tipi:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Betonarme', 'Prefabrik', 'Ahşap', 'Taş Yapı' ) ), 'isitma' => array( 'name' => 'isitma', 'title' => __( 'Isıtma:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Yok', 'Kombi', 'Kalorifer', 'Kat Kaloriferi', 'Soba', 'Klima', 'Diğer' ) ), 'yakit' => array( 'name' => 'yakit', 'title' => __( 'Yakıt:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Yok', 'Doğalgaz', 'Fuel-Oil', 'Elektrik', 'Kömür', 'Diğer' ) ), 'durum' => array( 'name' => 'durum', 'title' => __( 'Kullanım Durumu:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Boş', 'Kiracılı', 'İnşaat' ) ), 'ogrenci' => array( 'name' => 'ogrenci', 'title' => __( 'Öğrenciye/Bekara:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Hayır', 'Evet' ) ), 'takas' => array( 'name' => 'takas', 'title' => __( 'Takas:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Hayır', 'Evet' ) ), 'salon' => array( 'name' => 'salon', 'title' => __( 'Salon Sayısı:', 'ayesoft' ), 'type' => 'input' ), 'oda' => array( 'name' => 'oda', 'title' => __( 'Oda Sayısı:', 'ayesoft' ), 'type' => 'input' ), 'banyo' => array( 'name' => 'banyo', 'title' => __( 'Banyo Sayısı:', 'ayesoft' ), 'type' => 'input' ), 'm2' => array( 'name' => 'm2', 'title' => __( 'Metrekare:', 'ayesoft' ), 'type' => 'input' ), 'aidat' => array( 'name' => 'aidat', 'title' => __( 'Aidat:', 'ayesoft' ), 'type' => 'input' ), 'kira' => array( 'name' => 'kira', 'title' => __( 'Kira Getirisi:', 'ayesoft' ), 'type' => 'input' ), 'yas' => array( 'name' => 'yas', 'title' => __( 'Bina Yaşı:', 'ayesoft' ), 'type' => 'input' ), 'toplamkat' => array( 'name' => 'toplamkat', 'title' => __( 'Kat Sayısı:', 'ayesoft' ), 'type' => 'input' ), 'kat' => array( 'name' => 'kat', 'title' => __( 'Bulunduğu Kat:', 'ayesoft' ), 'type' => 'input' ), 'konum' => array( 'name' => 'konum', 'title' => __( 'Konum:', 'ayesoft' ), 'type' => 'harita' ) );
	return apply_filters( 'kutulari_getir', $meta_boxes );
}

function kutulari_getir3() {
	$meta_boxes = array( 'galeri' => array( 'name' => 'galeri', 'title' => __( 'Resimler:', 'ayesoft' ), 'type' => 'galeri' ), 'ozel' => array( 'name' => 'ozel', 'title' => __( 'Özel Durum:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Özel Durum Yok', 'Krediye Uygun', 'Fiyatı Düştü', 'Acil Satılık', 'Acil Kiralık' ) ), 'fiyat' => array( 'name' => 'fiyat', 'title' => __( 'Fiyatı:', 'ayesoft' ), 'type' => 'fiyat' ), 'birim' => array( 'name' => 'birim', 'title' => __( 'Birim:', 'ayesoft' ), 'type' => 'birim', 'options' => array( 'Türk Lirası', 'Dolar', 'Euro', 'Sterlin' ) ), 'isitma' => array( 'name' => 'isitma', 'title' => __( 'Isıtma:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Yok', 'Kombi', 'Kalorifer', 'Kat Kaloriferi', 'Soba', 'Klima', 'Diğer' ) ), 'yakit' => array( 'name' => 'yakit', 'title' => __( 'Yakıt:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Yok', 'Doğalgaz', 'Fuel-Oil', 'Elektrik', 'Kömür', 'Diğer' ) ), 'durum' => array( 'name' => 'durum', 'title' => __( 'Kullanım Durumu:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Boş', 'Kiracılı', 'İnşaat' ) ), 'aidat' => array( 'name' => 'aidat', 'title' => __( 'Aidat:', 'ayesoft' ), 'type' => 'input' ), 'oda' => array( 'name' => 'oda', 'title' => __( 'Oda Sayısı:', 'ayesoft' ), 'type' => 'input' ), 'm2' => array( 'name' => 'm2', 'title' => __( 'Metrekare:', 'ayesoft' ), 'type' => 'input' ), 'yas' => array( 'name' => 'yas', 'title' => __( 'Bina Yaşı:', 'ayesoft' ), 'type' => 'input' ), 'toplamkat' => array( 'name' => 'toplamkat', 'title' => __( 'Kat Sayısı:', 'ayesoft' ), 'type' => 'input' ), 'kat' => array( 'name' => 'kat', 'title' => __( 'Bulunduğu Kat:', 'ayesoft' ), 'type' => 'input' ), 'takas' => array( 'name' => 'takas', 'title' => __( 'Takas:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Hayır', 'Evet' ) ), 'konum' => array( 'name' => 'konum', 'title' => __( 'Konum:', 'ayesoft' ), 'type' => 'harita', 'options' => array( 'Hayır', 'Evet' ) ) );
	return apply_filters( 'kutulari_getir', $meta_boxes );
}

function kutulari_getir4() {
	$meta_boxes = array( 'galeri' => array( 'name' => 'galeri', 'title' => __( 'Resimler:', 'ayesoft' ), 'type' => 'galeri' ), 'ozel' => array( 'name' => 'ozel', 'title' => __( 'Özel Durum:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Özel Durum Yok', 'Krediye Uygun', 'Fiyatı Düştü', 'Acil Satılık', 'Acil Kiralık' ) ), 'm2' => array( 'name' => 'm2', 'title' => __( 'm2:', 'ayesoft' ), 'type' => 'input' ), 'fiyat' => array( 'name' => 'fiyat', 'title' => __( 'm2 Fiyatı:', 'ayesoft' ), 'type' => 'fiyat' ), 'birim' => array( 'name' => 'birim', 'title' => __( 'Birim:', 'ayesoft' ), 'type' => 'birim', 'options' => array( 'Türk Lirası', 'Dolar', 'Euro', 'Sterlin' ) ), 'adano' => array( 'name' => 'adano', 'title' => __( 'Ada No:', 'ayesoft' ), 'type' => 'input' ), 'parselno' => array( 'name' => 'parselno', 'title' => __( 'Parsel No:', 'ayesoft' ), 'type' => 'input' ), 'paftano' => array( 'name' => 'paftano', 'title' => __( 'Pafta No:', 'ayesoft' ), 'type' => 'input' ), 'gabari' => array( 'name' => 'gabari', 'title' => __( 'Gabari:', 'ayesoft' ), 'type' => 'input' ), 'kaks' => array( 'name' => 'kaks', 'title' => __( 'KAKS (emsal):', 'ayesoft' ), 'type' => 'input' ), 'tapu' => array( 'name' => 'tapu', 'title' => __( 'Tapu Durumu:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Hisseli Tapu', 'Mustakil Parsel', 'Tahsis', 'Zilliyet' ) ), 'takas' => array( 'name' => 'takas', 'title' => __( 'Takas:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Hayır', 'Evet' ) ), 'konum' => array( 'name' => 'konum', 'title' => __( 'Konum:', 'ayesoft' ), 'type' => 'harita', 'options' => array( 'Hayır', 'Evet' ) ) );
	return apply_filters( 'kutulari_getir', $meta_boxes );
}

function kutulari_getir5() {
	$meta_boxes = array( 'galeri' => array( 'name' => 'galeri', 'title' => __( 'Resimler:', 'ayesoft' ), 'type' => 'galeri' ), 'ozel' => array( 'name' => 'ozel', 'title' => __( 'Özel Durum:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Özel Durum Yok', 'Krediye Uygun', 'Fiyatı Düştü', 'Acil Satılık', 'Acil Kiralık' ) ), 'fiyat' => array( 'name' => 'fiyat', 'title' => __( 'Fiyatı:', 'ayesoft' ), 'type' => 'fiyat' ), 'birim' => array( 'name' => 'birim', 'title' => __( 'Birim:', 'ayesoft' ), 'type' => 'birim', 'options' => array( 'Türk Lirası', 'Dolar', 'Euro', 'Sterlin' ) ), 'yapidurumu' => array( 'name' => 'yapidurumu', 'title' => __( 'Yapı Durumu:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Sıfır', 'İkinci El', 'İnşaat', 'Proje' ) ), 'yildiz' => array( 'name' => 'yildiz', 'title' => __( 'Yıldız Sayısı:', 'ayesoft' ), 'type' => 'input' ), 'oda' => array( 'name' => 'oda', 'title' => __( 'Oda Sayısı:', 'ayesoft' ), 'type' => 'input' ), 'yatak' => array( 'name' => 'yatak', 'title' => __( 'Yatak Sayısı:', 'ayesoft' ), 'type' => 'input' ), 'am2' => array( 'name' => 'am2', 'title' => __( 'Açık Alan m2:', 'ayesoft' ), 'type' => 'input' ), 'km2' => array( 'name' => 'km2', 'title' => __( 'Kapalı Alan m2:', 'ayesoft' ), 'type' => 'input' ), 'yas' => array( 'name' => 'yas', 'title' => __( 'Bina Yaşı:', 'ayesoft' ), 'type' => 'input' ), 'toplamkat' => array( 'name' => 'toplamkat', 'title' => __( 'Kat Sayısı:', 'ayesoft' ), 'type' => 'input' ), 'getiri' => array( 'name' => 'getiri', 'title' => __( 'Yıllık Getiri:', 'ayesoft' ), 'type' => 'input' ), 'etud' => array( 'name' => 'etud', 'title' => __( 'Zemin Etüdü:', 'ayesoft' ), 'type' => 'input' ), 'takas' => array( 'name' => 'takas', 'title' => __( 'Takas:', 'ayesoft' ), 'type' => 'radio', 'options' => array( 'Hayır', 'Evet' ) ), 'konum' => array( 'name' => 'konum', 'title' => __( 'Konum:', 'ayesoft' ), 'type' => 'harita', 'options' => array( 'Hayır', 'Evet' ) ) );
	return apply_filters( 'kutulari_getir', $meta_boxes );
}

function kutular() {
	global $post;

	$meta_boxes = kutulari_getir(  );
	echo '	<table class="form-table">
	';
	foreach ($meta_boxes as $meta) {
		$value = get_post_meta( $post->ID, $meta['name'], true );

		if ($meta['type'] == 'radio') {
			radio_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'select') {
			select_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'input') {
			kutu_yazi( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'harita') {
			harita( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'galeri') {
			galeri( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'fiyat') {
			kutu_fiyat( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'birim') {
			kutu_birim( $meta, $value );
			continue;
		}
	}

	echo '	</table>
';
}

function kutular6() {
	global $post;

	
	$meta_boxes = kutulari_getir6(  );
	echo '<style>
.form-table-ayesoft th {text-align:left;}
.form-table-ayesoft th, .form-table-ayesoft td{padding:4px;}
</style>
	<table class="form-table-ayesoft">
	';
	foreach ($meta_boxes as $meta) {
		$value = get_post_meta( $post->ID, $meta['name'], true );

		if ($meta['type'] == 'radio') {
			radio_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'select') {
			select_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'input') {
			kutu_yazi( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'text') {
			kutu_textarea( $meta, $value );
			continue;
		}
	}

	echo '	</table>
';
}

function kutular2() {
	global $post;

	
	$meta_boxes = kutulari_getir2(  );
	echo '	<table class="form-table">
	';
	foreach ($meta_boxes as $meta) {
		$value = get_post_meta( $post->ID, $meta['name'], true );

		if ($meta['type'] == 'radio') {
			radio_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'select') {
			select_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'input') {
			kutu_yazi( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'harita') {
			harita( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'galeri') {
			galeri( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'fiyat') {
			kutu_fiyat( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'birim') {
			kutu_birim( $meta, $value );
			continue;
		}
	}

	echo '	</table>
';
}

function kutular3() {
	global $post;

	
	$meta_boxes = kutulari_getir3(  );
	echo '	<table class="form-table">
	';
	foreach ($meta_boxes as $meta) {
		$value = get_post_meta( $post->ID, $meta['name'], true );

		if ($meta['type'] == 'radio') {
			radio_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'select') {
			select_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'input') {
			kutu_yazi( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'harita') {
			harita( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'galeri') {
			galeri( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'fiyat') {
			kutu_fiyat( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'birim') {
			kutu_birim( $meta, $value );
			continue;
		}
	}

	echo '	</table>
';
}

function kutular4() {
	global $post;

	
	$meta_boxes = kutulari_getir4(  );
	echo '	<table class="form-table">
	';
	foreach ($meta_boxes as $meta) {
		$value = get_post_meta( $post->ID, $meta['name'], true );

		if ($meta['type'] == 'radio') {
			radio_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'select') {
			select_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'input') {
			kutu_yazi( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'harita') {
			harita( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'galeri') {
			galeri( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'fiyat') {
			kutu_fiyat( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'birim') {
			kutu_birim( $meta, $value );
			continue;
		}
	}

	echo '	</table>
';
}

function kutular5() {
	global $post;

	
	$meta_boxes = kutulari_getir5(  );
	echo '	<table class="form-table">
	';
	foreach ($meta_boxes as $meta) {
		$value = get_post_meta( $post->ID, $meta['name'], true );

		if ($meta['type'] == 'radio') {
			radio_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'select') {
			select_ekrana_yaz( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'input') {
			kutu_yazi( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'harita') {
			harita( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'galeri') {
			galeri( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'fiyat') {
			kutu_fiyat( $meta, $value );
			continue;
		}


		if ($meta['type'] == 'birim') {
			kutu_birim( $meta, $value );
			continue;
		}
	}

	echo '	</table>
';
}

function select_ekrana_yaz($args = array(  ), $value = false) {
	extract( $args );
	echo '	<tr>
		<th style="width:20%;">
			<label for="';
	echo $name;
	echo '">';
	echo $title;
	echo '</label>
		</th>
		<td>
			<select style="width:120px;" name="';
	echo $name;
	echo '" id="';
	echo $name;
	echo '">
			';
	foreach ($options as $option) {
		echo '				<option ';

		if (htmlentities( $value, ENT_QUOTES ) == $option) {
			echo ' selected="selected"';
		}

		echo '>
					';
		echo $option;
		echo '				</option>
			';
	}

	echo '			</select>
			<input type="hidden" name="';
	echo $name;
	echo '_noncename" id="';
	echo $name;
	echo '_noncename" value="';
	echo wp_create_nonce( plugin_basename( __FILE__ ) );
	echo '" />
		</td>
	</tr>
	';
}

function radio_ekrana_yaz($args = array(  ), $value = false) {
	extract( $args );
	echo '	<tr>
		<th style="width:20%;">
			<label for="';
	echo $name;
	echo '">';
	echo $title;
	echo '</label>
		</th>
		<td>
			';
	$i = 5;
	foreach ($options as $option) {
		++$i;
		echo '				<div style="float:left;padding:4px 2px;"> <input type="radio" name="';
		echo $name;
		echo '" id="';
		echo $name;
		echo '-';
		echo $i;
		echo '" value="';
		echo $i;
		echo '" ';

		if (( htmlentities( $value, ENT_QUOTES ) == $i || ( htmlentities( $value, ENT_QUOTES ) == '' && $i == 1 ) )) {
			echo 'checked=""';
		}

		echo ' > <label for="';
		echo $name;
		echo '-';
		echo $i;
		echo '">';
		echo $option;
		echo '</label></div>
			';
	}

	echo '			<input type="hidden" name="';
	echo $name;
	echo '_noncename" id="';
	echo $name;
	echo '_noncename" value="';
	echo wp_create_nonce( plugin_basename( __FILE__ ) );
	echo '" />
		</td>
	</tr>
	';
}

function galeri($args = array(  ), $value = false) {
	extract( $args );
	echo '	<tr>
		<th style="width:20%;">
			<label for="';
	echo $name;
	echo '">';
	echo $title;
	echo '</label>
		</th>
		<td>
		<style>
		#product_images_container{
			width:100%;
		}
		ul.product_images li.image{
			width: 80px;
			float: left;
			cursor: move;
			margin: 9px 9px 0px 0px;
			position: relative;
			-moz-box-sizing: border-box;
		}
		ul.product_images li.image a.delete{
			text-indent:-99999;
			border:1px #111 solid;
			background:#333;
			width:12px;
			height:12px;
			position:absolute;
			font-size:11px;
			line-height:9px;
			top:-6px;
			right:-8px;
			display:none;
			border-radius:8px;
			overflow:hidden;
			color:#fff;
			text-align:center;
			text-decoration:none;
			z-index:9999;
		}
		ul.product_images li.image:hover a.delete{
			display:block;
		}
		.wc-metabox-sortable-placeholder
		{
			width: 84px;
			height: 84px;
			float: left;
			cursor: move;
			margin: 9px 9px 0px 0px;
			position: relative;
			-moz-box-sizing: border-box;
			border:2px #ccc dashed;
		}
		ul.product_images li.image img{
			width: 100%;
			height:80px;
			display: block;
			cursor: move;
			border: 1px solid #D5D5D5;
			background: none repeat scroll 0% 0% #F7F7F7;
			border-radius: 2px;
			padding:2px;
		}
		.add_product_images{
			clear:both;
		}
		</style>
		<script>
		jQuery( function($){

			// Product gallery file uploads
			var product_gallery_frame;
			var $image_gallery_ids = $(\'#galeri\');
			var $product_images = $(\'#product_images_container ul.product_images\');

			jQuery(\'.add_product_images\').on( \'click\', \'a\', function( event ) {
				var $el = $(this);
				var attachment_ids = $image_gallery_ids.val();

				event.preventDefault();

				// If the media frame already exists, reopen it.
				if ( product_gallery_frame ) {
					product_gallery_frame.open();
					return;
				}

				// Create the media frame.
				product_gallery_frame = wp.media.frames.product_gallery = wp.media({
					// Set the title of the modal.
					title: $el.data(\'choose\'),
					button: {
						text: $el.data(\'update\'),
					},
					states : [
						new wp.media.controller.Library({
							title: $el.data(\'choose\'),
							filterable :	\'all\',
							multiple: true,
						})
					]
				});

				// When an image is selected, run a callback.
				product_gallery_frame.on( \'select\', function() {

					var selection = product_gallery_frame.state().get(\'selection\');

					selection.map( function( attachment ) {

						attachment = attachment.toJSON();

						if ( attachment.id ) {
							attachment_ids = attachment_ids ? attachment_ids + "," + attachment.id : attachment.id;

							$product_images.append(\'\
								<li class="image" data-attachment_id="\' + attachment.id + \'">\
									<img src="\' + attachment.url + \'" />\
									<ul class="actions">\
										<li><a href="#" class="delete" title="\' + $el.data(\'delete\') + \'">x</a></li>\
									</ul>\
								</li>\');
							}

						});

						$image_gallery_ids.val( attachment_ids );
					});

					// Finally, open the modal.
					product_gallery_frame.open();
				});

				// Image ordering
				$product_images.sortable({
					items: \'li.image\',
					cursor: \'move\',
					scrollSensitivity:40,
					forcePlaceholderSize: true,
					forceHelperSize: false,
					helper: \'clone\',
					opacity: 0.65,
					placeholder: \'wc-metabox-sortable-placeholder\',
					start:function(event,ui){
						ui.item.css(\'background-color\',\'#f6f6f6\');
					},
					stop:function(event,ui){
						ui.item.removeAttr(\'style\');
					},
					update: function(event, ui) {
						var attachment_ids = \'\';

						$(\'#product_images_container ul li.image\').css(\'cursor\',\'default\').each(function() {
							var attachment_id = jQuery(this).attr( \'data-attachment_id\' );
							attachment_ids = attachment_ids + attachment_id + \',\';
						});

						$image_gallery_ids.val( attachment_ids );
					}
				});

				// Remove images
				$(\'#product_images_container\').on( \'click\', \'a.delete\', function() {
					$(this).closest(\'li.image\').remove();

					var attachment_ids = \'\';

					$(\'#product_images_container ul li.image\').css(\'cursor\',\'default\').each(function() {
						var attachment_id = jQuery(this).attr( \'data-attachment_id\' );
						attachment_ids = attachment_ids + attachment_id + \',\';
					});

					$image_gallery_ids.val( attachment_ids );

					return false;
				});
		});
		</script>
		<div id="product_images_container">
			<ul class="product_images">
				';

	if (wp_specialchars( $value, 1 )) {
		$product_image_gallery = wp_specialchars( $value, 1 );
	}
	else {
		$attachment_ids = array_diff( $attachment_ids, array( get_post_thumbnail_id(  ) ) );
		$attachment_ids = get_posts( 'post_parent=' . $post->ID . '&numberposts=-1&post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&fields=ids&meta_key=_woocommerce_exclude_image&meta_value=0' );
		$product_image_gallery = implode( ',', $attachment_ids );
	}

	$attachments = array_filter( explode( ',', $product_image_gallery ) );

	if ($attachments) {
		foreach ($attachments as $attachment_id) {
			echo '<li class="image" data-attachment_id="' . esc_attr( $attachment_id ) . '">
								' . wp_get_attachment_image( $attachment_id, 'thumbnail' ) . '
								<ul class="actions">
									<li><a href="#" class="delete tips" data-tip="' . __( 'Resmi Sil', 'woocommerce' ) . '">x</a></li>
								</ul>
							</li>';
		}
	}

	echo '			</ul>
			<input type="hidden" class="fors-upload" name="';
	echo $name;
	echo '" id="';
	echo $name;
	echo '" value="';
	echo wp_specialchars( $value, 1 );
	echo '" size="30" tabindex="30" style="width: 400px;margin-top:-3px;" />
			<input type="hidden" name="';
	echo $name;
	echo '_noncename" id="';
	echo $name;
	echo '_noncename" value="';
	echo wp_create_nonce( plugin_basename( __FILE__ ) );
	echo '" />
			';
	echo '
		</div>
		<p class="add_product_images hide-if-no-js">
			<a href="#" data-choose="';
	_e( 'Yeni Resim Ekle', 'woocommerce' );
	echo '" data-update="';
	_e( 'Galeriye Ekle', 'woocommerce' );
	echo '" data-delete="';
	_e( 'Resmi Sil', 'woocommerce' );
	echo '" data-text="';
	_e( 'Sil', 'woocommerce' );
	echo '">';
	_e( 'Galeriye Resim Ekle', 'woocommerce' );
	echo '</a>
		</p>
		</td>
	</tr>
	';
}

function harita($args = array(  ), $value = false) {
	extract( $args );
	echo '	<tr>
		<th style="width:20%;">
			<label for="';
	echo $name;
	echo '">';
	echo $title;
	echo '</label>
		</th>
		<td>
			<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
			';

	if (wp_specialchars( $value, 1 )) {
		$konum = wp_specialchars( $value, 1 );
	}
	else {
		$konum = '39.90153482884412, 32.8603515625';
	}

	echo '			<script type="text/javascript">
				  function initialize() {
					var mapOptions = {
					  zoom: 7,
					  center: new google.maps.LatLng(';
	echo $konum;
	echo '),
					  mapTypeId: google.maps.MapTypeId.ROADMAP
					}
					var map = new google.maps.Map(document.getElementById(\'map_canvas\'), mapOptions);

						var cent = new google.maps.LatLng(';
	echo $konum;
	echo ');
						var marker = new google.maps.Marker({
							  position: cent,
							  map: map,
							  draggable: true
						});

						google.maps.event.addListener(marker, \'dragend\', function(event) {
							var lat = event.latLng.lat();
							var lng = event.latLng.lng();
							var elem = document.getElementById("';
	echo $name;
	echo '");
							elem.value = lat+\', \'+lng;
						});

						/*google.maps.event.addListener(marker, "dragend", function(event){
							var lat = event.latLng.lat();
							var lng = event.latLng.lng();
							$("#';
	echo $name;
	echo '").val(lat+\', \'+lng);
							alert(lat+\', \'+lng);
						});*/
					map.checkResize();
					}
					google.maps.event.addDomListener(window, \'load\', initialize);
			</script>
			<div id="map_canvas" style="width:100%;height:300px;border:#96cae3 1px solid;"></div>
			Eğer Harita Tam Olarak Görüntülenemiyorsa ve İmleci Doğru Olarak Düzenleyemiyorsanız Lütfen Sayfayı Yenileyerek Tekrar Deneyiniz.
			';

	if ($value) {
		$mevcut = wp_specialchars( $value, 1 );
	}
	else {
		$mevcut = '39.9015, 32.8603';
	}

	echo '			<input type="hidden" class="fors-upload" name="';
	echo $name;
	echo '" id="';
	echo $name;
	echo '" value="';
	echo $mevcut;
	echo '" size="30" tabindex="30" style="width: 400px;margin-top:-3px;" />
			<input type="hidden" name="';
	echo $name;
	echo '_noncename" id="';
	echo $name;
	echo '_noncename" value="';
	echo wp_create_nonce( plugin_basename( __FILE__ ) );
	echo '" />
		</td>
	</tr>
	';
}

function kutu_yazi($args = array(  ), $value = false) {
	extract( $args );
	echo '	<tr>
		<th style="width:20%;">
			<label for="';
	echo $name;
	echo '">';
	echo $title;
	echo '</label>
		</th>
		<td>
			<input type="text" class="fors-upload" name="';
	echo $name;
	echo '" id="';
	echo $name;
	echo '" value="';
	echo wp_specialchars( $value, 1 );
	echo '" size="30" tabindex="30" style="width: 400px;margin-top:-3px;" />
			<input type="hidden" name="';
	echo $name;
	echo '_noncename" id="';
	echo $name;
	echo '_noncename" value="';
	echo wp_create_nonce( plugin_basename( __FILE__ ) );
	echo '" />
			<br />
			<p class="description">';
	echo $desc;
	echo '</p>
		</td>
	</tr>
	';
}

function kutu_fiyat($args = array(  ), $value = false) {
	extract( $args );
	echo '	<tr>
		<th style="width:20%;">
			<label for="';
	echo $name;
	echo '">';
	echo $title;
	echo '</label>
		</th>
		<td>
			<input type="text" class="fors-upload" name="';
	echo $name;
	echo '" id="';
	echo $name;
	echo '" value="';
	echo wp_specialchars( $value, 1 );
	echo '" size="30" tabindex="30" style="width: 200px;margin-top:-3px;margin-right:10px;float:left;" />
			<input type="hidden" name="';
	echo $name;
	echo '_noncename" id="';
	echo $name;
	echo '_noncename" value="';
	echo wp_create_nonce( plugin_basename( __FILE__ ) );
	echo '" />
	';
}

function kutu_birim($args = array(  ), $value = false) {
	extract( $args );
	echo '
			<select style="width:120px;float:left;margin-top:-3px;" name="';
	echo $name;
	echo '" id="';
	echo $name;
	echo '">
			';
	foreach ($options as $option) {
		echo '				<option ';

		if (htmlentities( $value, ENT_QUOTES ) == $option) {
			echo ' selected="selected"';
		}

		echo '>
					';
		echo $option;
		echo '				</option>
			';
	}

	echo '			</select>
			<input type="hidden" name="';
	echo $name;
	echo '_noncename" id="';
	echo $name;
	echo '_noncename" value="';
	echo wp_create_nonce( plugin_basename( __FILE__ ) );
	echo '" />
		</td>
	</tr>
	';
}

function kutu_textarea($args = array(  ), $value = false) {
	extract( $args );
	echo '	<tr>
		<th style="width:20%;">
			<label for="';
	echo $name;
	echo '">';
	echo $title;
	echo '</label>
		</th>
		<td>
			<textarea name="';
	echo $name;
	echo '" id="';
	echo $name;
	echo '" cols="60" rows="4" tabindex="30" style="width: 97%;margin-top:-3px;">';
	echo wp_specialchars( $value, 1 );
	echo '</textarea>
			<input type="hidden" name="';
	echo $name;
	echo '_noncename" id="';
	echo $name;
	echo '_noncename" value="';
	echo wp_create_nonce( plugin_basename( __FILE__ ) );
	echo '" />
			<p class="description">';
	echo $desc;
	echo '</p>
		</td>
	</tr>
	';
}

function kutulari_kaydet($post_id) {
	global $post;

	if ('konut' == $_POST['post_type']) {
		$meta_boxes = array_merge( kutulari_getir2(  ) );
	}
	else {
		if ('isyeri' == $_POST['post_type']) {
			$meta_boxes = array_merge( kutulari_getir3(  ) );
		}
		else {
			if ('arsa' == $_POST['post_type']) {
				$meta_boxes = array_merge( kutulari_getir4(  ) );
			}
			else {
				if ('trustik-tesis' == $_POST['post_type']) {
					$meta_boxes = array_merge( kutulari_getir5(  ) );
				}
				else {
					if ('manset' == $_POST['post_type']) {
						$meta_boxes = array_merge( kutulari_getir6(  ) );
					}
					else {
						$meta_boxes = array_merge( kutulari_getir(  ) );
					}
				}
			}
		}
	}

	foreach ($meta_boxes as $meta_box) {

		if (!wp_verify_nonce( $_POST[$meta_box['name'] . '_noncename'], plugin_basename( __FILE__ ) )) {
			return $post_id;
		}

		$data = stripslashes( $_POST[$meta_box['name']] );

		if (get_post_meta( $post_id, $meta_box['name'] ) == '') {
			add_post_meta( $post_id, $meta_box['name'], $data, true );
			continue;
		}


		if ($data != get_post_meta( $post_id, $meta_box['name'], true )) {
			update_post_meta( $post_id, $meta_box['name'], $data );
			continue;
		}


		if ($data == '') {
			delete_post_meta( $post_id, $meta_box['name'], get_post_meta( $post_id, $meta_box['name'], true ) );
			continue;
		}
	}

}

function semtgetir() {
	$myterm = 'semt';
	$heTerm = 'semt';
	$terms = get_the_terms( $post->ID, $myterm );

	if ($terms) {
		$count = count( $terms );
		foreach ($terms as $term) {

			if (0 == $term->parent) {
				$parentsItems[] = $term;
			}


			if ($term->parent) {
				$childItems[] = $term;
				continue;
			}
		}


		if (is_taxonomy_hierarchical( $heTerm )) {
			if ($parentsItems) {
				foreach ($parentsItems as $parentsItem) {
					echo $parentsItem->name;

					if ($childItems) {
						foreach ($childItems as $childItem) {

							if ($childItem->parent == $parentsItem->term_id) {
								echo ' \\ ' . $childItem->name;
								foreach ($childItems as $child) {

									if ($child->parent == $childItem->term_id) {
										echo ' \\ ' . $child->name;
										continue;
									}
								}

								continue;
							}
						}

						continue;
					}
				}

				return null;
			}
		}
		else {
			foreach ($parentsItems as $parentsItem) {
				echo $parentsItem->name;
			}
		}
	}

}

function listegetir($type) {
	$terms = get_terms( 'tip', 'orderby=count' );
	$count = count( $terms );

	if (0 < $count) {
		echo '<ul>';
		foreach ($terms as $term) {

			if (0 == $term->parent) {
				$parentsItems[] = $term;
			}


			if ($term->parent) {
				$childItems[] = $term;
				continue;
			}
		}

		foreach ($parentsItems as $parentsItem) {
			echo '<li>' . $parentsItem->name . '(' . konusayisi( $parentsItem->slug, $type ) . ')<ul>';
			foreach ($childItems as $childItem) {

				if ($childItem->parent == $parentsItem->term_id) {
					echo '<li>' . $childItem->name . ' (' . konusayisi( $childItem->slug, $type ) . ')</li>';
					continue;
				}
			}

			echo '</ul></li>';
		}

		echo '</ul>';
	}

}

function konusayisi($term, $type) {
	$que = new WP_Query( array( 'tur' => $type, 'tip' => $term ) );
	return $que->post_count;
}

function kredidokumu($toplam, $oran, $vade, $tur) {
	$taksit = $toplam / $vade;
	$oran = $oran / 100;
	$ay = intval( date( 'n' ) );
	$yil = intval( date( 'Y' ) );

	if ($tur == 1) {
		$kkdforan = 0.149999999999999994448885;
		$bsmvoran = 0.0500000000000000027755576;
	}
	else {
		$kkdforan = 5;
		$bsmvoran = 5;
	}

	echo '<div style="float:left;padding:4px;width:20px;font-weight:bold;margin:0px 2px 2px 0px;border:1px #0099FF solid;background:#5EDFFF;font-size:10px;">Sıra</div>
		  <div style="float:left;padding:4px;width:80px;font-weight:bold;margin:0px 2px 2px 0px;border:1px #0099FF solid;background:#5EDFFF;font-size:10px;">Tarih</div>
		  <div style="float:left;padding:4px;width:80px;font-weight:bold;margin:0px 2px 2px 0px;border:1px #0099FF solid;background:#5EDFFF;font-size:10px;">Faiz</div>
		  <div style="float:left;padding:4px;width:69px;font-weight:bold;margin:0px 2px 2px 0px;border:1px #0099FF solid;background:#5EDFFF;font-size:10px;">KKDF</div>
		  <div style="float:left;padding:4px;width:69px;font-weight:bold;margin:0px 2px 2px 0px;border:1px #0099FF solid;background:#5EDFFF;font-size:10px;">BSMV</div>
		  <div style="float:left;padding:4px;width:80px;font-weight:bold;margin:0px 2px 2px 0px;border:1px #0099FF solid;background:#5EDFFF;font-size:10px;">Ödenen</div>
		  <div style="float:left;padding:4px;width:80px;font-weight:bold;margin:0px 2px 2px 0px;border:1px #0099FF solid;background:#5EDFFF;font-size:10px;">Taksit</div>
		  <div style="float:left;padding:4px;width:96px;font-weight:bold;margin:0px 2px 2px 0px;border:1px #0099FF solid;background:#5EDFFF;font-size:10px;">Kalan Borç</div>';
	echo '<div style="clear:both;"></div>';
	$i = 6;

	while ($i <= $vade) {
		$faiz = $toplam * $oran;
		$kkdf = $faiz * $kkdforan;
		$bsmv = $faiz * $bsmvoran;
		$odenen = $taksit - $faiz - $kkdf - $bsmv;
		$toplam = $toplam - $taksit;

		if ($i % 2 == 0) {
			$bgg = 'background:#FFFFCC;';
		}
		else {
			$bgg = '';
		}

		++$ay;

		if ($ay == 13) {
			++$yil;
			$ay = 6;
		}


		if ($ay < 10) {
			$ayy = '0' . $ay;
		}
		else {
			$ayy = $ayy;
		}

		echo '<div style="float:left;padding:4px;width:20px;margin:0px 4px 2px 0px;border-bottom: 1px dotted rgb(189, 189, 189);' . $bgg . '">' . $i . '</div>';
		echo '<div style="float:left;padding:4px;width:80px;margin:0px 4px 2px 0px;border-bottom: 1px dotted rgb(189, 189, 189);' . $bgg . '">' . $ayy . '.' . $yil . '</div>';
		echo '<div style="float:left;padding:4px;width:80px;margin:0px 4px 2px 0px;border-bottom: 1px dotted rgb(189, 189, 189);' . $bgg . '">' . number_format( $faiz, 2, '.', ',' ) . '</div>';
		echo '<div style="float:left;padding:4px;width:69px;margin:0px 4px 2px 0px;border-bottom: 1px dotted rgb(189, 189, 189);' . $bgg . '">' . number_format( $kkdf, 2, '.', ',' ) . '</div>';
		echo '<div style="float:left;padding:4px;width:69px;margin:0px 4px 2px 0px;border-bottom: 1px dotted rgb(189, 189, 189);' . $bgg . '">' . number_format( $bsmv, 2, '.', ',' ) . '</div>';
		echo '<div style="float:left;padding:4px;width:80px;margin:0px 4px 2px 0px;border-bottom: 1px dotted rgb(189, 189, 189);' . $bgg . '">' . number_format( $odenen, 2, '.', ',' ) . '</div>';
		echo '<div style="float:left;padding:4px;width:80px;margin:0px 4px 2px 0px;border-bottom: 1px dotted rgb(189, 189, 189);' . $bgg . '">' . number_format( $taksit, 2, '.', ',' ) . '</div>';
		echo '<div style="float:left;padding:4px;width:96px;margin:0px 4px 2px 0px;border-bottom: 1px dotted rgb(189, 189, 189);' . $bgg . '">' . number_format( $toplam, 2, '.', ',' ) . '</div>';
		echo '<div style="clear:both;"></div>';
		++$i;
	}

}

function kredi($toplam, $oran, $vade, $tur, $geri) {
	$oran = $oran / 100;

	if ($tur == 1) {
		$vergi = 0.20000000000000001110223;
	}
	else {
		$vergi = 4;
	}

	$oran2 = $oran + $oran * $vergi;
	$donus = $toplam * $oran2 / ( 1 - 1 / ck( 1 + $oran2, $vade ) );

	if ($geri == 1) {
		$donus = $donus * $vade;
	}

	return $donus;
}

function ck($hes, $tur) {
	$sonuc = $hes;
	$i = 6;

	while ($i < $tur) {
		$sonuc = $sonuc * $hes;
		++$i;
	}

	return $sonuc;
}

function kurgetir() {
	$dosya = TEMPLATEPATH . '/doviz_onbellek/doviz.html';

	if (file_exists( $dosya )) {
		$zaman = time(  ) - filemtime( $dosya );
	}
	else {
		$zaman = 600 + 100;
	}


	if (file_exists( $dosya )) {
		if (date( 'j', filemtime( $dosya ) ) == date( 'j' )) {
			$ttt = 7;
		}
		else {
			$ttt = 8;
		}
	}
	else {
		$ttt = 8;
	}


	if (( 600 <= $zaman || !file_exists( $dosya ) )) {
		$para_birimix[] = 'USD';
		$para_birimix[] = 'EUR';
		$para_birimix[] = 'GBP';
		$veri = array( 'Isim' => 'isim', 'forexbuying' => 'Alis', 'forexselling' => 'Satis' );
		$data = file_get_contents( 'http://www.tcmb.gov.tr/kurlar/today.xml' );
		$veri2 = '<div class="price-box">';

		if ($data) {
			$data = iconv( 'ISO-8859-9', 'UTF-8', $data );
			$satir = explode( '<Currency Kod=', $data );
			$satir_s = count( $satir );
			$para_s = count( $para_birimix );
			$tespit = 6;
			$i = 7;

			while ($i < $satir_s) {
				$veri = $satir[$i];
				foreach ($para_birimix as $para_kod) {
					$kod = substr( $veri, 1, 3 );

					if ($kod == $para_kod) {
						$para_kod = strtolower( $para_kod );
						$adi = f( '<Isim>', '</Isim>', $veri );

						if ($adi) {
							if ($i == 1) {
								$ad = 'Dolar';
								$sym = '$';
							}
							else {
								if ($i == 12) {
									$ad = 'Euro';
									$sym = '€';
								}
								else {
									$ad = 'Sterlin';
									$sym = '£';
								}
							}

							$eskia = file_get_contents( get_bloginfo( 'template_url' ) . '/doviz_onbellek/' . $ad . '-eski.html', true );
							$eskial = floatval( $eskia );
							$alis = f( '<ForexBuying>', '</ForexBuying>', $veri );
							$satis = f( '<ForexSelling>', '</ForexSelling>', $veri );

							if ($eskial < $alis) {
								$cll = 'fa-angle-double-up';
							}
							else {
								if ($alis < $eskial) {
									$cll = 'fa-angle-double-down';
								}
								else {
									$cll = 'fa-angle-double-right';
								}
							}

							$veri2 .= '<div class="doller">
											<span class="symbol">' . $sym . '</span>
											<span class="price-text">' . $ad . ' : ' . $satis . '</span>
											<a href="#" class="arrow"><i class="fa ' . $cll . '"></i></a>
										</div>';

							if ($ttt == 2) {
								$eskial = file_get_contents( get_bloginfo( 'template_url' ) . '/doviz_onbellek/' . $ad . '.html', true );
								$dosya2 = TEMPLATEPATH . '/doviz_onbellek/' . $ad . '-eski.html';
								$ac = fopen( $dosya2, 'w+' );
								fwrite( $ac, $eskial );
								fclose( $ac );
							}

							$dosya2 = TEMPLATEPATH . '/doviz_onbellek/' . $ad . '.html';
							$ac = fopen( $dosya2, 'w+' );
							fwrite( $ac, $alis );
							fclose( $ac );
						}

						unset( $$adi );
						unset( $$alis );
						unset( $$satis );
						++$tespit;

						if ($tespit == $para_s) {
							$i = $veri2;
							continue;
						}

						continue;
					}
				}

				unset( $$kod );
				unset( $$veri );
				++$i;
			}
		}

		$veri2 .= '</div>';
		echo $veri2;
		$ac = fopen( $dosya, 'w+' );
		fwrite( $ac, $veri2 );
		fclose( $ac );
		return null;
	}

	readfile( $dosya );
}

function f($bas, $son, $yazi) {
	@preg_match_all( '/' . @preg_quote( $bas, '/' ) . '(.*?)' . @preg_quote( $son, '/' ) . '/i', $yazi, $m );
	$x = $m[1];
	return $x[0];
}

function slideresim($postid) {
	if (get_post_meta( $postid, 'galeri', true )) {
		$ids = explode( ',', get_post_meta( $postid, 'galeri', true ) );
		foreach ($ids as $id) {
			echo wp_get_attachment_image( $id, 'list' );
		}

		return null;
	}

	echo '<img src="' . fs_get_option( 'fs_logo' ) . '" width="246" height="169" alt="" />';
}

function mansetresim($postid) {
	if (get_post_meta( $postid, 'galeri', true )) {
		$ids = explode( ',', get_post_meta( $postid, 'galeri', true ) );
		foreach ($ids as $id) {
			echo wp_get_attachment_image( $id, 'manset' );
		}

		return null;
	}

	echo '<img src="' . fs_get_option( 'fs_logo' ) . '" width="766" height="259" alt="" />';
}

function mansetaltiresim($postid) {
	if (get_post_meta( $postid, 'galeri', true )) {
		$ids = explode( ',', get_post_meta( $postid, 'galeri', true ) );
		$i = 5;
		foreach ($ids as $id) {

			if ($i < 7) {
				$stil = ' class="aktif"';
			}
			else {
				$stil = '';
			}

			echo '<li' . $stil . '>
				<a href="javascript:;" data-slide-index="' . $i . '">' . wp_get_attachment_image( $id, 'singlethumb' ) . '</a>
			</li>';
			++$i;
		}
	}

}

function simge($simge) {
	if ($simge == 'Türk Lirası') {
		echo 'TL';
	}


	if ($simge == 'Dolar') {
		echo '$';
	}


	if ($simge == 'Euro') {
		echo '€';
	}


	if ($simge == 'Sterlin') {
		echo '£';
	}

}

function ilanno($postid) {
	if (get_post_type(  ) == 'konut') {
		$idb = 'KNT';
	}
	else {
		if (get_post_type(  ) == 'isyeri') {
			$idb = 'ISY';
		}
		else {
			if (get_post_type(  ) == 'arsa') {
				$idb = 'ARS';
			}
			else {
				if (get_post_type(  ) == 'trustik-tesis') {
					$idb = 'TRS';
				}
			}
		}
	}

	echo $idb . '-' . $postid;
}

function satistipi() {
	$myterm = 'tur';
	$heTerm = 'tur';
	$terms = get_the_terms( $post->ID, $myterm );

	if ($terms) {
		$count = count( $terms );
		foreach ($terms as $term) {

			if ($term->slug == 'satilik') {
				echo '<span class="rent green">';
			}
			else {
				echo '<span class="rent">';
			}

			echo $term->name . ' ';

			if (get_post_type(  ) == 'konut') {
				echo 'Konut';
			}


			if (get_post_type(  ) == 'isyeri') {
				echo 'İşyeri';
			}


			if (get_post_type(  ) == 'arsa') {
				echo 'Arsa';
			}


			if (get_post_type(  ) == 'trustik-tesis') {
				echo 'Trustik Tesis';
			}

			echo '<span>';
		}
	}

}

$trendwp_tema['kod'] = 'trendemlak2';
$trendwp_tema['url'] = 'http://www.trendwp.com/musteri-paneli/';
$trendwp_tema['guvenlik_kodu'] = 'emlak21453*!';
$trend_wp = new TrendWP( $trendwp_tema );
unset( $trendwp_tema );
unset( $trend_wp );
require_once( 'inc/wp-pagenavi.php' );
require_once( 'widget/kira.php' );
require_once( 'widget/satis.php' );
require_once( 'widget/menu.php' );
require_once( 'widget/kredi.php' );
require_once( 'widget/ebulten.php' );
require_once( 'widget/haber.php' );
register_nav_menus( array( 'menu' => __( 'Ana Menü' ), 'ustmenu' => __( 'Üst Menü' ), 'altmenu' => __( 'Alt Menü' ), 'sidebar' => __( 'Sidebar Menü' ) ) );

if (function_exists( 'add_image_size' )) {
	add_theme_support( 'post-thumbnails' );
}


if (function_exists( 'add_image_size' )) {
	add_image_size( 'manset', 766, 359, true );
	add_image_size( 'list', 246, 169, true );
	add_image_size( 'haber', 203, 87, true );
	add_image_size( 'singlethumb', 79, 69, true );
}

add_action( 'init', 'manset_ekle' );
add_action( 'init', 'konut_ekle' );
add_action( 'init', 'isyeri_ekle' );
add_action( 'init', 'arsa_ekle' );
add_action( 'init', 'trustik_ekle' );

if (function_exists( 'register_sidebar' )) {
	register_sidebar( array( 'name' => 'Sidebar', 'id' => 'sidebar', 'before_widget' => '<div class="home-category">', 'after_widget' => '</div>', 'before_title' => '<div class="heading"><span><i class="fa fa-th-list"></i></span><h3>', 'after_title' => '</h3></div>' ) );
}

add_action( 'init', 'add_custom_taxonomies', 0 );
add_action( 'wp_footer', 'fs_theme_footer' );
require_once( TEMPLATEPATH . '/admin/theme-options.php' );
add_action( 'admin_menu', 'kutu_listesi' );
add_action( 'save_post', 'kutulari_kaydet' );

if ($_GET['s']) {
	$no = explode( '-', $_GET['s'] );

	if (in_array( $no[0], array( 'KNT', 'ISY', 'ARS', 'TRS', 'knt', 'isy', 'ars', 'trs' ) )) {
		$url = get_permalink( intval( trim( $no[1] ) ) );

		if ($url) {
			header( 'Location:' . $url );
		}
	}
}

?>
