<?php 
add_action("widgets_init", "trend_menu_widgets");
function trend_menu_widgets() {
    register_widget( 'trend_menu_widget' );
}
class trend_menu_widget extends WP_Widget {
function trend_menu_widget() 
{
    $widget_ops = array( 'classname' => 'widget_trend_menu', 'description' => __('Bu bileşen sayesinde menü alanını sidebarda gösterilecektir.', 'trend') );
    $this->WP_Widget( 'trend_menu_widget', __('Sidebar Menü', 'trend'), $widget_ops );
}
function widget( $args, $instance )
{
?>  
	<div class="home-category">
		<div class="heading">
			<span><i class="fa fa-th-list"></i></span>
			<h3>Emlak Kategorileri</h3>
		</div>
		<ul class="land-list">
			<?php wp_nav_menu( array( 'theme_location' => 'sidebar', 'container' => false, 'menu_class' => '', 'container_id' => 'header', 'fallback_cb' => 'wp_page_menu', 'items_wrap' => '%3$s', ) );  ?>
		</ul>

	</div>
<?php
}
function update( $new_instance, $old_instance ) {}
function form( $instance ) 
{
	$instance = wp_parse_args( (array) $instance, $defaults ); 
    echo'<p>Bileşene Ait Ek Bir Ayar Bulunmamaktadır!</p>';
}
}

?>