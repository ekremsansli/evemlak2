<?php 
add_action("widgets_init", "trend_kira_widgets");
function trend_kira_widgets() {
    register_widget( 'trend_kira_widget' );
}
class trend_kira_widget extends WP_Widget {
function trend_kira_widget() 
{
    $widget_ops = array( 'classname' => 'widget_trend_kira', 'description' => __('Bu bileşen sayesinde "Kiraya Vermek İstiyorum" alanını sidebarda gösterilecektir.', 'trend') );
    $this->WP_Widget( 'trend_kira_widget', __('Kiraya Vermek İstiyorum', 'trend'), $widget_ops );
}
function widget( $args, $instance )
{
	$sayfa = apply_filters( 'ayesoft', $instance['sayfa'] );
?>  
	<div class="adsbox-one">
		<a href="<?php echo get_page_link($sayfa); ?>" class="adv-photo"><img src="<?php bloginfo('template_url'); ?>/images/adv1.png" alt="" title=""/></a>
		<span class="ads-info"><strong>KİRAYA</strong> VERMEK İSTİYORUM</span>
		<i class="corner">&nbsp;</i>
	</div>	
<?php
}
function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['sayfa'] = ( ! empty( $new_instance['sayfa'] ) ) ? strip_tags( $new_instance['sayfa'] ) : '';		
		return $instance;
}
function form( $instance ) 
{
		$instance = wp_parse_args( (array) $instance, $defaults ); 				
		if ( isset( $instance[ 'sayfa' ] ) ) {
			$sayfa = $instance[ 'sayfa' ];
		}
		else {
			$sayfa = __( '0', 'trendemlak2' );
		}		
		?>				
		<p>
			<label for="<?php echo $this->get_field_id( 'sayfa' ); ?>"><?php _e( 'Sayfa:' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'sayfa' ); ?>" name="<?php echo $this->get_field_name( 'sayfa' ); ?>">
				<?php 
					$pages = get_pages(); 
					foreach ( $pages as $page ) {
						if(esc_attr( $sayfa ) == $page->ID) $select = 'selected="selected"'; else $select ='';
						$option = '<option value="' . $page->ID . '" '.$select.'>';
						$option .= $page->post_title;
						$option .= '</option>';
						echo $option;
					}					
				?>
			</select>
		</p>		
		<?php
}
}

?>