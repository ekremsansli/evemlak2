<?php 
add_action("widgets_init", "trend_kredi_widgets");
function trend_kredi_widgets() {
    register_widget( 'trend_kredi_widget' );
}
class trend_kredi_widget extends WP_Widget {
function trend_kredi_widget() 
{
    $widget_ops = array( 'classname' => 'widget_trend_kredi', 'description' => __('Bu bileşen sayesinde Kredi Hesaplama alanını sidebarda gösterilecektir.', 'trend') );
    $this->WP_Widget( 'trend_kredi_widget', __('Kredi Hesaplama', 'trend'), $widget_ops );
}
function widget( $args, $instance )
{
?>  
						<div class="loan-calculator">
							<h3>Kredi Hesaplama</h3>
							<div class="row clearfix">
								<strong>Kredi Miktarı</strong>
								<div class="scroll">									
									<i id="slider-range">&nbsp;</i>									
								</div>
								<span class="value" id="amount">80.000 TL</span>
							</div>
							<div class="row clearfix">
								<strong>Vade</strong>
								<div class="scroll">									
									<i id="slider-range1">&nbsp;</i>									
								</div>
								<span class="value" id="amount1">60 AY</span>
							</div>							
							<div class="row clearfix">
								<strong>Faiz Oranı</strong>
								<input type="text" id="oran" value="0.89" />

							</div>
							<div class="row clearfix">
								<strong>Aylık Ödeme Tutarı</strong>
								<input type="text" id="aylik" value="1,726.65 TL" />

							</div>
							<div class="row clearfix last-child">
								<strong>Toplam Tutar</strong>
								<input type="text" id="toplam" value="103,599.25 TL" />

							</div>
						</div>
<?php
}
function update( $new_instance, $old_instance ) {}
function form( $instance ) 
{
	$instance = wp_parse_args( (array) $instance, $defaults ); 
    echo'<p>Bileşene Ait Ek Bir Ayar Bulunmamaktadır!</p>';
}
}

?>