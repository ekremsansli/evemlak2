<?php 
add_action("widgets_init", "trend_haber_widgets");
function trend_haber_widgets() {
    register_widget( 'trend_haber_widget' );
}
class trend_haber_widget extends WP_Widget {
function trend_haber_widget() 
{
    $widget_ops = array( 'classname' => 'widget_trend_haber', 'description' => __('Bu bileşen sayesinde Haberler alanını sidebarda gösterilecektir.', 'trend') );
    $this->WP_Widget( 'trend_haber_widget', __('Haberler', 'trend'), $widget_ops );
}
function widget( $args, $instance )
{
	$sayfa = apply_filters( 'ayesoft', $instance['sayfa'] );
?>  
						<div class="home-category news-letter gray-box">
							<div class="heading">
								<span><i class="fa fa-envelope"></i></span>
								<h3>-Haberler</h3>
							</div>
							<div class="row">
								<?php 			
								wp_reset_query();			
								query_posts('showposts=3&post_type=post');
								while ( have_posts() ) : the_post(); 																
								?>
								<article class="real-state-news">
									<figure>
										<a href="<?php the_permalink(); ?>">
										<?php
											if ( has_post_thumbnail()) : 
												the_post_thumbnail( 'haber', array('class' => 'fl-right', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'' )); 
											elseif (get_post_meta($post->ID, 'resim', true) != '') : 
												echo '<img class="fl-right" src="'.get_post_meta($post->ID, 'resim', true).'" width="203" height="87" alt="'.get_the_title().'" />'; 
											else : 
												echo '<img class="fl-right" src="'.fs_get_option('fs_logo').'" width="203" height="87" alt="'.get_the_title().'" />';
											endif;
										?>
										</a>
									</figure>
									<h2><?php the_title(); ?></h2>
								</article>
								<?php endwhile; ?>								
								<a href="<?php echo get_page_link($sayfa); ?>" class="view-all">Tüm Haberleri Görüntüle</a>
							</div>

						</div>
<?php
}
function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['sayfa'] = ( ! empty( $new_instance['sayfa'] ) ) ? strip_tags( $new_instance['sayfa'] ) : '';		
		return $instance;
}
function form( $instance ) 
{
		$instance = wp_parse_args( (array) $instance, $defaults ); 				
		if ( isset( $instance[ 'sayfa' ] ) ) {
			$sayfa = $instance[ 'sayfa' ];
		}
		else {
			$sayfa = __( '0', 'trendemlak2' );
		}		
		?>				
		<p>
			<label for="<?php echo $this->get_field_id( 'sayfa' ); ?>"><?php _e( 'Sayfa:' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'sayfa' ); ?>" name="<?php echo $this->get_field_name( 'sayfa' ); ?>">
				<?php 
					$pages = get_pages(); 
					foreach ( $pages as $page ) {
						if(esc_attr( $sayfa ) == $page->ID) $select = 'selected="selected"'; else $select ='';
						$option = '<option value="' . $page->ID . '" '.$select.'>';
						$option .= $page->post_title;
						$option .= '</option>';
						echo $option;
					}					
				?>
			</select>
		</p>		
		<?php
}
}

?>