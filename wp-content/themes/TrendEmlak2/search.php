<?php get_header(); ?>
			<!-- Start Content Section-->
			<div id="content">
				<div class="main clearfix">
					<div class="mid-content">
						<div class="property-box type-2">
							<?php
							wp_reset_query();
							if($_GET['s']=='gelismis')
							{
								$tur = $_GET['emlak-turu'];
								$drm = $_GET['tur'];
								if($_GET['semtg']=='all') $semt = $_GET['ilce']; else $semt = $_GET['semtg'];
								$m1 = $_GET['m21'];
								$m2 = $_GET['m22'];
								$f1 = $_GET['fiyat1'];
								$f2 = $_GET['fiyat2'];
								$birim = $_GET['birim'];
								if(!$f1) $f1=-1;
								if(!$f2) $f2=100000000;
								if(!$m1) $m1=-1;
								if(!$m2) $m2=100000000;
								if($tur=='konut'){
									$type = 'konut';
									$tip = 'Konut';
									$q1 = 'konut-tipi';
									$q2 = $_GET['konut-tipi'];
								}elseif($tur=='isyeri'){
									$type = 'isyeri';
									$tip = 'İşyeri';
									$q1 = 'isyeri-tipi';
									$q2 = $_GET['isyeri-tipi'];
								}elseif($tur=='arsa'){
									$type = 'arsa';
									$tip = 'Arsa';
									$q1 = 'imar';
									$q2 = $_GET['imar'];
								}elseif($tur=='trustik-tesis'){
									$type = 'trustik-tesis';
									$tip = 'Trustik Tesis';
								}else{
									$type = 'any';
									$tip = 'Tüm';
									$q1 = 'trustik-tesis-tipi';
									$q2 = $_GET['trustik-tesis-tipi'];
								}
								if($drm=='satilik'){
									$dur = 'satilik';
									$durum = 'Satılık';
								}else{
									$dur= 'kiralik';
									$durum = 'Kiralık';
								}				
								$title = sprintf(('%s %s Araması Sonuçları'), $durum , $tip);					
								$query = wp_parse_args($query_string);							
								$query2 = array(
									'post_type' => $type,
									'tur' => $dur,
									$q1 => $q2,
									'semt' => $semt,
									'meta_query' => array(										
										array(
											'key' => 'm2',
											'value' => array( $m1, $m2 ),
											'type' => 'numeric',
											'compare' => 'BETWEEN'
										),
										array(
											'key' => 'fiyat',
											'value' => array( $f1, $f2 ),
											'type' => 'numeric',
											'compare' => 'BETWEEN'
										),
										array(
											'key' => 'birim',
											'value' => $birim
										)										
									)
								);
								query_posts($query2);
							}
							else
							{
								$typ = $_GET['type'];							
								if($typ == 'all'):
									$type = array('konut','isyeri','arsa','trustik-tesis');
									$yazi = 'Tüm Emlak';
								elseif($typ == '1'):
									$type = 'konut';
									$drm = 'kiralik';
									$yazi = 'Kiralık Konut';
								elseif($typ == '2'):
									$type = 'isyeri';
									$drm = 'kiralik';
									$yazi = 'Kiralık İşyeri';
								elseif($typ == '3'):
									$type = 'arsa';
									$drm = 'kiralik';
									$yazi = 'Kiralık Arsa';
								elseif($typ == '4'):
									$type = 'trustik-tesis';
									$drm = 'kiralik';
									$yazi = 'Kiralık Trustik Tesis';
								elseif($typ == '5'):
									$type = 'konut';
									$drm = 'satilik';
									$yazi = 'Satılık Konut';
								elseif($typ == '6'):
									$type = 'isyeri';
									$drm = 'satilik';
									$yazi = 'Satılık İşyeri';
								elseif($typ == '7'):
									$type = 'arsa';
									$drm = 'satilik';
									$yazi = 'Satılık Arsa';
								elseif($typ == '8'):
									$type = 'trustik-tesis';								
									$drm = 'satilik';
									$yazi = 'Satılık Trustik Tesis';
								else:
									$type = array('konut','isyeri','arsa','trustik-tesis');								
									$yazi = 'Tüm Emlak';
								endif;
								
								$query = wp_parse_args($query_string);							
								$query2 = array(
									'post_type' => $type,
									'tur' => $drm,
									's' => $query['s']
								);							
								$title = sprintf(('%s için %s Araması Sonuçları'), '&quot;'.get_search_query().'&quot;' , '&quot;'.$yazi.'&quot;');					
								query_posts($query2);
							}
							?> 
							<div class="title-head clearfix">
								<h2><?php echo $title; ?></h2><a class="all">&nbsp;</a>
							</div>
							<ul class="gallery-block">
								<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
								<li>
									<figure class="appartment">
										<a href="<?php the_permalink(); ?>">
										<?php
											if ( has_post_thumbnail()) : 
												the_post_thumbnail( 'list', array('class' => 'fl-right', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'' )); 
											elseif (get_post_meta($post->ID, 'resim', true) != '') : 
												echo '<img class="fl-right" src="'.get_post_meta($post->ID, 'resim', true).'" width="246" height="169" alt="'.get_the_title().'" />'; 
											else : 
												echo '<img class="fl-right" src="'.fs_get_option('fs_logo').'" width="246" height="169" alt="'.get_the_title().'" />';
											endif;
										?>
										</a>
										<span class="pricing">
										<?php
											if(get_post_type()=="arsa")
											{
												echo number_format((intval(get_post_meta($post->ID, 'fiyat', true)) * intval(get_post_meta($post->ID, 'm2', true))), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true));
											}
											else
											{
												echo number_format(intval(get_post_meta($post->ID, 'fiyat', true)), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); 
											}
										?>
										</span>
										<?php satistipi(); ?>
									</figure>
									<article class="appartment-details">
										<strong><?php the_title(); ?></strong>
										<span><?php semtgetir(); ?></span>
										<a href="<?php the_permalink(); ?>" class="more-info">Detaylı İncele</a>
									</article>
								</li>
								<?php endwhile; endif; ?>
							</ul>
						</div>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
			<!-- End content Section-->
<?php get_footer(); ?>