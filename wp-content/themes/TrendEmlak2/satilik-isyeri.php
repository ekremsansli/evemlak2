<?php
/*
Template Name: Satılık İşyeri
*/
?>
<?php get_header(); ?>
			<!-- Start Content Section-->
			<div id="content">
				<div class="main clearfix">
					<div class="mid-content">
						<div class="property-box type-2">
							<?php
							wp_reset_query();
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts(
								array( 
									'post_type'=> 'isyeri',
									'tur' => 'satilik',
									'paged' => $paged 
								)
							);							
							?> 
							<div class="title-head clearfix">
								<h2>Satılık İşyeri İlanları</h2><a class="all">&nbsp;</a>
							</div>
							<ul class="gallery-block">
								<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
								<li>
									<figure class="appartment">
										<a href="<?php the_permalink(); ?>">
										<?php
											if ( has_post_thumbnail()) : 
												the_post_thumbnail( 'list', array('class' => 'fl-right', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'' )); 
											elseif (get_post_meta($post->ID, 'resim', true) != '') : 
												echo '<img class="fl-right" src="'.get_post_meta($post->ID, 'resim', true).'" width="246" height="169" alt="'.get_the_title().'" />'; 
											else : 
												echo '<img class="fl-right" src="'.fs_get_option('fs_logo').'" width="246" height="169" alt="'.get_the_title().'" />';
											endif;
										?>
										</a>
										<span class="pricing">
										<?php										
											if(get_post_type()=="arsa")
											{
												echo number_format((intval(get_post_meta($post->ID, 'fiyat', true)) * intval(get_post_meta($post->ID, 'm2', true))), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true));
											}
											else
											{
												echo number_format(intval(get_post_meta($post->ID, 'fiyat', true)), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); 
											}										
										?>
										</span>
										<?php satistipi(); ?>
									</figure>
									<article class="appartment-details">
										<strong><?php the_title(); ?></strong>
										<span><?php semtgetir(); ?></span>
										<a href="<?php the_permalink(); ?>" class="more-info">Detaylı İncele</a>
									</article>
								</li>
								<?php endwhile; endif; ?>
							</ul>
						</div>
						<div class="clear"></div>
						<div id="pagenavi">		
							<?php wp_pagenavi(); ?>
						</div>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
			<!-- End content Section-->
<?php get_footer(); ?>