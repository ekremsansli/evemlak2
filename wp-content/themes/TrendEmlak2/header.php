<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=1000px" />
		<title><?php
		global $page, $paged;
			wp_title( '|', true, 'right' );
			bloginfo( 'name' );
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
			if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Sayfa %s', 'ayesoft' ), max( $paged, $page ) );
		?></title>
		<?php wp_head(); ?>
		<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico"/>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/reset.css" type="text/css"/>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" type="text/css"/>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/global.css" type="text/css"/>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" type="text/css"/>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/range.css" type="text/css"/>
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700,500' rel='stylesheet' type='text/css'>
		<!--[if  IE 8]>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/ie.css">
		<![endif]-->
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.selectbox-0.2.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/bxslider.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>
	</head>
	<body>
		<!-- Start Wrapper-->
		<div id="wrapper">
			<!-- Start Header Section-->

			<header id="header">
				<div class="top-header">
					<div class="main clearfix">
						<nav>
							<ul class="nav">
								<?php wp_nav_menu( array( 'theme_location' => 'ustmenu', 'container' => false, 'menu_class' => '', 'container_id' => 'header', 'fallback_cb' => 'wp_page_menu', 'items_wrap' => '%3$s', ) );  ?>
							</ul>
						</nav>

						<div class="social-media">
							<span class="links">Sosyal Medya’da TrendEmlak:</span>
							<ul class="social">
								<?php if(fs_get_option('fs_facebook')): ?>
								<li>
									<a href="<?php echo fs_get_option('fs_facebook'); ?>"><i class="fa fa-facebook"></i></a>
								</li>
								<?php endif; ?>
								<?php if(fs_get_option('fs_twitter')): ?>
								<li>
									<a href="<?php echo fs_get_option('fs_twitter'); ?>"><i class="fa fa-twitter"></i></a>
								</li>
								<?php endif; ?>
								<li>
									<a href="<?php bloginfo('home'); ?>/feed"><i class="fa fa-rss"></i></a>
								</li>

							</ul>

						</div>
						<span class="line"></span>

					</div>

				</div>
				<div class="main">
					<div class="mid-header clearfix">
						<a href="<?php bloginfo('home'); ?>">
							<img src="<?php echo fs_get_option('fs_logo'); ?>" alt="" />
						</a>
						<div class="block">
							<?php kurgetir(); ?>
							<div class="help-line">
								<span class="help-info">T&#220;M SORU VE SORUNLARINIZ İ&#199;İN</span>
								<i class="tel"><?php echo fs_get_option('fs_tel'); ?>  <?php if(fs_get_option('fs_gsm')) echo '-'; ?>  <?php echo fs_get_option('fs_gsm'); ?></i>
								<span class="phone-icon"> <i class="fa fa-phone"></i> </span>
							</div>
						</div>
					</div>

					<div class="main-header">

						<nav>
							<ul class="menu clearfix">
								<li class="home">
									<a href="<?php bloginfo('home'); ?>"></a>
								</li>
								<?php wp_nav_menu( array( 'theme_location' => 'menu', 'container' => false, 'menu_class' => '', 'container_id' => 'header', 'fallback_cb' => 'wp_page_menu', 'items_wrap' => '%3$s', ) );  ?>
							</ul>

						</nav>
						<div class="quick-search clearfix">
							<span class="search">hızlı arama</span>
							<form action="<?php bloginfo('home'); ?>" method="get">
								<select class="select-item" name="type" id="category_id">
									<option value="all" <?php if($_GET['type']=='all') echo 'selected="selected"'; ?>>Tüm Emlaklar</option>
									<option value="1" <?php if($_GET['type']=='1') echo 'selected="selected"'; ?>>Kiralık Konut</option>
									<option value="2" <?php if($_GET['type']=='2') echo 'selected="selected"'; ?>>Kiralık İşyeri</option>
									<option value="3" <?php if($_GET['type']=='3') echo 'selected="selected"'; ?>>Kiralık Arsa</option>
									<option value="4" <?php if($_GET['type']=='4') echo 'selected="selected"'; ?>>Kiralık Trustik Tesis</option>
									<option value="5" <?php if($_GET['type']=='5') echo 'selected="selected"'; ?>>Satılık Konut</option>
									<option value="6" <?php if($_GET['type']=='6') echo 'selected="selected"'; ?>>Satılık İşyeri</option>
									<option value="7" <?php if($_GET['type']=='7') echo 'selected="selected"'; ?>>Satılık Arsa</option>
									<option value="8" <?php if($_GET['type']=='8') echo 'selected="selected"'; ?>>Satılık Trustik Tesis</option>
								</select>

								<div class="search-box">
									<input type="text" name="s" placeholder="Örnek: Ankara, Çankaya" value="<?php echo $_GET['s']; ?>" />
									<input type="submit" value="" class="fa fa-search"/>
								</div>

								<input type="submit" class="button" value="ARA" />
								<a href="javascript:;" class="button pop-up">detaylı arama</a>
							</form>
						</div>

						<div class="form-info">
							<form action="<?php bloginfo('home'); ?>" method="get">
								<fieldset>
									<legend>
										Detaylı Arama
									</legend>
									<?php					  
									$terms = get_terms('semt', 'orderby=count&hide_empty=0');
									 $count = count($terms);
									 if ( $count > 0 ){	 		 
										 foreach ( $terms as $term ) {
										   if (0 == $term->parent) $parentsItems[] = $term;
										   if ($term->parent) {
											$childItems[] = $term;
										   }
										 }
										 $i=0;
										 foreach ($parentsItems as $parentsItem){
											$ilce[$i] = $parentsItem;			
											$j=0;
											foreach($childItems as $childItem){
												if ($childItem->parent == $parentsItem->term_id){
													$semt[$i][$j] = $childItem;
													$j++;
												}
											};                			
											$i++;
										 };														 
									 }
									 ?>
									<script type="text/javascript">
									$(document).ready(function() {
									var liste = [];
									var liste2;
										<?php 
										$i=0;
										foreach($semt as $sem) {
												unset($yeni);
												echo 'liste['.$i.'] = \'';
												echo '<option value="all">HEPSİ</option>';
												$j=0;
												foreach($sem as $se)
												{																															
													if($_GET['semt']==$se->slug) $sec='selected="selected"'; else $sec='';
													echo '<option '.$sec.' value="'.$se->slug.'">'.$se->name.'</option>'; 																												
													$j++;																
												}
												echo '\';';																
												$i++;
											}
										?>													
										$('#semtgetir2').html(liste[1]);													
										$("#ilcesec2").change(function(){
											$("#ilcesec2 option:selected").each(function(){
												var sayi = $(this).prevAll().length;
												getir = liste[sayi];
												$('#semtgetir2').html(getir);
											});
										}).change();													
									});
									</script>
									<input type="hidden" name="s" value="gelismis" />
									 <div class="find-info">
										<div class="left-col">
											<label>İlçe</label>
											<label class="select">
											<select id="ilcesec2" name="ilce">
												<?php foreach($ilce as $ilc) { if($_GET['ilce']==$ilc->slug)$sec = 'selected="selected"'; else $sec=''; echo '<option '.$sec.' value="'.$ilc->slug.'">'.$ilc->name.'</option>'; } ?>
											</select>
											</label>
										</div>
										<div class="left-col right-col">
											<label>Semt</label>
											<label class="select">
											<select id="semtgetir2" name="semtg">
											</select>
											</label>
										</div>													
									</div>									
									<div class="find-info">
										<div class="left-col">
											<label>Emlak Türü</label>
											<label class="select">
											<select name="emlak-turu" id="tur2">
												<option value="konut">Konut</option>
												<option value="isyeri">İşyeri</option>
												<option value="arsa">Arsa</option>
												<option value="trustik-tesis">Trustik Tesis</option>
											</select>
											</label>
										</div>
										<div class="left-col right-col konut2">
											<label>Emlak Tipi</label>
											<label class="select">
											<select name="konut-tipi">
												<?php 																							   																	
													$args = array( 'taxonomy' => 'konut-tipi', 'hide_empty'    => false );				
													$terms = get_terms('konut-tipi', $args);
													$count = count($terms);
													if ($count > 0) {
														foreach ($terms as $term) {																				
															$term_list .= '<option value="' . $term->slug . '">' . $term->name . '</option>';					
														}
														echo $term_list;
													}
													unset($term_list);
												?>															
											</select>
											</label>
										</div>
										<div class="left-col right-col isyeri2">
											<label>Emlak Tipi</label>
											<label class="select">
											<select name="isyeri-tipi">
												<?php 																							   																	
													$args = array( 'taxonomy' => 'isyeri-tipi', 'hide_empty'    => false );				
													$terms = get_terms('isyeri-tipi', $args);
													$count = count($terms);
													if ($count > 0) {
														foreach ($terms as $term) {																				
															$term_list .= '<option value="' . $term->slug . '">' . $term->name . '</option>';					
														}
														echo $term_list;
													}
													unset($term_list);
												?>															
											</select>
											</label>
										</div>
										<div class="left-col right-col arsa2">
											<label>İmar</label>
											<label class="select">
											<select name="imar">
												<?php 																							   																	
													$args = array( 'taxonomy' => 'imar', 'hide_empty'    => false );				
													$terms = get_terms('imar', $args);
													$count = count($terms);
													if ($count > 0) {
														foreach ($terms as $term) {																				
															$term_list .= '<option value="' . $term->slug . '">' . $term->name . '</option>';					
														}
														echo $term_list;
													}
													unset($term_list);
												?>															
											</select>
											</label>
										</div>
										<div class="left-col right-col trustik-tesis2">
											<label>Emlak Tipi</label>
											<label class="select">
											<select name="trustik-tesis-tipi">
												<?php 																							   																	
													$args = array( 'taxonomy' => 'trustik-tesis-tipi', 'hide_empty'    => false );				
													$terms = get_terms('trustik-tesis-tipi', $args);
													$count = count($terms);
													if ($count > 0) {
														foreach ($terms as $term) {																				
															$term_list .= '<option value="' . $term->slug . '">' . $term->name . '</option>';					
														}
														echo $term_list;
													}
													unset($term_list);
												?>															
											</select>
											</label>
										</div>
									</div>
									<div class="find-info">
										<div class="left-col">
											<label>m2 Aralığı</label>
											<div class="range">
												<input type="text" name="m21" value="" />
												<i class="minus">-</i>
												<input type="text" name="m22" value="" />

											</div>
										</div>
										<div class="left-col right-col">
											<label>Fiyat Aralığı <span class="arsa2"> / m2</span></label>
											<div class="range">
												<input type="text" name="fiyat1" value="" />
												<i class="minus">-</i>
												<input type="text" name="fiyat2" value="" />

											</div>
										</div>
									</div>
									<div class="find-info">
										<div class="left-col">
											<label>Emlak Türü</label>
											<label class="select">
											<select name="tur">
												<?php 																							   																	
													$args = array( 'taxonomy' => 'tur', 'hide_empty'    => false );				
													$terms = get_terms('tur', $args);
													$count = count($terms);
													if ($count > 0) {
														foreach ($terms as $term) {																				
															$term_list .= '<option value="' . $term->slug . '">' . $term->name . '</option>';					
														}
														echo $term_list;
													}
													unset($term_list);
												?>															
											</select>
											</label>
										</div>
										<div class="left-col right-col">
											<label>Birim</label>
											<div class="range">												
												<label class="select">
												<select name="birim">
													<option value="Türk Lirası">Türk Lirası</option>
													<option value="Dolar">Dolar</option>
													<option value="Euro">Euro</option>
													<option value="Sterlin">Sterlin</option>
												</select>
												</label>
											</div>
										</div>
									</div>									
									<input type="submit" value="detaylı aramayı başlat" />
								</fieldset>

							</form>

						</div>

					</div>

				</div>
			</header>
			<!-- End Header Section-->
