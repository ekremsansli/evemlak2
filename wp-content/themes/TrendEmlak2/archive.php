<?php get_header(); ?>
			<!-- Start Content Section-->
			<div id="content">
				<div class="main clearfix">
					<div class="mid-content">
						<div class="property-box type-2">
							<?php
							wp_reset_query();
							$post = $posts[0];
							if (is_category()) {
							 $baslik = '"'. single_cat_title("",false).'" kategorisi';
							} 
							elseif( is_tag() ) {								
								$baslik = '"'.single_tag_title("",false).'" olarak etiketlenmiş yazılar';
								$tag       = get_queried_object();				
								$tag_slug  = $tag->slug;
								query_posts(array( 'post_type' => array('referans','galeri','urun','hizmet','post'), 'tag' => $tag_slug ));
							} 
							elseif (is_day()) {
								$baslik = '"'.get_the_time('d F Y').'" için Arşiv';
							} 
							elseif (is_month()) { 
								$baslik = '"'.get_the_time('F Y').'" için Arşiv';
							} 
							elseif (is_year()) {
								$baslik = '"'.get_the_time('Y').'" için Arşiv';
							} 
							elseif (is_search()) { 
								$title = sprintf(('<h2>%s <strong>Arama Sonuçları</strong></h2>'), '&quot;'.get_search_query().'&quot;');
								$baslik = $title; 
							}
							elseif (is_author()) { 
								$baslik = 'Yazar Arşivi';	
							} elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { 
								$baslik = 'Blog Arşivi';
							}
							elseif(get_post_type()=="konut") {
								$baslik = 'Konut';
							}
							elseif(get_post_type()=="isyeri") {
								$baslik = 'İşyeri';
							}
							elseif(get_post_type()=="arsa") {
								$baslik = 'Arsa';
							}
							elseif(get_post_type()=="trustik-tesis") {
								$baslik = 'Trustik Tesis';
							}							
							else
							{
								$category = get_the_category(); $baslik = $category[0]->cat_name;
							}			
							?> 
							<div class="title-head clearfix">
								<h2><?php echo $baslik; ?></h2><a class="all">&nbsp;</a>
							</div>
							<ul class="gallery-block">
								<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
								<li>
									<figure class="appartment">
										<a href="<?php the_permalink(); ?>">
										<?php if(get_post_type()<>"post"): ?>
											<?php
												if ( has_post_thumbnail()) : 
													the_post_thumbnail( 'list', array('class' => 'fl-right', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'' )); 
												elseif (get_post_meta($post->ID, 'resim', true) != '') : 
													echo '<img class="fl-right" src="'.get_post_meta($post->ID, 'resim', true).'" width="246" height="169" alt="'.get_the_title().'" />'; 
												else : 
													echo '<img class="fl-right" src="'.fs_get_option('fs_logo').'" width="246" height="169" alt="'.get_the_title().'" />';
												endif;
											?>
										<?php else: ?>
										<?php
											if ( has_post_thumbnail()) : 
												the_post_thumbnail( 'list', array('class' => 'fl-right', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'' )); 
											elseif (get_post_meta($post->ID, 'resim', true) != '') : 
												echo '<img class="fl-right" src="'.get_post_meta($post->ID, 'resim', true).'" width="246" height="169" alt="'.get_the_title().'" />'; 
											else : 
												echo '<img class="fl-right" src="'.fs_get_option('fs_logo').'" width="246" height="169" alt="'.get_the_title().'" />';
											endif;
										?>
										</a>
										<?php endif; ?>
										<?php if(get_post_type()<>"post"): ?>
										<span class="pricing">
										<?php										
											if(get_post_type()=="arsa")
											{
												echo number_format((intval(get_post_meta($post->ID, 'fiyat', true)) * intval(get_post_meta($post->ID, 'm2', true))), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true));
											}
											else
											{
												echo number_format(intval(get_post_meta($post->ID, 'fiyat', true)), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); 
											}										
										?>
										</span>
										<?php satistipi(); ?>
										<?php endif; ?>
									</figure>
									<article class="appartment-details">
										<strong><?php the_title(); ?></strong>
										<span><?php semtgetir(); ?></span>
										<a href="<?php the_permalink(); ?>" class="more-info">Detaylı İncele</a>
									</article>
								</li>
								<?php endwhile; endif; ?>
							</ul>
						</div>
						<div class="clear"></div>
						<div id="pagenavi">		
							<?php wp_pagenavi(); ?>
						</div>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
			<!-- End content Section-->
<?php get_footer(); ?>