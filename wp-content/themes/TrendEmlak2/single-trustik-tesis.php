<?php get_header(); ?>
			<!-- Start Content Section-->
			<div id="content">
				<div class="main clearfix">
					<div class="mid-content">
						<?php wp_reset_query(); if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="banner2">
							<ul class="slides">
								<li>
									<div class="slideforsingle">
										<?php mansetresim($post->ID); ?>
									</div>
									<div class="banner-text">
										<span class="left"><?php the_title(); ?></span>
										<span class="right"><?php echo number_format(intval(get_post_meta($post->ID, 'fiyat', true)), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); ?></span>
									</div>

								</li>
							</ul>
						</div>

						<section class="carousel">
							<div class="carousel-box">
								<ul class="mansetalti">
									<?php mansetaltiresim($post->ID); ?>									
								</ul>
								<ul class="carousel-nav">
									<li>
										<a href="javascript:;" class="prev"></a>
									</li>
									<li>
										<a href="javascript:;" class="next"></a>
									</li>
								</ul>
							</div>

						</section>

						<div class="ad-details-info">
							<h2 class="head-box">İlan Bilgileri</h2>
							<div class="ad-details">
								<ul class="catalog">
									<li>
										<i class="icon1"></i>
										<span class="left-text">ilan No:</span>
										<small class="left-text right-text"><?php ilanno($post->ID); ?></small>
									</li>
									<li>
										<i class="fa fa-calendar"></i>
										<span class="left-text">İlan Tarihi:</span>
										<small class="left-text right-text"><?php the_date('d.m.Y'); ?> </small>
									</li>
									<li>
										<i class="icon2"></i>
										<span class="left-text">Metrekare:</span>
										<small class="left-text right-text"><?php echo intval(get_post_meta($post->ID, 'am2', true))+intval(get_post_meta($post->ID, 'km2', true)); ?> m²</small>
									</li>
									<li>
										<i class="icon3"></i>
										<span class="left-text">oda:</span>
										<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'oda', true); ?></small>
									</li>
									<li>
										<i class="icon4"></i>
										<span class="left-text">yatak:</span>
										<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'yatak', true); ?></small>
									</li>
									<li>
										<i class="icon6"></i>
										<span class="left-text">Yıldız:</span>
										<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'yildiz', true); ?></small>
									</li>
								</ul>
							</div>
							<div class="ad-info">
								<article class="para">
									<?php the_content(); ?>
									<div class="list-gallery">
										<ul class="catalog">
											<li>
												<span class="left-text">İlan Türü</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php the_terms( $post->ID, 'tur', '', ', ', '' ); ?></small>
											</li>
											<li>
												<span class="left-text">Konut Şekli</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php the_terms( $post->ID, 'trustik-tesis-tipi', '', ', ', '' ); ?></small>
											</li>
											<li>
												<span class="left-text">Binanın Yaşı</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'yas', true); ?></small>
											</li>
											<li>
												<span class="left-text">Kat Sayısı</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'toplamkat', true); ?></small>
											</li>											
										</ul>
										<ul class="catalog">
											<li>
												<span class="left-text">Açık Alan m²</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'am2', true); ?></small>
											</li>
											<li>
												<span class="left-text">Kapalı Alan m²</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'km2', true); ?></small>
											</li>
											<li>
												<span class="left-text">Zemin Etüdü</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'etud', true); ?></small>
											</li>
											<li>
												<span class="left-text">Yapının Durumu</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'yapidurumu', true)){ 
													case 1: echo 'Sıfır';break;
													case 2: echo 'İkinci El';break;
													case 3: echo 'İnşaat';break;
													case 4: echo 'Proje';break;													
													default; echo 'Belirtilmemiş'; 
												}?>
												</small>
											</li>
										</ul>

										<ul class="catalog">
											<li>
												<span class="left-text">Krediye Uygunluk</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'ozel', true)){ 													
													case 2: echo 'Evet';break;							
													default; echo 'Hayır'; 
												}?>
												</small>
											</li>											
											<li>
												<span class="left-text">Takas</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'takas', true)){ 
													case 1: echo 'Hayır';break;
													case 2: echo 'Evet';break;							
													default; echo 'Belirtilmemiş'; 
												}?>	
												</small>
											</li>
											<li>
												<span class="left-text">Yıllık Getirisi</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'getiri', true); ?></small>
											</li>

										</ul>
									</div>

								</article>

							</div>

						</div>
						
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'manzara');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Manzara</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'manzara', 'hide_empty'    => false );				
								$terms = get_terms('manzara', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'altyapi');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Altyapı</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'altyapi', 'hide_empty'    => false );				
								$terms = get_terms('altyapi', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'cevre');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Çevre Özellikleri</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'cevre', 'hide_empty'    => false );				
								$terms = get_terms('cevre', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'oda-tipi');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Oda Tipi</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'oda-tipi', 'hide_empty'    => false );				
								$terms = get_terms('oda-tipi', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'oda-ozellikleri');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Oda ÖZellikleri</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'oda-ozellikleri', 'hide_empty'    => false );				
								$terms = get_terms('oda-ozellikleri', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'banyo-ozellikleri');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Banyo ÖZellikleri</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'banyo-ozellikleri', 'hide_empty'    => false );				
								$terms = get_terms('banyo-ozellikleri', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'tesis-ozellikleri');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Tesis ÖZellikleri</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'tesis-ozellikleri', 'hide_empty'    => false );				
								$terms = get_terms('tesis-ozellikleri', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'yeme-icme');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Yeme & İçme</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'yeme-icme', 'hide_empty'    => false );				
								$terms = get_terms('yeme-icme', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'cevre-aktiviteleri');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Çevre Aktiviteleri</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'cevre-aktiviteleri', 'hide_empty'    => false );				
								$terms = get_terms('cevre-aktiviteleri', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'toplanti-kongre');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Toplantı & Kongre</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'toplanti-kongre', 'hide_empty'    => false );				
								$terms = get_terms('toplanti-kongre', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'ulasim');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Ulaşım</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'ulasim', 'hide_empty'    => false );				
								$terms = get_terms('ulasim', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>

						<div class="map-box">
							<h2 class="head-box">İlan Konumu</h2>
							<div class="map">
								<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
								<script>
									function initialize() {
									  var myLatlng = new google.maps.LatLng(<?php echo get_post_meta($post->ID, 'konum', true); ?>);
									  var mapOptions = {
										zoom: 16,
										center: new google.maps.LatLng(<?php echo get_post_meta($post->ID, 'konum', true); ?>),
										mapTypeId: google.maps.MapTypeId.ROADMAP
									  }					  
									  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

									  var marker = new google.maps.Marker({
										  position: myLatlng,
										  map: map,
										  title: 'Hello World!'						  
									  });
									  map.checkResize();					  
									}									
									google.maps.event.addDomListener(window, 'load', initialize);										
								</script>							  		
								<div id="map-canvas"></div>
							</div>
						</div>
						<?php endwhile; endif; ?>
						<?php include (TEMPLATEPATH . "/benzer-emlaklar.php"); ?>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
			<!-- End content Section-->
<?php get_footer(); ?>