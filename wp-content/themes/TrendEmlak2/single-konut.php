<?php get_header(); ?>
			<!-- Start Content Section-->
			<div id="content">
				<div class="main clearfix">
					<div class="mid-content">
						<?php wp_reset_query(); if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="banner2">
							<ul class="slides">
								<li>
									<div class="slideforsingle">
										<?php mansetresim($post->ID); ?>
									</div>
									<div class="banner-text">
										<span class="left"><?php the_title(); ?></span>
										<span class="right"><?php echo number_format(intval(get_post_meta($post->ID, 'fiyat', true)), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); ?></span>
									</div>

								</li>
							</ul>
						</div>

						<section class="carousel">
							<div class="carousel-box">
								<ul class="mansetalti">
									<?php mansetaltiresim($post->ID); ?>									
								</ul>
								<ul class="carousel-nav">
									<li>
										<a href="javascript:;" class="prev"></a>
									</li>
									<li>
										<a href="javascript:;" class="next"></a>
									</li>
								</ul>
							</div>

						</section>

						<div class="ad-details-info">
							<h2 class="head-box">İlan Bilgileri</h2>
							<div class="ad-details">
								<ul class="catalog">
									<li>
										<i class="icon1"></i>
										<span class="left-text">ilan No:</span>
										<small class="left-text right-text"><?php ilanno($post->ID); ?></small>
									</li>
									<li>
										<i class="fa fa-calendar"></i>
										<span class="left-text">İlan Tarihi:</span>
										<small class="left-text right-text"><?php the_date('d.m.Y'); ?> </small>
									</li>
									<li>
										<i class="icon2"></i>
										<span class="left-text">Metrekare:</span>
										<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'm2', true); ?> m²</small>
									</li>
									<li>
										<i class="icon3"></i>
										<span class="left-text">salon:</span>
										<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'salon', true); ?></small>
									</li>
									<li>
										<i class="icon4"></i>
										<span class="left-text">yatak odasi:</span>
										<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'oda', true); ?></small>
									</li>
									<li>
										<i class="icon5"></i>
										<span class="left-text">banyo:</span>
										<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'banyo', true); ?></small>
									</li>
								</ul>
							</div>
							<div class="ad-info">
								<article class="para">
									<?php the_content(); ?>
									<div class="list-gallery">
										<ul class="catalog">
											<li>
												<span class="left-text">İlan Türü</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php the_terms( $post->ID, 'tur', '', ', ', '' ); ?></small>
											</li>
											<li>
												<span class="left-text">Konut Şekli</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php the_terms( $post->ID, 'konut-tipi', '', ', ', '' ); ?></small>
											</li>
											<li>
												<span class="left-text">Binanın Yaşı</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'yas', true); ?></small>
											</li>
											<li>
												<span class="left-text">Kat Sayısı</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'toplamkat', true); ?></small>
											</li>
											<li>
												<span class="left-text">Bulunduğu Kat</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'kat', true); ?></small>
											</li>

										</ul>

										<ul class="catalog">
											<li>
												<span class="left-text">Isınma Tipi</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'isitma', true)){ 
													case 1: echo 'Yok';break;
													case 2: echo 'Kombi';break;
													case 3: echo 'Kalorifer';break;
													case 4: echo 'Kat Kaloriferi';break;
													case 5: echo 'Soba';break;
													case 6: echo 'Klima';break;
													case 7: echo 'Diğer';break;
													default; echo 'Belirtilmemiş'; 
												}?>
												</small>
											</li>
											<li>
												<span class="left-text">Yakıt Tipi</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'yakit', true)){ 
													case 1: echo 'Yok';break;
													case 2: echo 'Doğalgaz';break;
													case 3: echo 'Fuel-Oil';break;
													case 4: echo 'Elektrik';break;
													case 5: echo 'Kömür';break;
													case 6: echo 'Diğer';break;
													default; echo 'Belirtilmemiş'; 
												}?>	
												</small>
											</li>
											<li>
												<span class="left-text">Yapı Tipi</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'yapitipi', true)){ 
													case 1: echo 'Betonarme';break;
													case 2: echo 'Prefabrik';break;
													case 3: echo 'Ahşap';break;
													case 4: echo 'Taş Yapı';break;													
													default; echo 'Belirtilmemiş'; 
												}?>	
												</small>
											</li>
											<li>
												<span class="left-text">Yapının Durumu</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'yapidurumu', true)){ 
													case 1: echo 'Sıfır';break;
													case 2: echo 'İkinci El';break;
													case 3: echo 'İnşaat';break;
													case 4: echo 'Proje';break;													
													default; echo 'Belirtilmemiş'; 
												}?>
												</small>
											</li>
											<li>
												<span class="left-text">Kullanım Durumu</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'durum', true)){ 
													case 1: echo 'Boş';break;
													case 2: echo 'Kiracılı';break;
													case 3: echo 'İnşaat';break;							
													default; echo 'Belirtilmemiş'; 
												}?>
												</small>
											</li>

										</ul>

										<ul class="catalog">
											<li>
												<span class="left-text">Krediye Uygunluk</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'ozel', true)){ 													
													case 2: echo 'Evet';break;							
													default; echo 'Hayır'; 
												}?>
												</small>
											</li>
											<li>
												<span class="left-text">Aidat</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'aidat', true); ?></small>
											</li>
											<li>
												<span class="left-text">Takas</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'takas', true)){ 
													case 1: echo 'Hayır';break;
													case 2: echo 'Evet';break;							
													default; echo 'Belirtilmemiş'; 
												}?>	
												</small>
											</li>
											<li>
												<span class="left-text">Öğrenciye/Bekara</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'ogrenci', true)){ 
													case 1: echo 'Hayır';break;
													case 2: echo 'Evet';break;							
													default; echo 'Belirtilmemiş'; 
												}?>	
												</small>
											</li>
											<li>
												<span class="left-text">Kira Getirisi</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'kira', true); ?></small>
											</li>

										</ul>
									</div>

								</article>

							</div>

						</div>
						
						<?php 
						   $cephe = wp_get_object_terms($post->ID, 'cephe');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Cephe</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}
								$args = array( 'taxonomy' => 'cephe', 'hide_empty'    => false );				
								$terms = get_terms('cephe', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'konut-ozelligi');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Konut Özelliği</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'konut-ozelligi', 'hide_empty'    => false );				
								$terms = get_terms('konut-ozelligi', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'ic-ozellik');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">İç Özellikler</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'ic-ozellik', 'hide_empty'    => false );				
								$terms = get_terms('ic-ozellik', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'manzara');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Manzara</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'manzara', 'hide_empty'    => false );				
								$terms = get_terms('manzara', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'altyapi');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Altyapı</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'altyapi', 'hide_empty'    => false );				
								$terms = get_terms('altyapi', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'cevre');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Çevre Özellikleri</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'cevre', 'hide_empty'    => false );				
								$terms = get_terms('cevre', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'sosyal');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Sosyal Özellikleri</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'sosyal', 'hide_empty'    => false );				
								$terms = get_terms('sosyal', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'ulasim');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Ulaşım</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'ulasim', 'hide_empty'    => false );				
								$terms = get_terms('ulasim', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>

						<div class="map-box">
							<h2 class="head-box" id="harit">İlan Konumu</h2>
							<div class="map">
								<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
								<script>
									function initialize() {
									  var myLatlng = new google.maps.LatLng(<?php echo get_post_meta($post->ID, 'konum', true); ?>);
									  var mapOptions = {
										zoom: 16,
										center: new google.maps.LatLng(<?php echo get_post_meta($post->ID, 'konum', true); ?>),
										mapTypeId: google.maps.MapTypeId.ROADMAP
									  }					  
									  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

									  var marker = new google.maps.Marker({
										  position: myLatlng,
										  map: map,
										  title: 'Hello World!'						  
									  });
									  map.checkResize();					  
									}									
									google.maps.event.addDomListener(window, 'load', initialize);										
								</script>
								<div id="map-canvas"></div>
							</div>
						</div>
						<?php endwhile; endif; ?>
						<?php include (TEMPLATEPATH . "/benzer-emlaklar.php"); ?>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
			<!-- End content Section-->
<?php get_footer(); ?>