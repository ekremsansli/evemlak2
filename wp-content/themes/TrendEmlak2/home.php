<?php get_header(); ?>
			<!-- Start Content Section-->
			<div id="content">
				<div class="main clearfix">
					<div class="mid-content">
						<?php include (TEMPLATEPATH . "/inc/manset.php"); ?>
						<?php include (TEMPLATEPATH . "/inc/vitrin.php"); ?>
						<?php include (TEMPLATEPATH . "/inc/tabs.php"); ?>
						<?php include (TEMPLATEPATH . "/inc/altlist.php"); ?>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
			<!-- End content Section-->
<?php get_footer(); ?>