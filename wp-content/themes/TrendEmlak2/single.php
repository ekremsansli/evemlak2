<?php
if(get_post_type()=="konut")
{
	include(TEMPLATEPATH . '/single-konut.php');
}
elseif(get_post_type()=="isyeri")
{
	include(TEMPLATEPATH . '/single-isyeri.php');
}
elseif(get_post_type()=="arsa")
{
	include(TEMPLATEPATH . '/single-arsa.php');
}
elseif(get_post_type()=="trustik-tesis")
{
	include(TEMPLATEPATH . '/single-trustik-tesis.php');
}
else
{
	include(TEMPLATEPATH . '/single-default.php');
}
?>