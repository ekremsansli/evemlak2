$(document).ready(function() {
	$('#tur').change(function(){
		var stil = $(this).val();
		$('.konut').hide();
		$('.isyeri').hide();
		$('.arsa').hide();
		$('.trustik-tesis').hide();
		$('.'+stil).show();
	});
	$('#tur2').change(function(){
		var stil = $(this).val();
		$('.konut2').hide();
		$('.isyeri2').hide();
		$('.arsa2').hide();
		$('.trustik-tesis2').hide();
		$('.'+stil+'2').show();
	});
	if($('.mansetalti li').length > 7) $('.carousel-nav').show();
	var f=1;
	$('.carousel-nav li a.prev').click(function(){
		yeni = f+6;		
		son = f-1;
		if(yeni==7)
		{
			return false;
		}
		else
		{				
			$('.mansetalti li:nth-child('+yeni+')').removeClass('aktif');			
			$('.mansetalti li:nth-child('+son+')').addClass('aktif');
			$('.mansetalti li').css('margin-left','9px');
			$('.mansetalti li:nth-child('+son+')').css('margin-left','0px');
			f--;
		}
	});
	$('.carousel-nav li a.next').click(function(){
		son = $('.mansetalti li').length - 6;
		if(f==son)
		{
			return false;
		}
		else
		{				
			yeni2 = f+1;
			$('.mansetalti li:nth-child('+f+')').removeClass('aktif');
			yeni = f+7;
			$('.mansetalti li:nth-child('+yeni+')').addClass('aktif');
			$('.mansetalti li').css('margin-left','9px');
			$('.mansetalti li:nth-child('+yeni2+')').css('margin-left','0px');
			f++;
		}
	});
	$(".pop-up").click(function() {
		$(".form-info").slideToggle(300);
		$(this).toggleClass('selected');

	});

	$('.select-box').click(function() {
		$(".drop-down").slideToggle(300);
	});
	$("#category_id").selectbox();
	/*$(".find-info select").selectbox();	*/
	$('.slideforsingle').bxSlider({
		pagerCustom: '.carousel-box ul.mansetalti',
		onSlideBefore:function(x,y,z){			
			$('.carousel-box ul li').removeClass('select');
			$('.carousel-box ul li').eq(z).addClass('select');			
		},
		onSliderLoad:function(){			
			$('.carousel-box ul li').eq(0).addClass('select');			
		}
	});
	var slider = $('.resimslide').bxSlider({
		pager:false
	});
	$('#manset').bxSlider({
		pagerCustom: '#thumb',
		auto:true,		
		autoDelay:3000,
		speed: 1200,
		pause:5000,
		autoHover:true,
		onSlideBefore:function(x,y,z){
			$('#manset li').removeClass('active-slide');
			$('#manset li').eq(z+1).addClass('active-slide');
			$('#thumb li').removeClass('active');
			$('#thumb li').eq(z).addClass('active');
			$('.inner-text div').removeClass('active');
			$('.inner-text div').eq(z).addClass('active');
		},
		onSliderLoad:function(){
			$('#manset li').eq(1).addClass('active-slide');
			$('#thumb li').eq(0).addClass('active');
			$('.inner-text div').eq(0).addClass('active');
		}
	});
	$('.slider').bxSlider({					
		nextText:' ',	
		prevText:' ',
		nextSelector:'.next',
		prevSelector:'.prev',
		pager:false,
		minSlides: 3,
		maxSlides: 30,
		slideWidth: 244,
		slideMargin:10,
		moveSlides:1,
		auto:false
	});
	/*$('.tabss').bxSlider({
		nextText:' ',	
		prevText:' ',
		nextSelector:'',
		prevSelector:'',
		pagerCustom: '.tabs-box'
	});	*/
});
function format(num) {
	var p = num.toFixed(2).split(".");
	return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
		return  num + (i && !(i % 3) ? "," : "") + acc;
	}, "") + "." + p[1];
}
function kredihesapla()
{
	oran = parseFloat($('#oran').val());
	toplam = parseInt($('#amount').html());	
	vade = parseInt($('#amount1').html());		
	oran = oran / 100;
	sonuc = 1+oran;
	for (var i = 1; i < vade; i++)
	{
		sonuc = sonuc * (1+oran);
	}			
	donus = (toplam * oran) / (1 - (1/sonuc));
	$('#aylik').val(format(donus*1000) + ' TL');
	$('#toplam').val(format((donus*vade)*1000) + ' TL');
}
$(function() {
	$("#slider-range").slider({
		range : true,
		min : 1,
		max : 250,
		values : [0, 80.000],
		slide : function(event, ui) {
			$("#amount").html(ui.values[1] + '.000 TL');						
			kredihesapla();
		}
	});
	$("#amount").html($("#slider-range").slider("values", 1) + '.000 TL');
	kredihesapla();
});
$(function() {
	$("#slider-range1").slider({
		range : true,
		min : 6,
		max : 120,
		values : [0, 60],
		slide : function(event, ui) {
			$("#amount1").html(ui.values[1] + ' AY');						
			kredihesapla();
		}
	});
	$("#amount1").html($("#slider-range1").slider("values", 1) + 'Ay');
	kredihesapla();
});
		