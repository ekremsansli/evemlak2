<?php get_header(); ?>
			<!-- Start Content Section-->
			<div id="content">
				<div class="main clearfix">
					<div class="mid-content">
						<?php wp_reset_query(); if (have_posts()) : while (have_posts()) : the_post(); ?>												
						<div class="ad-details-info">
							<h2 class="head-box"><?php the_title(); ?></h2>							
							<div class="ad-info">
								<article class="para">
									<?php the_content(); ?>									
								</article>
							</div>
						</div>												
						<?php endwhile; endif; ?>						
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
			<!-- End content Section-->
<?php get_footer(); ?>