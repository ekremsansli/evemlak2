<?php
	$args = array
				(
					'showposts'=> '3',
					'post_type'=> get_post_type(),					
					'post__not_in' => array($post->ID),
					'orderby'=>'date',
					'order'=>'DESC'
				);
	query_posts($args);
	?>
	<?php if(have_posts()): ?>
		<div class="more-ads">
			<h2 class="head-box">Benzer İlanlar</h2>
			<ul class="gallery-block">			
					<?php
					while ( have_posts() ) : the_post(); 								
					?>			
						<li>
							<figure class="appartment">
								<a href="<?php the_permalink(); ?>">
								<?php
									if ( has_post_thumbnail()) : 
										the_post_thumbnail( 'list', array('class' => 'fl-right', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'' )); 
									elseif (get_post_meta($post->ID, 'resim', true) != '') : 
										echo '<img class="fl-right" src="'.get_post_meta($post->ID, 'resim', true).'" width="246" height="169" alt="'.get_the_title().'" />'; 
									else : 
										echo '<img class="fl-right" src="'.fs_get_option('fs_logo').'" width="246" height="169" alt="'.get_the_title().'" />';
									endif;
								?>
								</a>
								<span class="pricing"><?php echo get_post_meta($post->ID, 'fiyat', true); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); ?></span>
							</figure>
							<article class="appartment-details">
								<strong><?php the_title(); ?></strong>
								<span><?php semtgetir(); ?></span>
								<a href="<?php the_permalink(); ?>" class="more-info">Detaylı İncele</a>
							</article>
						</li>
					<?php endwhile;?>				
				<?php wp_reset_query(); ?>
			</ul>
		</div>
	<?php endif; ?>	