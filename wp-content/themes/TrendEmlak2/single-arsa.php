<?php get_header(); ?>
			<!-- Start Content Section-->
			<div id="content">
				<div class="main clearfix">
					<div class="mid-content">
						<?php wp_reset_query(); if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="banner2">
							<ul class="slides">
								<li>
									<div class="slideforsingle">
										<?php mansetresim($post->ID); ?>
									</div>
									<div class="banner-text">
										<span class="left"><?php the_title(); ?></span>
										<span class="right"><?php echo number_format((intval(get_post_meta($post->ID, 'fiyat', true)) * intval(get_post_meta($post->ID, 'm2', true))), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); ?></span>
									</div>

								</li>
							</ul>
						</div>

						<section class="carousel">
							<div class="carousel-box">
								<ul class="mansetalti">
									<?php mansetaltiresim($post->ID); ?>									
								</ul>
								<ul class="carousel-nav">
									<li>
										<a href="javascript:;" class="prev"></a>
									</li>
									<li>
										<a href="javascript:;" class="next"></a>
									</li>
								</ul>
							</div>

						</section>

						<div class="ad-details-info">
							<h2 class="head-box">İlan Bilgileri</h2>
							<div class="ad-details">
								<ul class="catalog">
									<li>
										<i class="icon1"></i>
										<span class="left-text">ilan No:</span>
										<small class="left-text right-text"><?php ilanno($post->ID); ?></small>
									</li>
									<li>
										<i class="fa fa-calendar"></i>
										<span class="left-text">İlan Tarihi:</span>
										<small class="left-text right-text"><?php the_date('d.m.Y'); ?> </small>
									</li>
									<li>
										<i class="icon2"></i>
										<span class="left-text">Metrekare:</span>
										<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'm2', true); ?> m²</small>
									</li>
									<li>
										<i class="icon2"></i>
										<span class="left-text">Metrekare Fiyatı:</span>
										<small class="left-text right-text"><?php echo number_format(intval(get_post_meta($post->ID, 'fiyat', true)), 0, ',', '.'); ?> <?php simge(get_post_meta($post->ID, 'birim', true)); ?></small>
									</li>									
								</ul>
							</div>
							<div class="ad-info">
								<article class="para">
									<?php the_content(); ?>
									<div class="list-gallery">
										<ul class="catalog">
											<li>
												<span class="left-text">İlan Türü</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php the_terms( $post->ID, 'tur', '', ', ', '' ); ?></small>
											</li>
											<li>
												<span class="left-text">İmar Şekli</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php the_terms( $post->ID, 'imar', '', ', ', '' ); ?></small>
											</li>
											<li>
												<span class="left-text">Tapu Durumu</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'tapu', true)){ 
													case 1: echo 'Hisseli Tapu';break;
													case 2: echo 'Mustakil Parsel';break;
													case 3: echo 'Tahsis';break;
													case 4: echo 'Zilliyet';break;													
													default; echo 'Belirtilmemiş'; 
												}?>
												</small>
											</li>											
										</ul>

										<ul class="catalog">											
											<li>
												<span class="left-text">Ada No</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'adano', true); ?></small>
											</li>
											<li>
												<span class="left-text">Parsel No</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'parselno', true); ?></small>
											</li>
											<li>
												<span class="left-text">Pafta No</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'paftano', true); ?></small>
											</li>
										</ul>

										<ul class="catalog">
											<li>
												<span class="left-text">Takas</span>
												<b class="dot">:</b>
												<small class="left-text right-text">
												<?php switch (get_post_meta($post->ID, 'takas', true)){ 
													case 1: echo 'Hayır';break;
													case 2: echo 'Evet';break;							
													default; echo 'Belirtilmemiş'; 
												}?>	
												</small>
											</li>
											<li>
												<span class="left-text">Gabari</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'gabari', true); ?></small>
											</li>
											<li>
												<span class="left-text">KAKS</span>
												<b class="dot">:</b>
												<small class="left-text right-text"><?php echo get_post_meta($post->ID, 'kaks', true); ?></small>
											</li>

										</ul>
									</div>

								</article>

							</div>

						</div>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'manzara');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Manzara</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'manzara', 'hide_empty'    => false );				
								$terms = get_terms('manzara', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'arsa-altyapi');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Altyapı</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'arsa-altyapi', 'hide_empty'    => false );				
								$terms = get_terms('arsa-altyapi', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'arsa-ozellikler');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Genel Özellikler</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'arsa-ozellikler', 'hide_empty'    => false );				
								$terms = get_terms('arsa-ozellikler', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>
						<?php 							
						   $cephe = wp_get_object_terms($post->ID, 'konum');
							if(!empty($cephe)){
							  if(!is_wp_error( $cephe )){
								echo '<div class="specification">
										<h2 class="head-box">Konum</h2>
											<ul class="category">';
								foreach($cephe as $term){
								  $liste[]=$term->name;
								}								
								$args = array( 'taxonomy' => 'konum', 'hide_empty'    => false );				
								$terms = get_terms('konum', $args);
								$count = count($terms);
								if ($count > 0) {					
									foreach ($terms as $term) {
										if(in_array($term->name, $liste)) $class=' class="aktif"'; else $class='';
										$term_list .= '<li'.$class.'><i class="fa fa-check-circle-o"></i>' . $term->name . '</li>';					
									}
									echo $term_list;
								}				
								echo '</ul>
								</div>';
							  }
							}
							unset($term_list);
						?>

						<div class="map-box">
							<h2 class="head-box">İlan Konumu</h2>
							<div class="map">
								<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
								<script>
									function initialize() {
									  var myLatlng = new google.maps.LatLng(<?php echo get_post_meta($post->ID, 'konum', true); ?>);
									  var mapOptions = {
										zoom: 16,
										center: new google.maps.LatLng(<?php echo get_post_meta($post->ID, 'konum', true); ?>),
										mapTypeId: google.maps.MapTypeId.ROADMAP
									  }					  
									  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

									  var marker = new google.maps.Marker({
										  position: myLatlng,
										  map: map,
										  title: 'Hello World!'						  
									  });
									  map.checkResize();					  
									}									
									google.maps.event.addDomListener(window, 'load', initialize);										
								</script>							  		
								<div id="map-canvas"></div>
							</div>
						</div>
						<?php endwhile; endif; ?>
						<?php include (TEMPLATEPATH . "/benzer-emlaklar.php"); ?>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
			<!-- End content Section-->
<?php get_footer(); ?>