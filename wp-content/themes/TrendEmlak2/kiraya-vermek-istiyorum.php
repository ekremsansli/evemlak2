<?php
/*
Template Name: Kiraya Vermek İstiyorum
*/
?>
<?php get_header(); ?>
			<!-- Start Content Section-->
			<div id="content">
				<div class="main clearfix">
					<div class="mid-content">
						<?php wp_reset_query(); if (have_posts()) : while (have_posts()) : the_post(); ?>												
						<div class="ad-details-info">
							<h2 class="head-box"><?php the_title(); ?></h2>							
							<div class="ad-info">
							<?php
								if($_GET['kaydet']=='kaydet' && $_SESSION['cevap'] == $_GET['cevap']):
								$emlakturu = $_GET['emlak-turu'];
								$ilce = $_GET['ilce'];
								$semt = $_GET['semtg'];
								$adsoyad = $_GET['adsoyad'];
								$tel = $_GET['tel'];
								$fiyat = $_GET['fiyat'];
								$birim = $_GET['birim'];
								$m2 = $_GET['m2'];
								$takas = $_GET['takas'];
								$harita = $_GET['harita'];
								$tur = 'kiralik';								
								$title = $_GET['baslik'];
								$desc = $_GET['aciklama'];
								
								if($emlakturu == 'konut'):
									$konuttipi = $_GET['konut-tipi'];									
									$ogrenci = $_GET['ogrenci'];
									$yapitipi = $_GET['yapitipi'];
									$kullanimdurumu = $_GET['kullanimdurumu'];
									$isitma = $_GET['isitma'];
									$yakit = $_GET['yakit'];
									$salon = $_GET['salon'];
									$oda = $_GET['oda'];
									$banyo = $_GET['banyo'];
									$aidat = $_GET['aidat'];									
									$yas = $_GET['yas'];
									$toplamkat = $_GET['toplamkat'];
									$kat = $_GET['kat'];
									
									$cephe = $_GET['cephe'];
									$konutozelligi = $_GET['konut-ozelligi'];
									$icozellik = $_GET['ic-ozellik'];
									$manzara = $_GET['manzara'];
									$altyapi = $_GET['altyapi'];
									$cevre = $_GET['cevre'];
									$sosyal = $_GET['sosyal'];
									$ulasim = $_GET['ulasim'];
									
									$my_post = array(
									  'post_type'	  => 'konut',
									  'post_title'    => wp_strip_all_tags( $title ),
									  'post_content'  => $desc,
									  'post_status'   => 'pending',
									  'post_author'   => 1
									);									
									
									$post_id = wp_insert_post( $my_post );
																		
									add_post_meta($post_id, 'adsoyad', $adsoyad);
									add_post_meta($post_id, 'tel', $tel);
									add_post_meta($post_id, 'konum', $harita);
									add_post_meta($post_id, 'fiyat', $fiyat);
									add_post_meta($post_id, 'birim', $birim);
									add_post_meta($post_id, 'm2', $m2);
									add_post_meta($post_id, 'takas', $takas);
									add_post_meta($post_id, 'ogrenci', $ogrenci);
									add_post_meta($post_id, 'yapitipi', $yapitipi);
									add_post_meta($post_id, 'durum', $kullanimdurumu);
									add_post_meta($post_id, 'isitma', $isitma);
									add_post_meta($post_id, 'yakit', $yakit);
									add_post_meta($post_id, 'salon', $salon);
									add_post_meta($post_id, 'oda', $oda);
									add_post_meta($post_id, 'banyo', $banyo);
									add_post_meta($post_id, 'aidat', $aidat);									
									add_post_meta($post_id, 'oda', $oda);
									add_post_meta($post_id, 'yas', $yas);
									add_post_meta($post_id, 'toplamkat', $toplamkat);
									add_post_meta($post_id, 'kat', $kat);
									
									wp_set_object_terms($post_id, array($tur), 'tur' );
									wp_set_object_terms($post_id, array($ilce,$semt), 'semt' );
									wp_set_object_terms($post_id, array($konuttipi), 'konut-tipi' );
									wp_set_object_terms($post_id, $cephe, 'cephe' );
									wp_set_object_terms($post_id, $konutozelligi, 'konut-ozelligi' );
									wp_set_object_terms($post_id, $icozellik, 'ic-ozellik' );
									wp_set_object_terms($post_id, $manzara, 'manzara' );
									wp_set_object_terms($post_id, $altyapi, 'altyapi' );
									wp_set_object_terms($post_id, $cevre, 'cevre' );
									wp_set_object_terms($post_id, $sosyal, 'sosyal' );
									wp_set_object_terms($post_id, $ulasim, 'ulasim' );
									
									
								elseif($emlakturu == 'isyeri'):
									$isyeritipi = $_GET['isyeri-tipi'];									
									$iaidat = $_GET['iaidat'];
									$ikullanimdurumu = $_GET['ikullanimdurumu'];									
									$iyas = $_GET['iyas'];
									$ioda = $_GET['ioda'];
									$itoplamkat = $_GET['itoplamkat'];
									$ikat = $_GET['ikat'];	
									$isitma = $_GET['isitma'];
									$yakit = $_GET['yakit'];
									$cephe = $_GET['cephe'];
									$konutozelligi = $_GET['konut-ozelligi'];
									$isyeriozellikleri = $_GET['isyeri-ozellikleri'];
									$manzara = $_GET['manzara'];
									$altyapi = $_GET['altyapi'];
									$cevre = $_GET['cevre'];
									$ulasim = $_GET['ulasim'];
									
									$my_post = array(
									  'post_type'	  => 'isyeri',
									  'post_title'    => wp_strip_all_tags( $title ),
									  'post_content'  => $desc,
									  'post_status'   => 'pending',
									  'post_author'   => 1
									);									
									
									$post_id = wp_insert_post( $my_post );
																		
									add_post_meta($post_id, 'adsoyad', $adsoyad);
									add_post_meta($post_id, 'tel', $tel);
									add_post_meta($post_id, 'konum', $harita);
									add_post_meta($post_id, 'fiyat', $fiyat);
									add_post_meta($post_id, 'birim', $birim);
									add_post_meta($post_id, 'm2', $m2);
									add_post_meta($post_id, 'takas', $takas);
									add_post_meta($post_id, 'aidat', $iaidat);
									add_post_meta($post_id, 'durum', $ikullanimdurumu);
									add_post_meta($post_id, 'yas', $iyas);
									add_post_meta($post_id, 'oda', $ioda);
									add_post_meta($post_id, 'toplamkat', $itoplamkat);
									add_post_meta($post_id, 'kat', $ikat);
									add_post_meta($post_id, 'isitma', $isitma);
									add_post_meta($post_id, 'yakit', $yakit);
																		
									
									wp_set_object_terms($post_id, array($tur), 'tur' );
									wp_set_object_terms($post_id, array($ilce,$semt), 'semt' );									
									wp_set_object_terms($post_id, array($isyeritipi), 'isyeri-tipi' );
									wp_set_object_terms($post_id, $cephe, 'cephe' );
									wp_set_object_terms($post_id, $konutozelligi, 'konut-ozelligi' );
									wp_set_object_terms($post_id, $isyeriozellikleri, 'isyeri-ozellikleri' );																		
									wp_set_object_terms($post_id, $manzara, 'manzara' );
									wp_set_object_terms($post_id, $altyapi, 'altyapi' );
									wp_set_object_terms($post_id, $cevre, 'cevre' );									
									wp_set_object_terms($post_id, $ulasim, 'ulasim' );
									
								elseif($emlakturu == 'arsa'):
									$imar = $_GET['imar'];
									$tapudurumu = $_GET['tapudurumu'];
									$adano = $_GET['adano'];
									$parselno = $_GET['parselno'];
									$paftano = $_GET['paftano'];
									$gabari = $_GET['gabari'];
									$kaks = $_GET['kaks'];
									
									$manzara = $_GET['manzara'];
									$altyapi = $_GET['arsa-altyapi'];									
									$konum = $_GET['konum'];
									$arsaozellikler = $_GET['arsa-ozellikler'];
									
									$my_post = array(
									  'post_type'	  => 'arsa',
									  'post_title'    => wp_strip_all_tags( $title ),
									  'post_content'  => $desc,
									  'post_status'   => 'pending',
									  'post_author'   => 1
									);									
									
									$post_id = wp_insert_post( $my_post );
																		
									add_post_meta($post_id, 'adsoyad', $adsoyad);
									add_post_meta($post_id, 'tel', $tel);
									add_post_meta($post_id, 'konum', $harita);
									add_post_meta($post_id, 'fiyat', $fiyat);
									add_post_meta($post_id, 'birim', $birim);
									add_post_meta($post_id, 'm2', $m2);
									add_post_meta($post_id, 'takas', $takas);
									add_post_meta($post_id, 'tapu', $tapudurumu);
									add_post_meta($post_id, 'adano', $adano);
									add_post_meta($post_id, 'paftano', $paftano);
									add_post_meta($post_id, 'parselno', $parselno);
									add_post_meta($post_id, 'gabari', $gabari);
									add_post_meta($post_id, 'kaks', $kaks);									
																		
									
									wp_set_object_terms($post_id, array($tur), 'tur' );
									wp_set_object_terms($post_id, array($ilce,$semt), 'semt' );									
									wp_set_object_terms($post_id, array($imar), 'imar' );									
									wp_set_object_terms($post_id, $manzara, 'manzara' );
									wp_set_object_terms($post_id, $konum, 'konum' );
									wp_set_object_terms($post_id, $arsaozellikler, 'arsa-ozellikler' );
									wp_set_object_terms($post_id, $altyapi, 'arsa-altyapi' );									
									
								elseif($emlakturu == 'trustik-tesis'):
									$trustiktesistipi = $_GET['trustik-tesis-tipi'];
									$yapidurumu = $_GET['yapidurumu'];
									$yildiz = $_GET['yildiz'];
									$toda = $_GET['toda'];
									$tyatak = $_GET['tyatak'];
									$am2 = $_GET['am2'];
									$km2 = $_GET['km2'];
									$tyas = $_GET['tyas'];
									$tkat = $_GET['tkat'];
									$yillikgetiri = $_GET['yillikgetiri'];
									$zemin = $_GET['zemin'];
									
									$manzara = $_GET['manzara'];
									$altyapi = $_GET['altyapi'];
									$cevre = $_GET['cevre'];
									$ulasim = $_GET['ulasim'];
									$odatipi = $_GET['oda-tipi'];
									$odaozellikleri = $_GET['oda-ozellikleri'];
									$banyoozellikleri = $_GET['banyo-ozellikleri'];
									$tesisozellikleri = $_GET['tesis-ozellikleri'];
									$yemeicme = $_GET['yeme-icme'];
									$cevreaktiviteleri = $_GET['cevre-aktiviteleri'];
									$tesisaktiviteleri = $_GET['tesis-aktiviteleri'];
									$toplantikongre = $_GET['toplanti-kongre'];									
									
									$my_post = array(
									  'post_type'	  => 'trustik-tesis',
									  'post_title'    => wp_strip_all_tags( $title ),
									  'post_content'  => $desc,
									  'post_status'   => 'pending',
									  'post_author'   => 1
									);									
									
									$post_id = wp_insert_post( $my_post );
																		
									add_post_meta($post_id, 'adsoyad', $adsoyad);
									add_post_meta($post_id, 'tel', $tel);
									add_post_meta($post_id, 'konum', $harita);
									add_post_meta($post_id, 'fiyat', $fiyat);
									add_post_meta($post_id, 'birim', $birim);									
									add_post_meta($post_id, 'takas', $takas);
									add_post_meta($post_id, 'am2', $am2);
									add_post_meta($post_id, 'km2', $km2);									
									add_post_meta($post_id, 'yildiz', $yildiz);
									add_post_meta($post_id, 'yapidurumu', $yapidurumu);
									add_post_meta($post_id, 'oda', $toda);
									add_post_meta($post_id, 'yatak', $tyatak);
									add_post_meta($post_id, 'yas', $tyas);
									add_post_meta($post_id, 'toplamkat', $tkat);
									add_post_meta($post_id, 'getiri', $yillikgetiri);
									add_post_meta($post_id, 'etud', $zemin);									
									
									wp_set_object_terms($post_id, array($tur), 'tur' );
									wp_set_object_terms($post_id, array($ilce,$semt), 'semt' );
									wp_set_object_terms($post_id, array($trustiktesistipi), 'trustik-tesis-tipi' );
									wp_set_object_terms($post_id, $manzara, 'manzara' );
									wp_set_object_terms($post_id, $altyapi, 'altyapi' );
									wp_set_object_terms($post_id, $cevre, 'cevre' );
									wp_set_object_terms($post_id, $ulasim, 'ulasim' );
									wp_set_object_terms($post_id, $odatipi, 'oda-tipi' );
									wp_set_object_terms($post_id, $odaozellikleri, 'oda-ozellikleri' );
									wp_set_object_terms($post_id, $banyoozellikleri, 'banyo-ozellikleri' );
									wp_set_object_terms($post_id, $tesisozellikleri, 'tesis-ozellikleri' );
									wp_set_object_terms($post_id, $yemeicme, 'yeme-icme' );
									wp_set_object_terms($post_id, $cevreaktiviteleri, 'cevre-aktiviteleri' );
									wp_set_object_terms($post_id, $tesisaktiviteleri, 'tesis-aktiviteleri' );
									wp_set_object_terms($post_id, $toplantikongre, 'toplanti-kongre' );																		
									
								endif;								
								 								 
								echo'<article class="para">
									Kaydınız Başarılı Bir Şekilde Eklendi. Müşteri Temsilcilerimiz En Kısa Sürede Size Geri Dönüş Yapacaktır.
								</article>';
								else:								
							?>
								<article class="para">				
										<form action="#">
											<fieldset>
												<?php					  
												$terms = get_terms('semt', 'orderby=count&hide_empty=0');
													 $count = count($terms);
													 if ( $count > 0 ){	 		 
														 foreach ( $terms as $term ) {
														   if (0 == $term->parent) $parentsItems[] = $term;
														   if ($term->parent) {
															$childItems[] = $term;
														   }
														 }
														 $i=0;
														 foreach ($parentsItems as $parentsItem){
															$ilce[$i] = $parentsItem;			
															$j=0;
															foreach($childItems as $childItem){
																if ($childItem->parent == $parentsItem->term_id){
																	$semt[$i][$j] = $childItem;
																	$j++;
																}
															};                			
															$i++;
														 };														 
													 }
													 ?>
												<script type="text/javascript">
												$(document).ready(function() {
												var liste = [];
												var liste2;
													<?php 
													$i=0;
													foreach($semt as $sem) {
															unset($yeni);
															echo 'liste['.$i.'] = \'';			
															$j=0;
															foreach($sem as $se)
															{																															
																if($_GET['semt']==$se->slug) $sec='selected="selected"'; else $sec='';
																echo '<option '.$sec.' value="'.$se->slug.'">'.$se->name.'</option>'; 																												
																$j++;																
															}
															echo '\';';																
															$i++;
														}
													?>													
													$('#semtgetir').html(liste[1]);													
													$("#ilcesec").change(function(){
														$("#ilcesec option:selected").each(function(){
															var sayi = $(this).prevAll().length;
															getir = liste[sayi];
															$('#semtgetir').html(getir);
														});
													}).change();													
												});
												</script>
												<div class="secenek">
													<div class="left-col">
														<label>Adınız Soyadınız</label>
														<div class="range">
															<input type="text" name="adsoyad" class="m2" value="" />															
														</div>
													</div>
													<div class="left-col right-col">
														<label>Telefon Numaranız</label>
														<div class="range">
															<input type="text" name="tel" class="m2" value="" />
														</div>
													</div>
												</div>
												<div class="secenek">
													<div class="left-col fullcl">
														<label>Başlık</label>
														<div class="range">
															<input type="text" name="baslik" class="m2" value="" />
														</div>
													</div>
												</div>
												<div class="secenek">
													<div class="left-col fullcl">
														<label>Açıklama</label>													
														<div class="range">
															<textarea name="aciklama"></textarea>																											
														</div>
													</div>
												</div>												
												 <div class="secenek">
													<div class="left-col">
														<label>İlçe</label>
														<label class="select">
														<select id="ilcesec" name="ilce">
															<?php foreach($ilce as $ilc) { if($_GET['ilce']==$ilc->slug)$sec = 'selected="selected"'; else $sec=''; echo '<option '.$sec.' value="'.$ilc->slug.'">'.$ilc->name.'</option>'; } ?>
														</select>
														</label>
													</div>
													<div class="left-col right-col">
														<label>Semt</label>
														<label class="select">
														<select id="semtgetir" name="semtg">
														</select>
														</label>
													</div>													
												</div>
												<div class="secenek">
													<div class="left-col">
														<label>Emlak Türü</label>
														<label class="select">
														<select name="emlak-turu" id="tur">
															<option value="konut">Konut</option>
															<option value="isyeri">İşyeri</option>
															<option value="arsa">Arsa</option>
															<option value="trustik-tesis">Trustik Tesis</option>
														</select>
														</label>
													</div>
													<div class="left-col right-col konut">
														<label>Emlak Tipi</label>
														<label class="select">
														<select name="konut-tipi">
															<?php 																							   																	
																$args = array( 'taxonomy' => 'konut-tipi', 'hide_empty'    => false );				
																$terms = get_terms('konut-tipi', $args);
																$count = count($terms);
																if ($count > 0) {
																	foreach ($terms as $term) {																				
																		$term_list .= '<option value="' . $term->slug . '">' . $term->name . '</option>';					
																	}
																	echo $term_list;
																}
																unset($term_list);
															?>															
														</select>
														</label>
													</div>
													<div class="left-col right-col isyeri">
														<label>Emlak Tipi</label>
														<label class="select">
														<select name="isyeri-tipi">
															<?php 																							   																	
																$args = array( 'taxonomy' => 'isyeri-tipi', 'hide_empty'    => false );				
																$terms = get_terms('isyeri-tipi', $args);
																$count = count($terms);
																if ($count > 0) {
																	foreach ($terms as $term) {																				
																		$term_list .= '<option value="' . $term->slug . '">' . $term->name . '</option>';					
																	}
																	echo $term_list;
																}
																unset($term_list);
															?>															
														</select>
														</label>
													</div>
													<div class="left-col right-col arsa">
														<label>İmar</label>
														<label class="select">
														<select name="imar">
															<?php 																							   																	
																$args = array( 'taxonomy' => 'imar', 'hide_empty'    => false );				
																$terms = get_terms('imar', $args);
																$count = count($terms);
																if ($count > 0) {
																	foreach ($terms as $term) {																				
																		$term_list .= '<option value="' . $term->slug . '">' . $term->name . '</option>';					
																	}
																	echo $term_list;
																}
																unset($term_list);
															?>															
														</select>
														</label>
													</div>
													<div class="left-col right-col trustik-tesis">
														<label>Emlak Tipi</label>
														<label class="select">
														<select name="trustik-tesis-tipi">
															<?php 																							   																	
																$args = array( 'taxonomy' => 'trustik-tesis-tipi', 'hide_empty'    => false );				
																$terms = get_terms('trustik-tesis-tipi', $args);
																$count = count($terms);
																if ($count > 0) {
																	foreach ($terms as $term) {																				
																		$term_list .= '<option value="' . $term->slug . '">' . $term->name . '</option>';					
																	}
																	echo $term_list;
																}
																unset($term_list);
															?>															
														</select>
														</label>
													</div>
												</div>
												<div class="secenek">
													<div class="left-col">
														<label>Fiyatı<span class="arsa"> / m2</span></label>
														<div class="range">
															<input type="text" name="fiyat" class="fiyat" value="" />
															<label class="select birim">
															<select name="birim">
																<option value="Türk Lirası">Türk Lirası</option>
																<option value="Dolar">Dolar</option>
																<option value="Euro">Euro</option>
																<option value="Sterlin">Sterlin</option>
															</select>
															</label>
														</div>
													</div>
													<div class="left-col right-col">
														<label>Takas</label>
														<label class="select">
														<select name="takas">
															<option value="1">Hayır</option>
															<option value="2">Evet</option>															
														</select>
														</label>
													</div>
												</div>
												<div class="secenek konut isyeri arsa">
													<div class="left-col">
														<label>Metrekare</label>
														<div class="range">
															<input type="text" name="m2" class="m2" value="" />															
														</div>
													</div>													
													<div class="left-col right-col konut">
														<label>Öğrenciye / Bekara</label>
														<label class="select">
														<select name="ogrenci">
															<option value="1">Hayır</option>
															<option value="2">Evet</option>															
														</select>
														</label>
													</div>													
												</div>
												<div class="secenek konut">
													<div class="left-col">
														<label>Yapı Tipi</label>
														<label class="select">
														<select name="yapitipi">
															<option value="1">Betonarme</option>
															<option value="2">Prefabrik</option>
															<option value="3">Ahşap</option>
															<option value="4">Taş Yapı</option>
														</select>
														</label>
													</div>
													<div class="left-col right-col">
														<label>Kullanım Durumu</label>
														<label class="select">
														<select name="kullanimdurumu" id="tur">
															<option value="1">Boş</option>
															<option value="2">Kiracılı</option>
															<option value="3">İnşaat</option>															
														</select>
														</label>
													</div>
												</div>
												<div class="secenek trustik-tesis">
													<div class="left-col">
														<label>Yapı Durumu</label>
														<label class="select">
														<select name="yapidurumu">
															<option value="1">Sıfır</option>
															<option value="2">İkinci El</option>
															<option value="3">İnşaat</option>
															<option value="4">Proje</option>
														</select>
														</label>
													</div>
													<div class="left-col right-col">
														<label>Yıldız Sayısı</label>
														<label class="select">
														<select name="yildiz" id="tur">
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
														</select>
														</label>
													</div>
												</div>
												<div class="secenek trustik-tesis">
													<div class="left-col">
														<label>Oda Sayısı</label>
														<div class="range">
															<input type="text" name="toda" class="m2" value="" />															
														</div>
													</div>
													<div class="left-col right-col">
														<label>Yatak Sayısı</label>
														<div class="range">
															<input type="text" name="tyatak" class="m2" value="" />															
														</div>
													</div>
												</div>
												<div class="secenek trustik-tesis">
													<div class="left-col">
														<label>Açık Alan m2</label>
														<div class="range">
															<input type="text" name="am2" class="m2" value="" />															
														</div>
													</div>
													<div class="left-col right-col">
														<label>Kapalı Alan m2</label>
														<div class="range">
															<input type="text" name="km2" class="m2" value="" />															
														</div>
													</div>
												</div>
												<div class="secenek trustik-tesis">
													<div class="left-col">
														<label>Bina Yaşı</label>
														<div class="range">
															<input type="text" name="tyas" class="m2" value="" />															
														</div>
													</div>
													<div class="left-col right-col">
														<label>Kat Sayısı</label>
														<div class="range">
															<input type="text" name="tkat" class="m2" value="" />															
														</div>
													</div>
												</div>
												<div class="secenek trustik-tesis">
													<div class="left-col">
														<label>Yıllık Getiri</label>
														<div class="range">
															<input type="text" name="yillikgetiri" class="m2" value="" />															
														</div>
													</div>
													<div class="left-col right-col">
														<label>Zemin Etüdü</label>
														<div class="range">
															<input type="text" name="zemin" class="m2" value="" />															
														</div>
													</div>
												</div>
												<div class="secenek arsa">
													<div class="left-col">
														<label>Tapu Durumu</label>
														<label class="select">
														<select name="tapudurumu">
															<option value="1">Hisseli Tapu</option>
															<option value="2">Mustakil Parsel</option>
															<option value="3">Tahsis</option>
															<option value="4">Zilliyet</option>
														</select>
														</label>
													</div>
													<div class="left-col right-col">
														<label>Ada No</label>
														<div class="range">
															<input type="text" name="adano" class="m2" value="" />															
														</div>
													</div>
												</div>
												<div class="secenek arsa">
													<div class="left-col">
														<label>Parsel No</label>
														<div class="range">
															<input type="text" name="parselno" class="m2" value="" />															
														</div>
													</div>
													<div class="left-col right-col">
														<label>Pafta No</label>
														<div class="range">
															<input type="text" name="paftano" class="m2" value="" />															
														</div>
													</div>
												</div>
												<div class="secenek arsa">
													<div class="left-col">
														<label>Gabari</label>
														<div class="range">
															<input type="text" name="gabari" class="m2" value="" />															
														</div>
													</div>
													<div class="left-col right-col">
														<label>Kaks</label>
														<div class="range">
															<input type="text" name="kaks" class="m2" value="" />															
														</div>
													</div>
												</div>
												<div class="secenek isyeri">
													<div class="left-col">
														<label>Aidat</label>
														<div class="range">
															<input type="text" name="iaidat" class="m2" value="" />															
														</div>
													</div>
													<div class="left-col right-col">
														<label>Kullanım Durumu</label>
														<label class="select">
														<select name="ikullanimdurumu" id="tur">
															<option value="1">Boş</option>
															<option value="2">Kiracılı</option>
															<option value="3">İnşaat</option>															
														</select>
														</label>
													</div>
												</div>
												<div class="secenek isyeri">
													<div class="left-col">
														<label>Bina Yaşı</label>
														<div class="range">
															<input type="text" name="iyas" class="m2" value="" />															
														</div>
													</div>
													<div class="left-col right-col">
														<label>Oda Sayısı</label>
														<div class="range">
															<input type="text" name="ioda" class="m2" value="" />															
														</div>
													</div>
												</div>
												<div class="secenek isyeri">
													<div class="left-col">
														<label>Kat Sayısı</label>
														<div class="range">
															<input type="text" name="itoplamkat" class="m2" value="" />															
														</div>
													</div>
													<div class="left-col right-col">
														<label>Bulunduğu Kat</label>
														<div class="range">
															<input type="text" name="ikat" class="m2" value="" />															
														</div>
													</div>
												</div>
												<div class="secenek konut isyeri">
													<div class="left-col">
														<label>Isıtma</label>
														<label class="select">
														<select name="isitma">
															<option value="1">Yok</option>
															<option value="2">Kombi</option>
															<option value="3">Kalorifer</option>
															<option value="4">Kat Kaloriferi</option>
															<option value="5">Soba</option>
															<option value="6">Klima</option>
															<option value="7">Diğer</option>
														</select>
														</label>
													</div>
													<div class="left-col right-col">
														<label>Yakıt</label>
														<label class="select">
														<select name="yakit" id="tur">
															<option value="1">Yok</option>
															<option value="2">Doğalgaz</option>
															<option value="3">Fuel-Oil</option>
															<option value="4">Elektrik</option>
															<option value="5">Kömür</option>
															<option value="6">Diğer</option>
														</select>
														</label>
													</div>
												</div>												
												<div class="secenek konut">
													<div class="left-col">
														<label>Salon</label>
														<label class="select">
														<select name="salon">
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
														</select>
														</label>
													</div>
													<div class="left-col right-col">
														<label>Oda</label>
														<label class="select">
														<select name="oda">
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
														</select>
														</label>
													</div>
												</div>
												<div class="secenek konut">
													<div class="left-col">
														<label>Banyo</label>
														<label class="select">
														<select name="banyo">
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
														</select>
														</label>
													</div>
													<div class="left-col right-col">
														<label>Aidat</label>
														<div class="range">
															<input type="text" name="aidat" class="m2" value="" />
														</div>
													</div>
												</div>
												<div class="secenek konut">
													<div class="left-col">
														<label>Bina Yaşı</label>
														<div class="range">
															<input type="text" name="yas" class="m2" value="" />
														</div>
													</div>													
												</div>
												<div class="secenek konut">
													<div class="left-col">
														<label>Binadaki Toplam Kat Sayısı</label>
														<div class="range">
															<input type="text" name="toplamkat" class="m2" value="" />
														</div>
													</div>
													<div class="left-col right-col">
														<label>Bulunduğu Kat</label>
														<div class="range">
															<input type="text" name="kat" class="m2" value="" />
														</div>
													</div>
												</div>
												<div class="secenek konut isyeri">
													<label>Cephe</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'cephe', 'hide_empty'    => false );				
															$terms = get_terms('cephe', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="cephe[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek konut isyeri">
													<label>Konut Özellikleri</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'konut-ozelligi', 'hide_empty'    => false );				
															$terms = get_terms('konut-ozelligi', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="konut-ozelligi[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek arsa">
													<label>Arsa Özellikleri</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'arsa-ozellikler', 'hide_empty'    => false );				
															$terms = get_terms('arsa-ozellikler', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="arsa-ozellikler[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek isyeri">
													<label>İşyeri Özellikleri</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'isyeri-ozellikleri', 'hide_empty'    => false );				
															$terms = get_terms('isyeri-ozellikleri', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="isyeri-ozellikleri[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>												
												<div class="secenek konut">
													<label>İç Özellikleri</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'ic-ozellik', 'hide_empty'    => false );				
															$terms = get_terms('ic-ozellik', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="ic-ozellik[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek konut isyeri arsa trustik-tesis">
													<label>Manzara</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'manzara', 'hide_empty'    => false );				
															$terms = get_terms('manzara', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="manzara[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek konut isyeri trustik-tesis">
													<label>Alt Yapı</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'altyapi', 'hide_empty'    => false );				
															$terms = get_terms('altyapi', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="altyapi[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek arsa">
													<label>Alt Yapı</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'arsa-altyapi', 'hide_empty'    => false );				
															$terms = get_terms('arsa-altyapi', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="arsa-altyapi[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek arsa">
													<label>Konum</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'konum', 'hide_empty'    => false );				
															$terms = get_terms('konum', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="konum[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek konut isyeri trustik-tesis">
													<label>Çevre Özellikleri</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'cevre', 'hide_empty'    => false );				
															$terms = get_terms('cevre', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="cevre[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek konut isyeri trustik-tesis">
													<label>Sosyal Özellikler</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'sosyal', 'hide_empty'    => false );				
															$terms = get_terms('sosyal', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="sosyal[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek trustik-tesis">
													<label>Oda Tipi</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'oda-tipi', 'hide_empty'    => false );				
															$terms = get_terms('oda-tipi', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="oda-tipi[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek trustik-tesis">
													<label>Oda Özellikleri</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'oda-ozellikleri', 'hide_empty'    => false );				
															$terms = get_terms('oda-ozellikleri', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="oda-ozellikleri[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek trustik-tesis">
													<label>Banyo Özellikleri</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'banyo-ozellikleri', 'hide_empty'    => false );				
															$terms = get_terms('banyo-ozellikleri', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="banyo-ozellikleri[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek trustik-tesis">
													<label>Tesis Özellikleri</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'tesis-ozellikleri', 'hide_empty'    => false );				
															$terms = get_terms('tesis-ozellikleri', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="tesis-ozellikleri[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek trustik-tesis">
													<label>Yeme & İçme</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'yeme-icme', 'hide_empty'    => false );				
															$terms = get_terms('yeme-icme', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="yeme-icme[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek trustik-tesis">
													<label>Çevre Aktiviteleri</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'cevre-aktiviteleri', 'hide_empty'    => false );				
															$terms = get_terms('cevre-aktiviteleri', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="cevre-aktiviteleri[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek trustik-tesis">
													<label>Tesis Aktiviteleri</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'tesis-aktiviteleri', 'hide_empty'    => false );				
															$terms = get_terms('tesis-aktiviteleri', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="tesis-aktiviteleri[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek trustik-tesis">
													<label>Toplantı & Kongre</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'toplanti-kongre', 'hide_empty'    => false );				
															$terms = get_terms('toplanti-kongre', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="toplanti-kongre[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<div class="secenek konut isyeri trustik-tesis">
													<label>Ulaşım</label>
													<?php 																							   																	
															$args = array( 'taxonomy' => 'ulasim', 'hide_empty'    => false );				
															$terms = get_terms('ulasim', $args);
															$count = count($terms);
															if ($count > 0) {
																foreach ($terms as $term) {																				
																	$term_list .= '<input type="checkbox" name="ulasim[]" id="' . $term->slug . '" value="' . $term->slug . '" /><label class="check" for="' . $term->slug . '">' . $term->name . '</label>';
																}
																echo $term_list;
															}
															unset($term_list);
														?>
												</div>
												<?php
												$s1 = rand(1,10);
												$s2 = rand(1,10);
												$_SESSION['cevap'] = $s1 + $s2;
												?>
												<div class="clear"></div>
												<div class="secenek">
													<div class="left-col fullcl">
														<label><?php echo $s1.' + '.$s2.' = ?'; ?> (Güvenlik Sorusu)</label>
														<div class="range">
															<input type="text" name="cevap" class="m2" value="" />
														</div>
													</div>
												</div>
												<div class="clear" style="height:8px;"></div>	
												<div class="secenek">												
													<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>													
													<script type="text/javascript">
														  function initialize() {						
															var mapOptions = {
															  zoom: 7,
															  center: new google.maps.LatLng(39.90153482884412, 32.8603515625),
															  mapTypeId: google.maps.MapTypeId.ROADMAP
															}
															var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);											  

																var cent = new google.maps.LatLng(39.90153482884412, 32.8603515625);
																var marker = new google.maps.Marker({
																	  position: cent,
																	  map: map,
																	  draggable: true
																});
													 
																google.maps.event.addListener(marker, "dragend", function(event){
																	var lat = event.latLng.lat();
																	var lng = event.latLng.lng();				
																	$("#harita").val(lat+', '+lng);							
																});						
															map.checkResize();						
															}
															google.maps.event.addDomListener(window, 'load', initialize);					
													</script>	 			
													<div id="map_canvas" style="width:100%;height:300px;border:#96cae3 1px solid;"></div>
													Eğer Harita Tam Olarak Görüntülenemiyorsa ve İmleci Doğru Olarak Düzenleyemiyorsanız Lütfen Sayfayı Yenileyerek Tekrar Deneyiniz.			
													<input type="hidden" name="harita" id="harita" value="39.90153482884412, 32.8603515625" size="30" tabindex="30" style="width: 400px;margin-top:-3px;" />
													<input type="hidden" name="kaydet" value="kaydet" />
												</div>												
												<div class="clear"></div>
												<div class="secenek submitte">
													<input type="submit" value="Satışa Sunulmak Üzere Gönder" />
												</div>
											</fieldset>
										</form>
								</article>
								<?php endif; ?>
							</div>
						</div>												
						<?php endwhile; endif; ?>						
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
			<!-- End content Section-->
<?php get_footer(); ?>