<?php
/*
Template Name: Property Search Results
*/

/**
 * @package WordPress
 * @subpackage Reales
 */

global $post;
get_header();
$reales_appearance_settings = get_option('reales_appearance_settings','');
$show_bc = isset($reales_appearance_settings['reales_breadcrumbs_field']) ? $reales_appearance_settings['reales_breadcrumbs_field'] : '';
$show_type_label = isset($reales_appearance_settings['reales_type_label_field']) ? $reales_appearance_settings['reales_type_label_field'] : '';
$sort = isset($_GET['sort']) ? sanitize_text_field($_GET['sort']) : 'newest';
$searched_posts = reales_search_properties();
$total_p = $searched_posts->found_posts;
$users = get_users();
?>

<div id="wrapper">

    <div id="mapView">
        <div class="mapPlaceholder"><span class="fa fa-spin fa-spinner"></span> <?php esc_html_e('Harite yükleniyor...', 'reales'); ?></div>
    </div>
    <?php wp_nonce_field('app_map_ajax_nonce', 'securityAppMap', true); ?>

    <div id="content">
        
        <div class="resultsList">
            <?php if($show_bc != '') {
                reales_breadcrumbs();
            } ?>
            <h1 class="pull-left"><?php                 
                echo esc_html($post->post_title); 
            ?></h1>
            
            <div class="clearfix"></div>
            <div class="row">
            <?php

            while ( $searched_posts->have_posts() ) {
                $searched_posts->the_post();

                $prop_id = get_the_ID();
                $url = wp_get_attachment_url(get_post_thumbnail_id($prop_id)); 
                $gallery = get_post_meta($prop_id, 'property_gallery', true);
                $images = explode("~~~", $gallery);
                $price = get_field_object('fiyat', $prop_id)['value'];
                $reales_general_settings = get_option('reales_general_settings');
                $currency = "TL ";
                $currency_pos = isset($reales_general_settings['reales_currency_symbol_pos_field']) ? $reales_general_settings['reales_currency_symbol_pos_field'] : '';
                $price_label = get_post_meta($prop_id, 'property_price_label', true);
                setlocale(LC_MONETARY, 'en_US');
                $address = get_post_meta($prop_id, 'property_address', true);
                $city = get_post_meta($prop_id, 'property_city', true);
                $state = get_post_meta($prop_id, 'property_state', true);
                $neighborhood = get_post_meta($prop_id, 'property_neighborhood', true);
                $zip = get_post_meta($prop_id, 'property_zip', true);
                $country = get_post_meta($prop_id, 'property_country', true);
                $type =  wp_get_post_terms($prop_id, 'property_type_category');
                $bedrooms = get_post_meta($prop_id, 'property_bedrooms', true);
                $bathrooms = get_post_meta($prop_id, 'property_bathrooms', true);
                $area = get_post_meta($prop_id, 'property_area', true);
                $unit = isset($reales_general_settings['reales_unit_field']) ? $reales_general_settings['reales_unit_field'] : '';
                $stt = "";
                $categories = wp_get_post_terms($prop_id, 'property_type_location',array('orderby' => 'term_id', 'order' => 'ASC'));
                foreach ($categories as $key => $value) {
                    if($key != 2){
                        $stt .= $value->name."/";
                    }
                    else{
                        $stt .= $value->name;
                    }
            }
            ?>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <a href="<?php echo esc_url(get_permalink($prop_id)); ?>" class="card" id="card-<?php echo esc_attr($prop_id); ?>">
                        <div class="figure">
                        <div class="img" style="background-image:url(<?php echo esc_url($url); ?>);"></div>
                            <div class="figCaption">
                                <div><?php echo esc_html($currency) . money_format('%!.0i', esc_html($price)) . esc_html($price_label); ?></div>
                                <span><span class="icon-eye"></span> <?php echo esc_html(reales_get_post_views($prop_id, '')); ?></span>
                                <?php
                                $favs = 0;
                                foreach ($users as $user) {
                                    $user_fav = get_user_meta($user->data->ID, 'property_fav', true);
                                    if(is_array($user_fav) && in_array($prop_id, $user_fav)) {
                                        $favs = $favs + 1;
                                    }
                                }
                                ?>
                                <span><span class="icon-heart"></span> <?php echo esc_html($favs); ?></span>
                                <span><span class="icon-bubble"></span> <?php comments_number('0', '1', '%'); ?></span>
                            </div>
                            <div class="figView"><span class="icon-eye"></span></div>
                            <?php if($type) { ?>
                                <div class="figType"><?php echo esc_html($type[0]->name); ?></div>
                            <?php } ?>
                        </div>
                        <h2><?php the_title(); ?></h2>
                        <div class="cardAddress">
                            <?php 
                                echo $stt.'<br/>';
                                if($address != '') {
                                    echo esc_html($address);
                                }
                            ?>
                        </div>
                        <ul class="cardFeat">
                            <?php 
                                $a = get_field_object('oda_sayısı', $prop_id);
                                $b = get_field_object('banyo_sayısı', $prop_id);
                                $c = get_field_object('metrekare',$prop_id);

                            ?>
                            <?php if($a['choices'][$a['value'][0]] !== '') { ?>
                                <li><span class="fa fa-moon-o"></span> <?php echo esc_html($a['choices'][$a['value'][0]]); ?></li>
                            <?php } ?>
                            <?php if($b['value'] !== '') { ?>
                                <li><span class="icon-drop"></span> <?php echo esc_html($b['value']); ?></li>
                            <?php } ?>
                            <?php if($c['value'] !== '') { ?>
                                <li><span class="icon-frame"></span> <?php echo esc_html($c['value']) . ' ' . esc_html('m2'); ?></li>
                            <?php } ?>
                        </ul>
                        <div class="clearfix"></div>
                    </a>
                </div>
            <?php } ?>
            </div>
            <div class="pull-left">
                <?php reales_pagination($searched_posts->max_num_pages); ?>
            </div>
            <div class="pull-right search_prop_calc">
                <?php
                $reales_appearance_settings = get_option('reales_appearance_settings');
                $per_p_field = isset($reales_appearance_settings['reales_properties_per_page_field']) ? $reales_appearance_settings['reales_properties_per_page_field'] : '';
                $per_p = $per_p_field != '' ? intval($per_p_field) : 10;
                $page_no = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $from_p = ($page_no == 1) ? 1 : $per_p * ($page_no - 1) + 1;
                $to_p = ($total_p - ($page_no - 1) * $per_p > $per_p) ? $per_p * $page_no : $total_p;
                echo esc_html($from_p) . ' - ' . esc_html($to_p) . __('/', 'reales') . esc_html($total_p) . __(' ilan bulundu', 'reales');
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

</div>

<?php
get_template_part('templates/app_footer');
?>