<?php
/**
 * @package WordPress
 * @subpackage Reales
 */

$reales_appearance_settings = get_option('reales_appearance_settings');
$copyright = isset($reales_appearance_settings['reales_copyright_field']) ? $reales_appearance_settings['reales_copyright_field'] : '';
?>


    <div class="home-footer">
        <div class="page-wrapper">
            <div class="row">
                <?php get_sidebar('footer'); ?>
            </div>
            <?php if($copyright && $copyright != '') { ?>
                <div class="copyright"><?php echo esc_html($copyright); ?><br/>
                    <ul><li><a href="##" class="btn btn-sm btn-icon btn-round btn-o btn-white"><span class="fa fa-facebook"></span></a> <a href="##" class="btn btn-sm btn-icon btn-round btn-o btn-white"><span class="fa fa-twitter"></span></a> <a href="##" class="btn btn-sm btn-icon btn-round btn-o btn-white"><span class="fa fa-google-plus"></span></a> <a href="##" class="btn btn-sm btn-icon btn-round btn-o btn-white"><span class="fa fa-linkedin"></span></a></li></ul>
                </div>
            <?php } ?>
        </div>
    </div>

    <?php wp_footer(); ?>
</body>
</html>