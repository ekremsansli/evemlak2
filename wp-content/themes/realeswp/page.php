<?php
/**
 * @package WordPress
 * @subpackage Reales
 */

global $post;
get_header();
$reales_appearance_settings = get_option('reales_appearance_settings','');
$sidebar_position = isset($reales_appearance_settings['reales_sidebar_field']) ? $reales_appearance_settings['reales_sidebar_field'] : '';
$show_bc = isset($reales_appearance_settings['reales_breadcrumbs_field']) ? $reales_appearance_settings['reales_breadcrumbs_field'] : '';
?>

<div id="" class="page-wrapper">
    <div class="page-content">
        <div class="row">
            <?php 
                if(is_front_page() == true){
                    echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><marquee style="font-size: 22px;">'.get_post(1190)->post_content.'</marquee>';
                    echo do_shortcode('[services s1title="Emlak Danışmanlığı" s1icon="icon-pointer" s1text="Türkiye\'nin her yerinde uzman emlak danışmanlığı" s1link="/emlak-danismanligi/" s2icon="icon-users" s2title="Analiz ve Ekspertiz" s2text="İhtiyaçların doğru analizi ve ekspertiz ile müşterilerimizi sektörün en yüksek hizmet kalitesi standartlarıyla buluşturmak" s2link="/analiz-ve-ekspertiz/" s3icon="icon-home" s3title="Anahtar Teslim Ev Yenileme" s3text="Anahtar Teslim Ev Yenileme firmamız müşteri odaklı çalışma prensibine göre çalışmalarını yürütmektedir." s3link="/anahtar-teslim-ev-yenileme/" s4icon="icon-cloud-upload" s4title="Proje Geliştirme" s4text="Ev Emlak Gayrimenkul,global ölçekte değer yaratan ülkeye katma değer sağlayacak projeler geliştirmeyi hedeflemiştir." s4link="/proje-gelistirme/"]');
                    echo '</div>';
                }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                
                <?php while(have_posts()) : the_post(); ?>

                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if($show_bc != '') {
                        reales_breadcrumbs();
                    } ?>
                    <div class="entry-content">
                        <?php the_content(); ?>
                        <div class="clearfix"></div>
                        <?php wp_link_pages( array(
                            'before'      => '<div class="page-links">',
                            'after'       => '</div>',
                            'link_before' => '<span>',
                            'link_after'  => '</span>',
                            'pagelink'    => '%',
                            'separator'   => '',
                        ) ); ?>
                    </div>
                </div>

                <?php if(comments_open() || get_comments_number()) {
                    comments_template();
                }

                endwhile; ?>
            </div>
            <?php if($sidebar_position == 'right') {
            ?>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="content-sidebar" style="margin-top: 82px;">
                    <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
                        <h3 class="osLight sidebar-header">İnsan Kaynakları</h3>
                        <a href="/insan-kaynaklari/">
                            <img src="/wp-content/themes/realeswp/images/ik.jpg" class="img-responsive"/>
                            <p>Bizimle çalışmak ister misiniz?</p>
                        </a>
                    </div>
                    <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12" style="margin-top:25px;">
                        <h3 class="osLight sidebar-header">Ev Alırken</h3>
                            <img src="/wp-content/themes/realeswp/images/emlak.jpg" class="img-responsive"/>
                            <p>
                                <br/>
                                Ev Emlak Gayrimenkul olarak gayrimenkul sektöründeki tecrübemize dayanarak söyleyebiliriz ki bir evin ne kadar büyük olduğu,rengi ya da biçimi o evi hayalinizdeki ev yapmaz.<br/>
                                Hayalinizdeki evi bulmak, kapıdan içeriye girer girmez hissettikleriniz ve o evde nasıl yaşayacağınızı gözlerinizde canlandırabiliyor olmanızla ilgilidir.<br/><br/>
                                <a href="/ev-alirken-dikkat-edilecek-hususlar/">Daha Fazla Bilgi İçin Tıklayınız</a>
                            </p>
                    </div>
                    <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12" style="margin-top:25px;">
                        <h3 class="osLight sidebar-header">Evinizi Satarken</h3>
                            <img src="/wp-content/themes/realeswp/images/satis.jpg" class="img-responsive"/>
                            <p>
                                <br/>
                                Evinizi satmaya karar verdiğinizde satış işleminin en kısa zamanda gerçekleşmesini istersiniz ancak bir evi satmadan önce göz önünde bulundurmanız gerekenler vardır.Bu süreçte vereceğiniz en önemli kararlardan biri kiminle çalışacağınıza karar vermektir.

                                <br/><br/>
                                <a href="/evinizi-satarken-dikkat-etmeniz-gerekenler/">Daha Fazla Bilgi İçin Tıklayınız</a>
                            </p>
                    </div>
                    <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12" style="margin-top:25px;">
                        <h3 class="osLight sidebar-header">Linkler</h3>
                        <?=get_post(1192)->post_content?>
                    </div>
                    <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12" style="margin-top:25px;">
                        <img src="/wp-content/themes/realeswp/images/guventakas.jpg" class="img-responsive"/>
                    </div>
                    <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12" style="margin-top:25px;">
                        <a href="/insan-kaynaklari/">
                            <img src="/wp-content/themes/realeswp/images/kendiis.jpg" class="img-responsive"/>
                        </a>
                    </div>
                    <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12" style="margin-top:25px;">
                        <a href="/bilgi-bankasi/">
                            <img src="/wp-content/themes/realeswp/images/bilgibankasi.jpg" class="img-responsive"/>
                        </a>
                    </div>
                     <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12" style="margin-top:25px;">
                        <p>Tüm soru ve sorunlarınız için;<br/>
                            info@evemlakgayrimenkul.com<br/>
                            0216 594 76 30
                        </p>
                    </div>
                </div>
            </div>
            <?php
            } ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>