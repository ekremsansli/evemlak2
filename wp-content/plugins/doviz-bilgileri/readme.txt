=== Döviz Bilgileri ===
Contributors: Erdem ARSLAN @erdemsaid
Tags: döviz, dolar, piyasa, imkb, bist, euro, repo, altın
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=EHYSTPKQQEUYW
Requires at least: 2.8
Tested up to: 3.8.1
Stable tag: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Bu eklenti, USD, EURO, BİST 100, ALTIN ve REPO bilgilerini çeker ve gösterir.

== Description ==
Bu eklenti piyasa verilerinin Wordpress üzerinde gösterilmesi amacıyla yazılmıştır. BİST100, Amerikan Doları, Euro, Altın ve Repo bilgilerini gösterir. İster widget, ister shortcode, isterseniz de fonksiyon olarak verileri kullanabilirsiniz.

Elde edilen veriler, kesin bilgi içermemektedir. Alım ve satım işlemlerinde bu verileri kullanmayınız. Verilerin kullanımından doğan her türlü problemin sorumluluğu eklentiyi kullanana aittir. Bu eklentinin kullandığı veriler, Ziraat Bankası iPhone uygulamasından alınmaktadır. Ziraat Bankası bu verileri paylaşmayı ve yayınlamayı durdurma ve/veya değiştirme hakkına sahiptir. 

Bu eklenti eğitim amacıyla hazırlanmıştır. 

== Installation ==
1. Eklenti klasörünü /wp-content/plugins/ klasörüne kopyala.
2. Eklentiler menüsünden eklentiyi aktifleştirin.
3. Ayarlar bölümünden eklentinizi özelleştirin.
4. Herşey tamam !

== Frequently Asked Questions ==
Soru: Veriler kaç dakikada bir güncellenmektedir?
Cevap: Verileri Ziraat Bankası güncellemektedir. Bu güncellemenin periyotlarıyla ilgili kesin bir bilgi mevcut değildir. Ancak eklenti her 15 dakikada bir önbellek dosyasını yenilemektedir.

Soru: Shortcode nasıl kullanılır?
Cevap: [doviz] şeklinde kullanmanız yeterlidir.

Soru: Fonksiyon nasıl kullanılır?
Cevap: Fonksiyonu kullanmak istediğiniz yerde piyasa_verileri() yazmanız yeterlidir. Bu fonksiyon size ham verileri döndürmektedir.

== Screenshots ==
1. Bileşen / Widget
2. Shortcode

== Changelog ==
= v.1.1 =
readme.txt dosyasında düzenleme ve verssiyon bilgisini hatasının düzeltilmesi

= v.1.0 =
İlk yayınlanma

== Upgrade Notice ==
İlk yayınlanma