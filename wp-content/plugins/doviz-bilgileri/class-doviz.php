<?php

Class Doviz {
	
	private $cache_dosyasi;
	
	public function __construct() {
		// Cache Dosyası
		$this->cache_dosyasi = DOVIZ_PLUGIN_DIR . DOVIZ_PLUGIN_CACHE_DIR . DIRECTORY_SEPARATOR . DOVIZ_PLUGIN_CACHE_FILE;
		// Döviz verilerini çek! veri JSON Formatında olacak! Ona göre kullan
	}
	
	
	/*
		döviz verilerini alır!
	*/
	public function veriler()
	{
		if( file_exists( $this->cache_dosyasi ) && ( current_time('timestamp') - filemtime( $this->cache_dosyasi ) ) < DOVIZ_PLUGIN_CACHE_TIME ) {
			// Dosya var ve zamanı geçmemiş !
			return file_get_contents( $this->cache_dosyasi );
		} else {
			// dosya yok veya zamanı geçmiş!
			return $this->doviz_bilgilerini_cek();
		}
	}
	
	
	/*
		Döviz Verilerini sunucudan al !
	*/
	private function doviz_bilgilerini_cek()
	{
		$HTTP_args = array(
			'timeout'     => 5,
			'user-agent'  => 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0'
		);
		$yanit = wp_remote_get( 'http://iphone.ziraatbank.com.tr/iphone/FinancialData.aspx', $HTTP_args );
		
		if( wp_remote_retrieve_response_code( $yanit ) == 200 ) {
			// yanıt sağlam ise, oku ve yaz ve ver!
			$json_veri = json_decode( wp_remote_retrieve_body( $yanit ), TRUE );
			
			
			// bunları düzenle
			$bilgiler = json_encode(
				array(
					'bist'	=> array(
						'degeri'	=> $json_veri[0]['Value'],
						'degisim'	=> $this->degisim_cevir( $json_veri[0]['Difference'] )
					),
					'repo'	=> array(
						'degeri'	=> $json_veri[1]['Value'],
						'degisim'	=> $this->degisim_cevir( $json_veri[1]['Difference'] )
					),
					'usd'	=> array(
						'degeri'	=> $this->TL_sil( $json_veri[4]['Value'] ),
						'degisim'	=> $this->degisim_cevir( $json_veri[4]['Difference'] )
					),
					'euro'	=> array(
						'degeri'	=> $this->TL_sil( $json_veri[3]['Value'] ),
						'degisim'	=> $this->degisim_cevir( $json_veri[3]['Difference'] )
					),
					'altin'	=> array(
						'degeri'	=> $this->TL_sil( $json_veri[2]['Value'] ),
						'degisim'	=> $this->degisim_cevir( $json_veri[2]['Difference'] )
					),
					'zaman'	=> current_time('timestamp')
				)
			);
			
			// cache dosyasına yaz!
			$fp = fopen( $this->cache_dosyasi, "w");
			fwrite( $fp, $bilgiler );
			fclose( $fp );
			
		} else {
			// veri alınamadı! Sahte veri hazırla! Varsa dosyayı oku ve al! yoksa, 0 doldur ver!
			if ( file_exists( $this->cache_dosyasi ) )
			{
				$bilgiler = file_get_contents( $this->cache_dosyasi );
			} else {
				$bilgiler = json_encode(
					array(
						'bist'	=> array(
							'degeri'	=> '00000',
							'degisim'	=> 'asagi'
						),
						'usd'	=> array(
							'degeri'	=> '0,0000',
							'degisim'	=> 'asagi'
						),
						'euro'	=> array(
							'degeri'	=> '0,0000',
							'degisim'	=> 'asagi'
						),
						'repo'	=> array(
							'degeri'	=> '0,0',
							'degisim'	=> 'asagi'
						),
						'altin'	=> array(
							'degeri'	=> '0,0000',
							'degisim'	=> 'asagi'
						),
						'zaman'	=> current_time('timestamp')
					)
				);
			}
		}
		
		return $bilgiler;
	}
	
	private function degisim_cevir( $deger )
	{
		if( $deger == 0 )
		{
			return 'yukari';
		}
		elseif ( $deger == 1 )
		{
			return 'asagi';
		} else {
			return 'sabit';
		}
	}
	
	private function TL_sil( $veri )
	{
		$bol = explode( ' ', $veri );
		return $bol[0];
	}
	
	
	
}